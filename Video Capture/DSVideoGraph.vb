#Region "Options"
Option Explicit On
Option Strict Off
#End Region

#Region "Copyright"
'Dextera Labs Inc.
'(c) Copyright 2005-2013 Dextera Labs Inc.
'ALL RIGHTS RESERVED

'The software, source code and information contained herein are
'proprietary to, and comprise valuable intellectual property and
'trade secrets of Dextera Labs Inc.

'This software is furnished pursuant to a Source Code License
'Agreement between Dextera and the Licensee and may be used,
'copied, transmitted, and stored only by the Licensee in accordance
'with the terms of such license and with the inclusion of this
'copyright notice.

'This software, source code and information or any other copies
'thereof may not be provided or otherwise made available to any
'person not bound by the terms of the Source Code License Agreement.
#End Region

#Region "imports"
Imports System.Runtime.InteropServices
Imports DirectShowLib
Imports System.Runtime.InteropServices.ComTypes
Imports System.Threading
Imports VBLibraries.Utils.BaseUtility.Time
Imports INOGENISampleApp.INOGENIInterfaceModule.HRESULT
Imports INOGENISampleApp.INOGENIInterfaceModule
#End Region

Namespace GENIE1080P


    Public Class DSVideoGraph
        Implements ISampleGrabberCB

#Region "Events"
        Public Event OnWindowSizeRequested(ByVal Sender As Object)
        Public Event OnGraphStateChanged(ByVal Sender As Object, ByVal GraphState As PlayState)
        Public Event OnGraphBuilded(ByVal Sender As Object, ByVal GraphIsBuilded As Boolean)
#End Region

#Region "Constant"
        Public Const VIDEO_STREAM_CONFIG_CAPS As Integer = 128
        Dim D As Integer = Convert.ToInt32("0X8000", 16)
        Public WM_GRAPHNOTIFY As Integer = D + 1


#End Region

#Region "Enums"
        Enum PlayState
            Stopped
            Paused
            Running
            Init
        End Enum
        Enum WindowType
            SeparatedWindow
            PictureBox
        End Enum

#End Region

#Region "inner Classes"
        Public Structure VideoFormat
            Public Width As Long
            Public Height As Long
            Public FrameRate As Single
        End Structure
        Public Class VideoCapabilitiesDef
            Public Width As Long
            Public Height As Long
            Public MinFrameRate As Single
            Public MaxFrameRate As Single
            Public AvgFrameRate As Single


            Public ReadOnly Property subTypeName
                Get
                    Return DsToString.MediaSubTypeToString(subType)
                End Get
            End Property
            Public subType As System.Guid
            Public Function isvalid() As Boolean
                If Height > 0 AndAlso Width > 0 Then
                    Return True
                End If
            End Function
            Public Function Compare(ByVal ElementCompared As VideoCapabilitiesDef) As Boolean
                If ElementCompared.Height = Height AndAlso ElementCompared.Width = Width AndAlso ElementCompared.subTypeName = subTypeName AndAlso ElementCompared.MinFrameRate = MinFrameRate AndAlso ElementCompared.MaxFrameRate = MaxFrameRate Then
                    Return True
                End If
                Return False
            End Function
            Public Sub New()
                Width = 0
                Height = 0
                MinFrameRate = 0
                MaxFrameRate = 0
                AvgFrameRate = 0
                subType = Nothing
            End Sub
        End Class
        Public Structure RendereringProp
            Public AverageFrameRate As Integer
            Public SyncOffset As Integer
            Public DevSyncOffset As Integer
            Public FramesDrawn As Integer
            Public FramesDropped As Integer
            Public Jitter As Integer
        End Structure
#End Region

#Region "Properties"

        
        Private UseAnalyzerFilterValue As Boolean = False
        Public Property UseAnalyzerFilter() As Boolean
            Get
                Return UseAnalyzerFilterValue
            End Get
            Set(ByVal value As Boolean)
                UseAnalyzerFilterValue = value
            End Set
        End Property

        Private _CurrentState As PlayState = PlayState.Stopped
        Private Property CurrentState As PlayState
            Get
                Return _CurrentState
            End Get
            Set(ByVal value As PlayState)
                If value <> _CurrentState Then
                    RaiseEvent OnGraphStateChanged(Me, value)
                End If
                _CurrentState = value
            End Set
        End Property

        Public ReadOnly Property State As PlayState
            Get
                Return _CurrentState
            End Get

        End Property

        Private _VideoCapabilities() As VideoCapabilitiesDef
        Public ReadOnly Property VideoCapabilities As VideoCapabilitiesDef()
            Get
                Return _VideoCapabilities
            End Get
        End Property

        Private _SelectedCap As VideoCapabilitiesDef
        Public Property SelectedCap As VideoCapabilitiesDef
            Get
                Return _SelectedCap
            End Get
            Set(ByVal value As VideoCapabilitiesDef)
                _SelectedCap = value
            End Set
        End Property

        Private VideoWindow As IVideoWindow = Nothing
        Private MediaControl As IMediaControl = Nothing
        Private MediaEventEx As IMediaEventEx = Nothing
        Protected GraphBuilder As IGraphBuilder = Nothing
        Protected CaptureGraphBuilder As ICaptureGraphBuilder2 = Nothing
        Protected ConfigStreamCaps As IAMStreamConfig = Nothing
        'Recorder
        Private MediaControlRecorder As IMediaControl = Nothing
        Private FilterGraph As IFilterGraph2 = Nothing

        'SampleGrabber
        Protected baseGrabFlt As IBaseFilter = Nothing
        Protected sampGrabber As ISampleGrabber = Nothing

        Protected AViDecFlt As IBaseFilter = Nothing

        Protected Renderer As IBaseFilter = Nothing
        Protected RendererIQualProp As IQualProp = Nothing

        Protected RendererDirectDraw As IBaseFilter = Nothing
        Protected RendererIQualProDirectDraw As IQualProp = Nothing

        'Inogeni Analyzer
        Protected baseINOGENIFlt As IBaseFilter = Nothing
        Private InogeniAnalyzer As ISourcePropertiesInogeni = Nothing



        Protected videoRendererFilter As IBaseFilter = Nothing

        Protected AVIDecompressorFilter As IBaseFilter = Nothing
        Protected ColorSpaceConverterFilter As IBaseFilter = Nothing


        'Video input  object
        Protected PinInput As IPin = Nothing
        Protected SourceFilter As IBaseFilter

        Private Handle As System.IntPtr


        'Filter ID
        Protected CLSID As Guid
        Protected _FilterName As String
        Public ReadOnly Property FilterName() As String
            Get
                Return _FilterName
            End Get
        End Property
        Public ReadOnly Property SourceFilterCLSID As Guid
            Get
                Return CLSID
            End Get
        End Property
        Public ReadOnly Property isGraphBuilded() As Boolean
            Get
                If GraphBuilder Is Nothing OrElse CaptureGraphBuilder Is Nothing OrElse VideoWindow Is Nothing OrElse MediaControl Is Nothing OrElse MediaEventEx Is Nothing OrElse FilterName = "" Then
                    Return False
                Else
                    Return True
                End If
            End Get
        End Property
        Public ReadOnly Property isGraphRunning() As Boolean
            Get
                If _CurrentState = PlayState.Running Then
                    Return True
                End If
                Return False
            End Get
        End Property

        Private rot As DsROTEntry = Nothing
        Protected VideoInfoHeader As VideoInfoHeader

        Private _CapturePictureBox As PictureBox
        Public Property CapturePictureBox() As PictureBox
            Get
                Return _CapturePictureBox
            End Get
            Set(ByVal value As PictureBox)
                _CapturePictureBox = value
            End Set
        End Property

        Private _OutputType As WindowType
        Public Property OutputType() As WindowType
            Get
                Return _OutputType
            End Get
            Set(ByVal value As WindowType)
                _OutputType = value
            End Set
        End Property
#End Region

#Region "Constructor"
        Public Sub New(ByVal name As String, ByVal VideoOutput As WindowType)
            _OutputType = VideoOutput
            _SelectedCap = Nothing
            _VideoCapabilities = Nothing
        End Sub

        Public Sub New(ByVal name As String, ByRef outputForm As System.IntPtr)
            Me._OutputType = WindowType.SeparatedWindow
            Me.Handle = outputForm
            Me.CapturePictureBox = Nothing
            _SelectedCap = Nothing
            _VideoCapabilities = Nothing
        End Sub
        Public Sub New(ByVal name As String, ByRef CaptureBox As PictureBox)
            Me._OutputType = WindowType.PictureBox
            Me.Handle = Nothing
            Me.CapturePictureBox = CaptureBox
            _SelectedCap = Nothing
            _VideoCapabilities = Nothing
        End Sub
#End Region


#Region "Interfaces"

        Public Function BufferCB(ByVal SampleTime As Double, ByVal pBuffer As System.IntPtr, ByVal BufferLen As Integer) As Integer Implements ISampleGrabberCB.BufferCB
            Debug.WriteLine("ISampleGrabberCB::BufferCB ")
            Return 0
        End Function
        Public Function SampleCB(ByVal SampleTime As Double, ByVal pSample As DirectShowLib.IMediaSample) As Integer Implements ISampleGrabberCB.SampleCB
            Debug.WriteLine("ISampleGrabberCB::SampleCB")
            Return 0
        End Function
#End Region

#Region "Direct show methods"
        ''' <summary>
        ''' Get renderer IQualProp statistics on video grabbing
        ''' </summary>
        ''' <param name="stat"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GetRenderStatistics(ByRef stat As RendereringProp) As Boolean
            Dim hr As Integer = 0
            Dim rendererProp As IQualProp = Nothing
            Try
                'Init
                stat.AverageFrameRate = -1
                stat.SyncOffset = -1
                stat.DevSyncOffset = -1
                stat.FramesDrawn = -1
                stat.FramesDropped = -1
                stat.Jitter = -1



                ' Check if the interface is ready
                If _SelectedCap.subType = MediaSubType.NV12 Or _SelectedCap.subType = MediaSubType.YV12 Or _SelectedCap.subType = MediaSubType.I420 Then
                    rendererProp = RendererIQualProDirectDraw
                Else
                    rendererProp = RendererIQualProp
                End If

                If rendererProp IsNot Nothing Then
                    hr = rendererProp.get_AvgFrameRate(stat.AverageFrameRate)
                    DsError.ThrowExceptionForHR(hr)

                    hr = rendererProp.get_AvgSyncOffset(stat.SyncOffset)
                    DsError.ThrowExceptionForHR(hr)

                    hr = rendererProp.get_DevSyncOffset(stat.DevSyncOffset)
                    DsError.ThrowExceptionForHR(hr)

                    hr = rendererProp.get_FramesDrawn(stat.FramesDrawn)
                    DsError.ThrowExceptionForHR(hr)

                    hr = rendererProp.get_FramesDroppedInRenderer(stat.FramesDropped)
                    DsError.ThrowExceptionForHR(hr)

                    hr = rendererProp.get_Jitter(stat.Jitter)
                    DsError.ThrowExceptionForHR(hr)

                    Return True
                End If
                Return False
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function GrabAFrame() As Byte()
            If Me._OutputType = WindowType.PictureBox Then
                Dim size As Integer = VideoInfoHeader.BmiHeader.ImageSize
                If ((size < 1000) OrElse (size > 16000000)) Then
                    Return Nothing
                End If
                Dim buffer(size) As Byte

                Dim handle As GCHandle = GCHandle.Alloc(buffer, GCHandleType.Pinned)
                Dim scan0 As Integer = handle.AddrOfPinnedObject
                sampGrabber.GetCurrentBuffer(size, CType(scan0, IntPtr))
                handle.Free()

                Return buffer
            Else
                'Not supported yet
                MsgBox("Error samplegrabber is not supported yet in separated windows!")
                Return (Nothing)
            End If
        End Function

        Public Function TakeAPicture3() As Bitmap
            If Me._OutputType = WindowType.PictureBox Then
                Dim size As Integer = VideoInfoHeader.BmiHeader.ImageSize
                If ((size < 1000) OrElse (size > 16000000)) Then
                    Return Nothing
                End If
                Dim buffer(size + 64000) As Byte



                Dim handle As GCHandle = GCHandle.Alloc(buffer, GCHandleType.Pinned)
                Dim scan0 As Integer = handle.AddrOfPinnedObject
                sampGrabber.GetCurrentBuffer(size, CType(scan0, IntPtr))


                Dim w As Integer = Me.VideoInfoHeader.BmiHeader.Width
                Dim h As Integer = Me.VideoInfoHeader.BmiHeader.Height
                If (((w And &H3) <> 0) Or (w < 32) Or (w > 4096) Or (h < 32) Or (h > 4096)) Then
                    Return Nothing
                End If
                Dim stride As Integer = w * 3


                scan0 += (h - 1) * stride




                Dim b As Bitmap = New Bitmap(w, h, -stride, Imaging.PixelFormat.Format24bppRgb, CType(scan0, IntPtr))
                'Dim stride As Integer = w * 2


                'scan0 += (h - 1) * stride
                'Dim b As Bitmap = New Bitmap(w, h, -stride, Imaging.PixelFormat.Undefined, CType(scan0, IntPtr))
                handle.Free()

                Return b
            Else
                'Not supported yet
                MsgBox("Error snapshot are not supported yet in separated windows!")
            End If
            Return Nothing
        End Function

        Public Function BuildTheGraph(ByVal FilterName As String) As Boolean
            Return CaptureVideo(FilterName)
        End Function
        Public Sub BreakTheGraph()
            closeinterfaces()
        End Sub


        'Private Function GetPin(ByRef Filter As IBaseFilter, ByRef name As String) As IPin

        'End Function

        Public Sub updatecap(ByVal Filtername As String)
            Try
                GetInterfaces()
                _VideoCapabilities = Nothing
                _VideoCapabilities = (GetFilterCapabilities(Filtername))
                If _SelectedCap Is Nothing Then
                    _SelectedCap = GetFormat()
                End If

                If _SelectedCap Is Nothing AndAlso _VideoCapabilities IsNot Nothing AndAlso _VideoCapabilities.Length > 0 Then
                    'select one by defaylt

                    _SelectedCap = _VideoCapabilities(0)
                End If
            Catch ex As Exception

            End Try

        End Sub
        Private Function CaptureVideo(ByVal FilterName As String) As Boolean
            Dim hr As Integer = 0
            Dim sourceFilter As IBaseFilter = Nothing
            Try
                GetInterfaces()

                _VideoCapabilities = Nothing
                _VideoCapabilities = (GetFilterCapabilities(FilterName))
                If _SelectedCap Is Nothing Then
                    _SelectedCap = GetFormat()
                End If

                If _SelectedCap Is Nothing AndAlso _VideoCapabilities IsNot Nothing AndAlso _VideoCapabilities.Length > 0 Then
                    'select one by defaylt

                    _SelectedCap = _VideoCapabilities(0)
                End If

                hr = Me.CaptureGraphBuilder.SetFiltergraph(Me.GraphBuilder) 'Specifies filter graph "graphbuilder" for the capture graph builder "captureGraphBuilder" to use.
                Debug.WriteLine("Attach the filter graph to the capture graph : " & DsError.GetErrorText(hr))
                DsError.ThrowExceptionForHR(hr)

                Dim media2 As New AMMediaType()
                media2.majorType = MediaType.Video
                media2.subType = MediaSubType.RGB24
                media2.formatType = FormatType.VideoInfo

                hr = sampGrabber.SetMediaType(media2)
                If hr < 0 Then
                    Marshal.ThrowExceptionForHR(hr)
                End If



                hr = sampGrabber.SetMediaType(Nothing)
                Debug.WriteLine("Create media type for sample grabber failed : " & DsError.GetErrorText(hr))
                DsError.ThrowExceptionForHR(hr)



                If UseAnalyzerFilterValue AndAlso baseINOGENIFlt IsNot Nothing Then
                    hr = GraphBuilder.AddFilter(baseINOGENIFlt, "Inoggeni Analyzer")
                    Debug.WriteLine("Add Inogeni Analyzer to our graph : " & DsError.GetErrorText(hr))
                    DsError.ThrowExceptionForHR(hr)
                End If

                If AViDecFlt IsNot Nothing Then
                    hr = GraphBuilder.AddFilter(AViDecFlt, "AVI decompressor")
                    Debug.WriteLine("add AVI Decompressor to our graph : " & DsError.GetErrorText(hr))
                    DsError.ThrowExceptionForHR(hr)
                End If


                'If FrmMain.cbxExternalWindow.Checked = False Then
                hr = GraphBuilder.AddFilter(baseGrabFlt, "Ds.NET Grabber")
                Debug.WriteLine("Add SampleGrabber to our graph : " & DsError.GetErrorText(hr))
                DsError.ThrowExceptionForHR(hr)
                'End If


                ''add AVI Decompressor
                AVIDecompressorFilter = CType(New AVIDec(), IBaseFilter)
                hr = Me.GraphBuilder.AddFilter(AVIDecompressorFilter, "AVI Decompressor")
                DsError.ThrowExceptionForHR(hr)

                ''add Color Space Converter
                ColorSpaceConverterFilter = CType(New Colour(), IBaseFilter)
                hr = Me.GraphBuilder.AddFilter(ColorSpaceConverterFilter, "Color Space Converter")
                DsError.ThrowExceptionForHR(hr)



                sourceFilter = FindCaptureDevice(FilterName)
                If sourceFilter Is Nothing Then
                    Debug.WriteLine("Unable to find device")
                    MsgBox("Unable to lock on " & FilterName & "!")
                    Return False
                End If


                'Ok find the pind
                hr = Me.CaptureGraphBuilder.FindPin(sourceFilter, PinDirection.Output, DirectShowLib.PinCategory.Capture, CLSID_MEdiaType_Video, False, 0, PinInput)
                'hr = Me.CaptureGraphBuilder1.FindPin(sourceFilter, PinDirection.Output, DirectShowLib.PinCategory.Capture, DirectShowLib.MediaType.Video, False, 0, PinInput1)
                Debug.WriteLine("Get the filter output pin : " & DsError.GetErrorText(hr))
                DsError.ThrowExceptionForHR(hr)

                ' Query the stream capabalities
                If PinInput Is Nothing Then
                    MessageBox.Show("Input pin cannot be created!")
                    Return False
                End If

                'Get the IAMStreamconfig interface from the pin
                ConfigStreamCaps = CType(PinInput, IAMStreamConfig)

                Dim Count As Integer = 0
                Dim Size As Integer = 0
                If ConfigStreamCaps Is Nothing Then
                    MessageBox.Show("unable to query the IAMStreamConfig interface of the pin!")
                    Return False
                End If

                hr = Me.ConfigStreamCaps.GetNumberOfCapabilities(Count, Size)
                Debug.WriteLine("GetNumberOfCapabilities return an error : " & DsError.GetErrorText(hr))
                DsError.ThrowExceptionForHR(hr)


                If Size = VIDEO_STREAM_CONFIG_CAPS Then

                    For i As Integer = 0 To (Count - 1)
                        Dim scc As New VideoStreamConfigCaps
                        Dim pScc As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(scc))
                        Dim media As DirectShowLib.AMMediaType = Nothing
                        Dim infoheader As DirectShowLib.VideoInfoHeader = Nothing
                        Dim myobj As Object = scc

                        hr = Me.ConfigStreamCaps.GetStreamCaps(i, media, pScc)
                        Debug.WriteLine("GetNumberOfCapabilities return an error : " & DsError.GetErrorText(hr))
                        DsError.ThrowExceptionForHR(hr)

                        Debug.WriteLine(media.subType.ToString)


                        Marshal.PtrToStructure(pScc, scc)

                        '' copy out the videoinfoheader
                        infoheader = New VideoInfoHeader
                        Marshal.PtrToStructure(media.formatPtr, infoheader)


                        ''Examine the format, and possibly use it. 
                        Debug.WriteLine("Set Format: " & infoheader.BmiHeader.Width.ToString & "x" & infoheader.BmiHeader.Height.ToString & "@" & Math.Round((10000000 / scc.MinFrameInterval), 2))
                        Debug.WriteLine("Set Format size: " & infoheader.BmiHeader.ImageSize.ToString)
                        If media.subType = MediaSubType.RGB24 Then
                            Debug.WriteLine("Set Format media subtype: RGB24")

                        ElseIf media.subType = MediaSubType.UYVY Then
                            Debug.WriteLine("Set Format media subtype: UYVY")
                        ElseIf media.subType = MediaSubType.YUY2 Then
                            Debug.WriteLine("Set Format media subtype: YUY2")
                        ElseIf media.subType = MediaSubType.YUYV Then
                            Debug.WriteLine("Set Format media subtype: YUYV")
                        Else
                            Debug.WriteLine("Set Format media subtype: " & media.subType.ToString)
                        End If

                        ' MsgBox("Set Format : " & String.Format("{0}x{1}", infoheader.BmiHeader.Width, infoheader.BmiHeader.Height))

                        If (_SelectedCap IsNot Nothing AndAlso _SelectedCap.Height = infoheader.BmiHeader.Height AndAlso _SelectedCap.Width = infoheader.BmiHeader.Width AndAlso _SelectedCap.subType.ToString = media.subType.ToString) Then

                            infoheader.AvgTimePerFrame = (10000000 / _SelectedCap.AvgFrameRate)

                            Marshal.StructureToPtr(infoheader, media.formatPtr, True)
                            hr = Me.ConfigStreamCaps.SetFormat(media)
                            Debug.WriteLine("SetFormat return an error : " & DsError.GetErrorText(hr))
                            DsError.ThrowExceptionForHR(hr)

                            Me.VideoInfoHeader = Marshal.PtrToStructure(media.formatPtr, GetType(DirectShowLib.VideoInfoHeader))

                        End If


                        hr = sampGrabber.SetMediaType(media)
                        Debug.WriteLine("Create media type for sample grabber failed : " & DsError.GetErrorText(hr))
                        DsError.ThrowExceptionForHR(hr)
                        ''Delete the media type when you are done.
                        Marshal.DestroyStructure(pScc, GetType(VideoStreamConfigCaps))
                        Marshal.DestroyStructure(media.formatPtr, GetType(VideoInfoHeader))
                        DsUtils.FreeAMMediaType(media)
                        media = Nothing
                        infoheader = Nothing
                        scc = Nothing
                        'Exit For
                    Next
                End If
                Debug.WriteLine("Count = " & Count.ToString & " Size = " & Size.ToString)

                hr = Me.GraphBuilder.AddFilter(sourceFilter, "Video Capture")
                Debug.WriteLine("Add capture filter to our graph : " & DsError.GetErrorText(hr))
                DsError.ThrowExceptionForHR(hr)



                If Me._OutputType = WindowType.PictureBox Then
                    Dim mymedia As AMMediaType = New AMMediaType

                    mymedia.majorType = MediaType.Video
                    mymedia.subType = MediaSubType.RGB24
                    mymedia.formatType = FormatType.VideoInfo
                    hr = sampGrabber.SetMediaType(mymedia)
                    Debug.WriteLine("Create media type for sample grabber failed : " & DsError.GetErrorText(hr))
                    DsError.ThrowExceptionForHR(hr)




                    'videoRendererFilter = CType(New VideoRenderer(), IBaseFilter)
                    'hr = Me.GraphBuilder.AddFilter(videoRendererFilter, "Renderer")
                    'Debug.WriteLine("AddFilter renderer to our graph : " & DsError.GetErrorText(hr))
                    'DsError.ThrowExceptionForHR(hr)


                    If _SelectedCap.subType = MediaSubType.NV12 Or _SelectedCap.subType = MediaSubType.YV12 Or _SelectedCap.subType = MediaSubType.I420 Then
                        hr = Me.GraphBuilder.AddFilter(RendererDirectDraw, "DirectDraw Renderer")
                        Debug.WriteLine("AddFilter renderer to our graph : " & DsError.GetErrorText(hr))
                        DsError.ThrowExceptionForHR(hr)
                    Else
                        hr = Me.GraphBuilder.AddFilter(Renderer, "Renderer")
                        Debug.WriteLine("AddFilter renderer to our graph : " & DsError.GetErrorText(hr))
                        DsError.ThrowExceptionForHR(hr)
                    End If





                    If UseAnalyzerFilterValue AndAlso baseINOGENIFlt IsNot Nothing Then



                        Dim PinInogeniInput As IPin = Nothing
                        hr = Me.CaptureGraphBuilder.FindPin(Me.baseINOGENIFlt, PinDirection.Input, Nothing, Nothing, False, 0, PinInogeniInput)
                        Debug.WriteLine("FindPin of Inogeni analyser to our graph : " & DsError.GetErrorText(hr))
                        DsError.ThrowExceptionForHR(hr)

                        hr = Me.GraphBuilder.Connect(PinInput, PinInogeniInput)
                        Debug.WriteLine("Connect to Inogeni Analyzer to our graph : " & DsError.GetErrorText(hr))
                        DsError.ThrowExceptionForHR(hr)


                        If _SelectedCap.subType = MediaSubType.YUY2 Then
                            Dim PinInogeniout As IPin = Nothing
                            hr = Me.CaptureGraphBuilder.FindPin(Me.baseINOGENIFlt, PinDirection.Output, Nothing, Nothing, False, 0, PinInogeniout)
                            Debug.WriteLine("FindPin of Inogeni analyser to our graph : " & DsError.GetErrorText(hr))
                            DsError.ThrowExceptionForHR(hr)

                            Dim PinsaavidecompInput As IPin = Nothing
                            hr = Me.CaptureGraphBuilder.FindPin(Me.AVIDecompressorFilter, PinDirection.Input, Nothing, Nothing, False, 0, PinsaavidecompInput)
                            Debug.WriteLine("FindPin of Inogeni analyser to our graph : " & DsError.GetErrorText(hr))
                            DsError.ThrowExceptionForHR(hr)

                            hr = Me.GraphBuilder.Connect(PinInogeniout, PinsaavidecompInput)
                            Debug.WriteLine("Connect to Inogeni Analyzer to our graph : " & DsError.GetErrorText(hr))
                            DsError.ThrowExceptionForHR(hr)

                            hr = Me.CaptureGraphBuilder.RenderStream(Nothing, MediaType.Video, AVIDecompressorFilter, Nothing, Renderer)
                        ElseIf _SelectedCap.subType = MediaSubType.NV12 Or _SelectedCap.subType = MediaSubType.YV12 Or _SelectedCap.subType = MediaSubType.I420 Then
                            Dim PinInogeniout As IPin = Nothing
                            hr = Me.CaptureGraphBuilder.FindPin(Me.baseINOGENIFlt, PinDirection.Output, Nothing, Nothing, False, 0, PinInogeniout)
                            Debug.WriteLine("FindPin of Inogeni analyser to our graph : " & DsError.GetErrorText(hr))
                            DsError.ThrowExceptionForHR(hr)

                            Dim PinRendererInput As IPin = Nothing
                            hr = Me.CaptureGraphBuilder.FindPin(RendererDirectDraw, PinDirection.Input, Nothing, Nothing, False, 0, PinRendererInput)
                            Debug.WriteLine("FindPin of Inogeni analyser to our graph : " & DsError.GetErrorText(hr))
                            DsError.ThrowExceptionForHR(hr)


                            hr = Me.GraphBuilder.Connect(PinInogeniout, PinRendererInput)
                            Debug.WriteLine("Connect to Inogeni Analyzer to our graph : " & DsError.GetErrorText(hr))
                            DsError.ThrowExceptionForHR(hr)

                            hr = Me.GraphBuilder.Render(PinInput)
                        Else
                            hr = Me.CaptureGraphBuilder.RenderStream(Nothing, MediaType.Video, baseINOGENIFlt, Nothing, Renderer)
                        End If







                        'hr = Me.CaptureGraphBuilder.RenderStream(Nothing, MediaType.Video, baseGrabFlt, Nothing, videoRendererFilter)
                        ' hr = Me.CaptureGraphBuilder.RenderStream(Nothing, MediaType.Video, AVIDecompressorFilter, Nothing, RendererOldDD)

                        Debug.WriteLine("Render the preview pin on the video capture filter : " & DsError.GetErrorText(hr))
                        DsError.ThrowExceptionForHR(hr)



                    Else

                        ' hr = Me.CaptureGraphBuilder.RenderStream(PinCategory.Capture, MediaType.Video, sourceFilter, baseGrabFlt, videoRendererFilter)
                        If _SelectedCap.subType = MediaSubType.NV12 Or _SelectedCap.subType = MediaSubType.YV12 Or _SelectedCap.subType = MediaSubType.I420 Then
                            Dim PinRendererInput As IPin = Nothing
                            hr = Me.CaptureGraphBuilder.FindPin(RendererDirectDraw, PinDirection.Input, Nothing, Nothing, False, 0, PinRendererInput)
                            Debug.WriteLine("FindPin of Inogeni analyser to our graph : " & DsError.GetErrorText(hr))
                            DsError.ThrowExceptionForHR(hr)


                            hr = Me.GraphBuilder.Connect(PinInput, PinRendererInput)
                            Debug.WriteLine("Connect to Inogeni Analyzer to our graph : " & DsError.GetErrorText(hr))
                            DsError.ThrowExceptionForHR(hr)

                            hr = Me.GraphBuilder.Render(PinInput)

                        Else
                            'hr = Me.CaptureGraphBuilder.RenderStream(PinCategory.Capture, MediaType.Video, sourceFilter, Nothing, Renderer)
                            hr = Me.CaptureGraphBuilder.RenderStream(PinCategory.Capture, MediaType.Video, sourceFilter, baseGrabFlt, Renderer)
                        End If
                        Debug.WriteLine("Render the preview pin on the video capture filter : " & DsError.GetErrorText(hr))
                        DsError.ThrowExceptionForHR(hr)
                    End If

                    If _SelectedCap.subType = MediaSubType.YUY2 Then
                        mymedia = New AMMediaType()
                        hr = sampGrabber.GetConnectedMediaType(mymedia)
                        Debug.WriteLine("GetConnectedMediaType : " & DsError.GetErrorText(hr))
                        DsError.ThrowExceptionForHR(hr)

                        If ((mymedia.formatType <> FormatType.VideoInfo) Or (mymedia.formatPtr = IntPtr.Zero)) Then
                            MsgBox("Unknown Grabber Media Format")
                        End If


                        Me.VideoInfoHeader = Marshal.PtrToStructure(mymedia.formatPtr, GetType(DirectShowLib.VideoInfoHeader))



                        Marshal.FreeCoTaskMem(mymedia.formatPtr)
                        mymedia.formatPtr = IntPtr.Zero

                        hr = sampGrabber.SetBufferSamples(True)
                        Debug.WriteLine("SetBufferSamples : " & DsError.GetErrorText(hr))
                        DsError.ThrowExceptionForHR(hr)
                        hr = sampGrabber.SetOneShot(False)
                        Debug.WriteLine("SetOneShot : " & DsError.GetErrorText(hr))
                        DsError.ThrowExceptionForHR(hr)
                        hr = sampGrabber.SetCallback(Nothing, 0)
                        Debug.WriteLine("SetCallback : " & DsError.GetErrorText(hr))
                        DsError.ThrowExceptionForHR(hr)
                    End If


                Else
                    Dim mymedia As AMMediaType = New AMMediaType
                    If sampGrabber IsNot Nothing Then
                        mymedia.majorType = MediaType.Video
                        mymedia.subType = MediaSubType.RGB24
                        mymedia.formatType = FormatType.VideoInfo
                        hr = sampGrabber.SetMediaType(mymedia)
                        Debug.WriteLine("Create media type for sample grabber failed : " & DsError.GetErrorText(hr))
                        DsError.ThrowExceptionForHR(hr)
                    End If



                    videoRendererFilter = CType(New VideoRenderer(), IBaseFilter)
                    hr = Me.GraphBuilder.AddFilter(videoRendererFilter, "Renderer")
                    Debug.WriteLine("AddFilter renderer to our graph : " & DsError.GetErrorText(hr))
                    DsError.ThrowExceptionForHR(hr)




                    If _SelectedCap.subType = MediaSubType.NV12 Or _SelectedCap.subType = MediaSubType.YV12 Or _SelectedCap.subType = MediaSubType.I420 Then
                        hr = Me.GraphBuilder.AddFilter(RendererDirectDraw, "DirectDraw Renderer")
                        Debug.WriteLine("AddFilter renderer to our graph : " & DsError.GetErrorText(hr))
                        DsError.ThrowExceptionForHR(hr)
                    Else
                        hr = Me.GraphBuilder.AddFilter(Renderer, "Renderer")
                        Debug.WriteLine("AddFilter renderer to our graph : " & DsError.GetErrorText(hr))
                        DsError.ThrowExceptionForHR(hr)
                    End If


                    If UseAnalyzerFilterValue AndAlso baseINOGENIFlt IsNot Nothing Then



                        Dim PinInogeniInput As IPin = Nothing
                        hr = Me.CaptureGraphBuilder.FindPin(Me.baseINOGENIFlt, PinDirection.Input, Nothing, Nothing, False, 0, PinInogeniInput)
                        Debug.WriteLine("FindPin of Inogeni analyser to our graph : " & DsError.GetErrorText(hr))
                        DsError.ThrowExceptionForHR(hr)



                        hr = Me.GraphBuilder.Connect(PinInput, PinInogeniInput)
                        Debug.WriteLine("Connect to Inogeni Analyzer to our graph : " & DsError.GetErrorText(hr))
                        DsError.ThrowExceptionForHR(hr)


                        If _SelectedCap.subType = MediaSubType.YUY2 Then
                            Dim PinInogeniout As IPin = Nothing
                            hr = Me.CaptureGraphBuilder.FindPin(Me.baseINOGENIFlt, PinDirection.Output, Nothing, Nothing, False, 0, PinInogeniout)
                            Debug.WriteLine("FindPin of Inogeni analyser to our graph : " & DsError.GetErrorText(hr))
                            DsError.ThrowExceptionForHR(hr)

                            Dim PinsaavidecompInput As IPin = Nothing
                            hr = Me.CaptureGraphBuilder.FindPin(Me.AVIDecompressorFilter, PinDirection.Input, Nothing, Nothing, False, 0, PinsaavidecompInput)
                            Debug.WriteLine("FindPin of Inogeni analyser to our graph : " & DsError.GetErrorText(hr))
                            DsError.ThrowExceptionForHR(hr)

                            hr = Me.GraphBuilder.Connect(PinInogeniout, PinsaavidecompInput)
                            Debug.WriteLine("Connect to Inogeni Analyzer to our graph : " & DsError.GetErrorText(hr))
                            DsError.ThrowExceptionForHR(hr)

                            hr = Me.CaptureGraphBuilder.RenderStream(Nothing, MediaType.Video, AVIDecompressorFilter, Nothing, Renderer)
                        ElseIf _SelectedCap.subType = MediaSubType.NV12 Or _SelectedCap.subType = MediaSubType.YV12 Or _SelectedCap.subType = MediaSubType.I420 Then
                            Dim PinInogeniout As IPin = Nothing
                            hr = Me.CaptureGraphBuilder.FindPin(Me.baseINOGENIFlt, PinDirection.Output, Nothing, Nothing, False, 0, PinInogeniout)
                            Debug.WriteLine("FindPin of Inogeni analyser to our graph : " & DsError.GetErrorText(hr))
                            DsError.ThrowExceptionForHR(hr)

                            Dim PinRendererInput As IPin = Nothing
                            hr = Me.CaptureGraphBuilder.FindPin(RendererDirectDraw, PinDirection.Input, Nothing, Nothing, False, 0, PinRendererInput)
                            Debug.WriteLine("FindPin of Inogeni analyser to our graph : " & DsError.GetErrorText(hr))
                            DsError.ThrowExceptionForHR(hr)


                            hr = Me.GraphBuilder.Connect(PinInogeniout, PinRendererInput)
                            Debug.WriteLine("Connect to Inogeni Analyzer to our graph : " & DsError.GetErrorText(hr))
                            DsError.ThrowExceptionForHR(hr)

                            hr = Me.GraphBuilder.Render(PinInput)
                        Else
                            hr = Me.CaptureGraphBuilder.RenderStream(Nothing, MediaType.Video, baseINOGENIFlt, Nothing, Renderer)
                        End If



                    Else
                        If _SelectedCap.subType = MediaSubType.NV12 Or _SelectedCap.subType = MediaSubType.YV12 Or _SelectedCap.subType = MediaSubType.I420 Then
                            Dim PinRendererInput As IPin = Nothing
                            hr = Me.CaptureGraphBuilder.FindPin(RendererDirectDraw, PinDirection.Input, Nothing, Nothing, False, 0, PinRendererInput)
                            Debug.WriteLine("FindPin of Inogeni analyser to our graph : " & DsError.GetErrorText(hr))
                            DsError.ThrowExceptionForHR(hr)


                            hr = Me.GraphBuilder.Connect(PinInput, PinRendererInput)
                            Debug.WriteLine("Connect to Inogeni Analyzer to our graph : " & DsError.GetErrorText(hr))
                            DsError.ThrowExceptionForHR(hr)

                            hr = Me.GraphBuilder.Render(PinInput)
                        Else
                            ' hr = Me.CaptureGraphBuilder.RenderStream(PinCategory.Capture, MediaType.Video, sourceFilter, baseGrabFlt, videoRendererFilter)
                            hr = Me.CaptureGraphBuilder.RenderStream(PinCategory.Capture, MediaType.Video, sourceFilter, baseGrabFlt, Renderer)
                            Debug.WriteLine("Render the preview pin on the video capture filter : " & DsError.GetErrorText(hr))
                            DsError.ThrowExceptionForHR(hr)
                        End If


                    End If

                    ' hr = Me.GraphBuilder.Render(PinInput)
                    ' Debug.WriteLine("Render the preview pin on the video capture filter : " & DsError.GetErrorText(hr))
                    'DsError.ThrowExceptionForHR(hr)
                End If

                If Me._OutputType = WindowType.SeparatedWindow Then
                    Dim scc As New VideoStreamConfigCaps
                    Dim pScc As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(scc))
                    Dim media As DirectShowLib.AMMediaType = Nothing
                    Dim infoheader As DirectShowLib.VideoInfoHeader = Nothing
                    Dim myobj As Object = scc

                    'Getformat

                    hr = Me.ConfigStreamCaps.GetFormat(media)
                    Debug.WriteLine("GetNumberOfCapabilities return an error : " & DsError.GetErrorText(hr))
                    DsError.ThrowExceptionForHR(hr)
                    Marshal.PtrToStructure(pScc, scc)

                    '' copy out the videoinfoheader
                    Me.VideoInfoHeader = Marshal.PtrToStructure(media.formatPtr, GetType(DirectShowLib.VideoInfoHeader))


                    ''Examine the format, and possibly use it. 
                    'Debug.WriteLine("Set Format: " & infoheader.BmiHeader.Width.ToString & "x" & infoheader.BmiHeader.Height.ToString & "@" & Math.Round((10000000 / scc.MinFrameInterval), 2))




                    'hr = sampGrabber.SetMediaType(media)
                    'Debug.WriteLine("Create media type for sample grabber failed : " & DsError.GetErrorText(hr))
                    'DsError.ThrowExceptionForHR(hr)
                    ''Delete the media type when you are done.
                    Marshal.DestroyStructure(pScc, GetType(VideoStreamConfigCaps))
                    Marshal.DestroyStructure(media.formatPtr, GetType(VideoInfoHeader))
                    DsUtils.FreeAMMediaType(media)
                    media = Nothing
                    infoheader = Nothing
                    scc = Nothing
                End If

                Marshal.ReleaseComObject(sourceFilter)
                RaiseEvent OnGraphBuilded(Me, True)
                Return True
            Catch ex As Exception
                MessageBox.Show("An unrecoverable error has occurred.With error : " & ex.ToString)
                Return False
            End Try
        End Function
        Public Function GetFormat() As VideoCapabilitiesDef
            Dim hr As Integer = 0
            Dim Cap As VideoCapabilitiesDef = Nothing
            Dim index As Integer = 0
            Try


                If SourceFilter Is Nothing OrElse PinInput Is Nothing Then
                    Debug.WriteLine("Unable to find device")
                    ' MsgBox("Unable to lock on " & FilterName & "!")
                    Return Nothing
                End If

                'Get the IAMStreamconfig interface from the pin
                ConfigStreamCaps = CType(PinInput, IAMStreamConfig)

                Dim Count As Integer = 0
                Dim Size As Integer = 0
                If ConfigStreamCaps Is Nothing Then
                    '  MessageBox.Show("unable to query the IAMStreamConfig interface of the pin!")
                    Return Nothing
                End If








                Dim media As DirectShowLib.AMMediaType = Nothing
                hr = Me.ConfigStreamCaps.GetFormat(media)
                Debug.WriteLine("GetFormat return an error : " & DsError.GetErrorText(hr))
                DsError.ThrowExceptionForHR(hr)



                '' copy out the videoinfoheader
                Dim infoheader As DirectShowLib.VideoInfoHeader = Nothing
                infoheader = New VideoInfoHeader
                Marshal.PtrToStructure(media.formatPtr, infoheader)


                Debug.WriteLine("Get Format: " & infoheader.BmiHeader.Width.ToString & "x" & infoheader.BmiHeader.Height.ToString)
                Debug.WriteLine("Get Format size: " & infoheader.BmiHeader.ImageSize.ToString)
                Cap = New VideoCapabilitiesDef
                Cap.Height = infoheader.BmiHeader.Height
                Cap.Width = infoheader.BmiHeader.Width
                Cap.subType = media.subType
                If infoheader.AvgTimePerFrame > 0 Then
                    Cap.AvgFrameRate = Math.Round((10000000 / infoheader.AvgTimePerFrame), 2)
                Else
                    Cap.AvgFrameRate = 0
                End If



                Return Cap
            Catch ex As Exception
                MessageBox.Show("An unrecoverable error has occurred.With error : " & ex.ToString)
                Return Nothing
            End Try
        End Function
        Public Function GetFilterCapabilities(ByVal FilterName As String) As VideoCapabilitiesDef()
            Dim hr As Integer = 0
            Dim sourceFilter As IBaseFilter = Nothing
            Dim Caps() As VideoCapabilitiesDef = Nothing
            Dim index As Integer = 0
            Try

                sourceFilter = FindCapDevice(FilterName)
                If sourceFilter Is Nothing Then
                    Debug.WriteLine("Unable to find device")
                    ' MsgBox("Unable to lock on " & FilterName & "!")
                    Return Nothing
                End If

                'Ok find the pind
                hr = Me.CaptureGraphBuilder.FindPin(sourceFilter, PinDirection.Output, DirectShowLib.PinCategory.Capture, CLSID_MEdiaType_Video, False, 0, PinInput)
                Debug.WriteLine("Get the filter output pin : " & DsError.GetErrorText(hr))
                DsError.ThrowExceptionForHR(hr)

                ' Query the stream capabalities
                If PinInput Is Nothing Then
                    '  MessageBox.Show("Input pin cannot be created!")
                    Return Nothing
                End If




                'Get the IAMStreamconfig interface from the pin
                Try
                    ConfigStreamCaps = CType(PinInput, IAMStreamConfig)
                Catch ex As Exception
                    Debug.WriteLine("ooops the inferface IAMStreamconfig")
                End Try
                If ConfigStreamCaps Is Nothing Then
                    Return Nothing
                End If

                Dim Count As Integer = 0
                Dim Size As Integer = 0
                If ConfigStreamCaps Is Nothing Then
                    '  MessageBox.Show("unable to query the IAMStreamConfig interface of the pin!")
                    Return Nothing
                End If

                hr = Me.ConfigStreamCaps.GetNumberOfCapabilities(Count, Size)
                Debug.WriteLine("GetNumberOfCapabilities return an error : " & DsError.GetErrorText(hr))
                DsError.ThrowExceptionForHR(hr)


                If Size = VIDEO_STREAM_CONFIG_CAPS Then

                    For i As Integer = 0 To (Count - 1)
                        Dim scc As New VideoStreamConfigCaps
                        Dim pScc As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(scc))
                        Dim media As DirectShowLib.AMMediaType = Nothing
                        Dim infoheader As DirectShowLib.VideoInfoHeader = Nothing
                        Dim myobj As Object = scc

                        hr = Me.ConfigStreamCaps.GetStreamCaps(i, media, pScc)
                        Debug.WriteLine("GetNumberOfCapabilities return an error : " & DsError.GetErrorText(hr))
                        DsError.ThrowExceptionForHR(hr)

                        Debug.WriteLine(media.subType.ToString)


                        Marshal.PtrToStructure(pScc, scc)

                        '' copy out the videoinfoheader
                        infoheader = New VideoInfoHeader
                        Marshal.PtrToStructure(media.formatPtr, infoheader)


                        ''Examine the format, and possibly use it. 
                        If scc.MinFrameInterval > 0 Then
                            Debug.WriteLine("Set Format: " & infoheader.BmiHeader.Width.ToString & "x" & infoheader.BmiHeader.Height.ToString & "@" & Math.Round((10000000 / scc.MinFrameInterval), 2))
                        Else

                            Debug.WriteLine("Set Format: " & infoheader.BmiHeader.Width.ToString & "x" & infoheader.BmiHeader.Height.ToString)
                        End If

                        Debug.WriteLine("Set Format size: " & infoheader.BmiHeader.ImageSize.ToString)
                        If media.subType = MediaSubType.RGB24 Then
                            Debug.WriteLine("Set Format media subtype: RGB24")

                        ElseIf media.subType = MediaSubType.UYVY Then
                            Debug.WriteLine("Set Format media subtype: UYVY")
                        ElseIf media.subType = MediaSubType.YUY2 Then
                            Debug.WriteLine("Set Format media subtype: YUY2")
                        ElseIf media.subType = MediaSubType.YUYV Then
                            Debug.WriteLine("Set Format media subtype: YUYV")
                        Else
                            Debug.WriteLine("Set Format media subtype: " & media.subType.ToString)
                        End If

                        ' MsgBox("Set Format " & DsToString.MediaSubTypeToString(media.subType) & " : " & String.Format("{0}x{1}", infoheader.BmiHeader.Width, infoheader.BmiHeader.Height))
                        If infoheader.BmiHeader.Height > 0 AndAlso infoheader.BmiHeader.Width > 0 Then
                            ReDim Preserve Caps(index)
                            Caps(index) = New VideoCapabilitiesDef
                            Caps(index).Height = infoheader.BmiHeader.Height
                            Caps(index).Width = infoheader.BmiHeader.Width
                            Caps(index).subType = media.subType
                            If scc.MinFrameInterval > 0 Then
                                Caps(index).MaxFrameRate = Math.Round((10000000 / scc.MinFrameInterval), 2)
                            Else
                                Caps(index).MaxFrameRate = 0
                            End If
                            If scc.MaxFrameInterval > 0 Then
                                Caps(index).MinFrameRate = Math.Round((10000000 / scc.MaxFrameInterval), 2)
                            Else
                                Caps(index).MinFrameRate = 0
                            End If
                            If infoheader.AvgTimePerFrame > 0 Then
                                Caps(index).AvgFrameRate = Math.Round((10000000 / infoheader.AvgTimePerFrame), 2)
                            Else
                                Caps(index).AvgFrameRate = 0
                            End If

                            index += 1
                        End If


                        ''Delete the media type when you are done.
                        Marshal.DestroyStructure(pScc, GetType(VideoStreamConfigCaps))
                        Marshal.DestroyStructure(media.formatPtr, GetType(VideoInfoHeader))
                        DsUtils.FreeAMMediaType(media)
                        media = Nothing
                        infoheader = Nothing
                        scc = Nothing
                        'Exit For
                    Next
                End If
                Debug.WriteLine("Count = " & Count.ToString & " Size = " & Size.ToString)
                Return Caps
            Catch ex As Exception
                MessageBox.Show("An unrecoverable error has occurred.With error : " & ex.ToString)
                Return Nothing
            End Try
        End Function

        Public Shared Function TestFilterSupportIAMStreamConfig(ByVal FilterName As String) As Boolean
            Dim hr As Integer = 0
            Dim sourceFilter As IBaseFilter = Nothing
            Dim CaptureGraphBuilder As ICaptureGraphBuilder2 = Nothing
            Dim PinInput As IPin = Nothing
            Dim ConfigStreamCaps As IAMStreamConfig = Nothing
            Try
                sourceFilter = FindCapDevice(FilterName)
                If sourceFilter Is Nothing Then
                    ' The filter is not valid!
                    Return False
                End If

                'Ok find the pind
                CaptureGraphBuilder = CType(New CaptureGraphBuilder2, ICaptureGraphBuilder2)
                hr = CaptureGraphBuilder.FindPin(sourceFilter, PinDirection.Output, DirectShowLib.PinCategory.Capture, CLSID_MEdiaType_Video, False, 0, PinInput)
                Debug.WriteLine("Get the filter output pin : " & DsError.GetErrorText(hr))
                DsError.ThrowExceptionForHR(hr)

                ' Query the stream capabalities
                If PinInput Is Nothing Then
                    ' The filter is not valid!
                    Return False
                End If

                'Get the IAMStreamconfig interface from the pin to confirm the filter is OK!
                Try
                    ConfigStreamCaps = CType(PinInput, IAMStreamConfig)
                Catch ex As Exception
                    Debug.WriteLine("ooops the inferface IAMStreamconfig")
                    Return False
                End Try
                If ConfigStreamCaps Is Nothing Then
                    Return False
                End If

                'If we get here
                Return True
            Catch ex As Exception
                Return False
            Finally
                If CaptureGraphBuilder IsNot Nothing Then
                    Marshal.ReleaseComObject(CaptureGraphBuilder) : CaptureGraphBuilder = Nothing
                End If
                If PinInput IsNot Nothing Then
                    Marshal.ReleaseComObject(PinInput) : PinInput = Nothing
                End If
                If ConfigStreamCaps IsNot Nothing Then
                    Marshal.ReleaseComObject(ConfigStreamCaps) : ConfigStreamCaps = Nothing
                End If
            End Try
        End Function



        ''' <summary>
        ''' Get input source video format
        ''' </summary>
        ''' <param name="PixelFormat"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GetInogeniAnalyzerResolution(ByRef PixelFormat As VideoFormat) As Boolean
            Dim hr As HRESULT = S_FALSE
            Try
                Dim FrameRate As Long = 0
                Dim temp As Long = 0
                PixelFormat.FrameRate = 0
                PixelFormat.Height = 0
                PixelFormat.Width = 0
                If InogeniAnalyzer IsNot Nothing Then
                    hr = InogeniAnalyzer.GetFrameRate(FrameRate)
                    If hr <> S_OK Then
                        Return False
                    End If

                    If FrameRate >= 800000 Then
                        'Frame cannot be detected
                        PixelFormat.FrameRate = -1
                    Else
                        PixelFormat.FrameRate = CSng(FrameRate / 100)
                    End If

                    'Get detected reolution
                    hr = InogeniAnalyzer.GetFormat(PixelFormat.Width, PixelFormat.Height)
                    If hr <> S_OK Then
                        Return False
                    End If

                    Return True
                End If
                Return False
            Catch ex As Exception
                ' MessageBox.Show("GetSourceResolution an unrecoverable error has occurred.With error : " & ex.ToString)
                Return False
            End Try
        End Function

        ''' <summary>
        ''' Get input source video format
        ''' </summary>
        ''' <param name="PTSStart"></param>
        ''' <param name="PTSEnd"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GetInogeniAnalyzerPTS(ByRef PTSStart As Int64, ByRef PTSEnd As Int64) As Boolean
            Dim hr As HRESULT = S_FALSE
            Try

                PTSStart = 0
                PTSEnd = 0
                If InogeniAnalyzer IsNot Nothing Then
                    hr = InogeniAnalyzer.GetPresentationTimeStampStart(PTSStart)
                    If hr <> S_OK Then
                        Return False
                    End If



                    'Get detected reolution
                    hr = InogeniAnalyzer.GetPresentationTimeStampStop(PTSEnd)
                    If hr <> S_OK Then
                        Return False
                    End If


                    Return True
                End If
                Return False
            Catch ex As Exception
                ' MessageBox.Show("GetSourceResolution an unrecoverable error has occurred.With error : " & ex.ToString)
                Return False
            End Try
        End Function





        ''' <summary>
        ''' Get input source video format
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getInogeniAnalyzerPTSRegenerationMode(ByRef Enabled As Boolean) As Boolean
            Dim hr As HRESULT = S_FALSE
            Dim Data As Byte = 0
            Try
                Enabled = False
                If InogeniAnalyzer IsNot Nothing Then
                    hr = InogeniAnalyzer.GetPTSRegenerationMode(Data)
                    If hr <> S_OK Then
                        Return False
                    End If
                    If Data Then
                        Enabled = True
                    End If
                    Return True
                End If

                Return False
            Catch ex As Exception
                ' MessageBox.Show("GetSourceResolution an unrecoverable error has occurred.With error : " & ex.ToString)
                Return False
            End Try
        End Function

        ''' <summary>
        ''' Get input source video format
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function SetInogeniAnalyzerPTSRegenerationMode(ByVal Enabled As Boolean) As Boolean
            Dim hr As HRESULT = S_FALSE
            Dim Data As Byte = 0
            Try

                If Enabled Then
                    Data = 1
                End If
                If InogeniAnalyzer IsNot Nothing Then
                    hr = InogeniAnalyzer.SetPTSRegenerationMode(Data)
                    If hr <> S_OK Then
                        Return False
                    End If
                    Return True
                End If

                Return False
            Catch ex As Exception
                ' MessageBox.Show("GetSourceResolution an unrecoverable error has occurred.With error : " & ex.ToString)
                Return False
            End Try
        End Function

        ''' <summary>
        ''' Get Configured Video version
        ''' </summary>
        ''' <param name="PixelFormat"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overridable Function GetVideoResolution(ByRef PixelFormat As VideoFormat) As Boolean
            Dim hr As HRESULT = S_FALSE
            Try
                Dim FrameRate As Long = 0
                Dim temp As Long = 0
                PixelFormat.FrameRate = 0
                PixelFormat.Height = 0
                PixelFormat.Width = 0

                If VideoInfoHeader.AvgTimePerFrame > 0 Then
                    PixelFormat.FrameRate = CSng(CSng(10000000) / CSng(VideoInfoHeader.AvgTimePerFrame))
                End If

                'Get configured resolution from connection header
                PixelFormat.Height = VideoInfoHeader.BmiHeader.Height
                PixelFormat.Width = VideoInfoHeader.BmiHeader.Width
                Return True
            Catch ex As Exception
                ' MessageBox.Show("GetFPGAVersion an unrecoverable error has occurred.With error : " & ex.ToString)
                Return False
            End Try
        End Function

        Public Function BuildRecorderGraph(ByVal FilterName As String, ByVal FileName As String) As Boolean
            Dim hr As Integer = 0
            Dim sourceFilter As IBaseFilter = Nothing
            Dim asfwriter As IBaseFilter = Nothing

            Try
                GetInterfaces()

                _VideoCapabilities = Nothing
                _VideoCapabilities = (GetFilterCapabilities(FilterName))
                If _SelectedCap Is Nothing Then
                    _SelectedCap = GetFormat()
                End If
                If _SelectedCap Is Nothing AndAlso _VideoCapabilities IsNot Nothing AndAlso _VideoCapabilities.Length > 0 Then
                    'select one by defaylt
                    _SelectedCap = _VideoCapabilities(0)
                End If

                rot = New DsROTEntry(Me.FilterGraph)

                hr = Me.CaptureGraphBuilder.SetFiltergraph(Me.FilterGraph) 'Specifies filter graph "graphbuilder" for the capture graph builder "captureGraphBuilder" to use.
                Debug.WriteLine("Attach the filter graph to the capture graph : " & DsError.GetErrorText(hr))
                DsError.ThrowExceptionForHR(hr)

                sourceFilter = FindCaptureDevice(FilterName)

                Me.FilterGraph.AddFilter(sourceFilter, "Video Capture")
                Debug.WriteLine("Add capture filter to our graph : " & DsError.GetErrorText(hr))
                DsError.ThrowExceptionForHR(hr)

                asfwriter = ConfigAsf(Me.CaptureGraphBuilder, FileName)

                hr = Me.CaptureGraphBuilder.RenderStream(Nothing, Nothing, sourceFilter, Nothing, asfwriter)
                Debug.WriteLine("RenderStream : " & DsError.GetErrorText(hr))
                DsError.ThrowExceptionForHR(hr)

                Me.MediaControlRecorder = CType(Me.FilterGraph, IMediaControl)
                Return True
            Catch ex As Exception
                MessageBox.Show("An unrecoverable error has occurred.With error : " & ex.ToString)
                Return False
            Finally
                If sourceFilter IsNot Nothing Then
                    Marshal.ReleaseComObject(sourceFilter)
                End If
                If asfwriter IsNot Nothing Then
                    Marshal.ReleaseComObject(asfwriter)
                End If

            End Try
        End Function
        Private Function ConfigAsf(ByVal capGraph As ICaptureGraphBuilder2, ByVal szOutputFileName As String) As IBaseFilter
            Dim pTmpSink As IFileSinkFilter = Nothing
            Dim asfWriter As IBaseFilter = Nothing
            Dim hr As Integer = 0
            Try

                hr = capGraph.SetOutputFileName(MediaSubType.Asf, szOutputFileName, asfWriter, pTmpSink)
                Debug.WriteLine("SetOutputFileName : " & DsError.GetErrorText(hr))
                Marshal.ThrowExceptionForHR(hr)


                Dim lConfig As IConfigAsfWriter = asfWriter

                ' Windows Media Video 8 for Dial-up Modem (No audio, 56 Kbps)
                ' READ THE README for info about using guids
                Dim cat As Guid = New Guid(&H6E2A6955, &H81DF, &H4943, &HBA, &H50, &H68, &HA9, &H86, &HA7, &H8, &HF6)

                hr = lConfig.ConfigureFilterUsingProfileGuid(cat)

                Debug.WriteLine("ConfigureFilterUsingProfileGuid : " & DsError.GetErrorText(hr))
                Marshal.ThrowExceptionForHR(hr)

            Catch ex As Exception
                MessageBox.Show("An unrecoverable error has occurred.With error : " & ex.ToString)
            Finally
                Marshal.ReleaseComObject(pTmpSink)
            End Try

            Return asfWriter
        End Function

        Public Sub Pause(ByVal pause As Boolean)
            If pause Then
                MediaControl.Pause()
            Else
                MediaControl.Run()
            End If
        End Sub

        Public Sub Play(ByVal play As Boolean)
            Dim hr As Integer = 0
            Try
                If play Then
                    If Me.CurrentState <> PlayState.Running Then
                        SetupVideoWindow()

                        If Not (rot Is Nothing) Then
                            rot.Dispose()
                            rot = Nothing
                        End If
                        rot = New DsROTEntry(Me.GraphBuilder)

                        Dim success As Boolean = False ' Alert UGLY patch, the run will sometimes generate an exception
                        Dim RetryCount As Integer = 10
                        Do
                            Try
                                hr = Me.MediaControl.Run()
                                Debug.WriteLine("Start capturing video data : " & DsError.GetErrorText(hr))
                                DsError.ThrowExceptionForHR(hr)
                                success = True
                            Catch ex As Exception
                                Debug.WriteLine("Start capturing video data exception: " & ex.Message)
                                success = False
                            End Try
                            RetryCount -= 1
                        Loop While RetryCount > 0 AndAlso success = False





                        Me.CurrentState = PlayState.Running
                        Debug.WriteLine("The currentstate : " & Me.CurrentState.ToString)
                    End If

                Else
                    If Me.CurrentState <> PlayState.Stopped Then
                        hr = Me.MediaControl.Stop
                        Debug.WriteLine("Start capturing video data : " & DsError.GetErrorText(hr))
                        DsError.ThrowExceptionForHR(hr)

                        Me.CurrentState = PlayState.Stopped
                        Debug.WriteLine("The currentstate : " & Me.CurrentState.ToString)
                    End If

                End If
            Catch ex As Exception
                MessageBox.Show("An unrecoverable error has occurred.With error : " & ex.ToString)
            End Try
        End Sub
        Public Sub Record(ByVal play As Boolean)
            Dim hr As Integer = 0
            Try
                If play Then
                    If Me.CurrentState <> PlayState.Running Then

                        '  rot = New DsROTEntry(Me.GraphBuilder)

                        hr = Me.MediaControlRecorder.Run()
                        Debug.WriteLine("Start capturing video data : " & DsError.GetErrorText(hr))
                        DsError.ThrowExceptionForHR(hr)

                        Me.CurrentState = PlayState.Running
                        Debug.WriteLine("The currentstate : " & Me.CurrentState.ToString)
                    End If

                Else
                    If Me.CurrentState <> PlayState.Stopped Then
                        hr = Me.MediaControlRecorder.Stop
                        Debug.WriteLine("Start capturing video data : " & DsError.GetErrorText(hr))
                        DsError.ThrowExceptionForHR(hr)

                        Me.CurrentState = PlayState.Stopped
                        Debug.WriteLine("The currentstate : " & Me.CurrentState.ToString)
                    End If

                End If
            Catch ex As Exception
                MessageBox.Show("An unrecoverable error has occurred.With error : " & ex.ToString)
            End Try
        End Sub
        Public Shared Function ListVideoCaptureDevice() As String(,)
            Try
                Dim Array(,) As String = Nothing
                Dim NumberOfElement As Integer = 0
                Dim VideoCaptureDevices As DsDevice() = DsDevice.GetDevicesOfCat(DirectShowLib.FilterCategory.VideoInputDevice)
                If VideoCaptureDevices IsNot Nothing Then
                    For Each device As DsDevice In VideoCaptureDevices
                        If device IsNot Nothing Then

                            'Ok test the filter is not valid
                            If TestFilterSupportIAMStreamConfig(device.Name) Then
                                If NumberOfElement = 0 Then
                                    ReDim Array(1, 0)
                                Else
                                    ReDim Preserve Array(1, NumberOfElement)
                                End If

                                Array(0, NumberOfElement) = device.Name
                                Array(1, NumberOfElement) = device.GetPropBagValue("CLSID")
                                NumberOfElement += 1
                            End If

                        End If
                    Next
                End If

                Return Array
            Catch ex As Exception
                MessageBox.Show("An unrecoverable error has occurred.With error : " & ex.ToString)
                Return Nothing
            End Try
        End Function

        Public Function isInogeniAnalyserWorking() As Boolean
            Try
                If baseINOGENIFlt IsNot Nothing Then
                    Return True
                End If
                Return False
            Catch ex As Exception
                Return False
            End Try
        End Function
        Protected Sub GetInterfaces()
            Dim hr As Integer = 0
            Me.GraphBuilder = CType(New FilterGraph, IGraphBuilder)
            Me.CaptureGraphBuilder = CType(New CaptureGraphBuilder2, ICaptureGraphBuilder2)
            Me.MediaControl = CType(Me.GraphBuilder, IMediaControl)
            Me.VideoWindow = CType(Me.GraphBuilder, IVideoWindow)
            Me.MediaEventEx = CType(Me.GraphBuilder, IMediaEventEx)

            'Recorder
            Me.FilterGraph = CType(New FilterGraph, IFilterGraph2)



            'INOGENI Analyser
            If UseAnalyzerFilterValue Then
                Dim comtype1 As Type = Nothing
                Dim comobj1 As Object = Nothing
                Try
                    comtype1 = Type.GetTypeFromCLSID(CLSID_INOGENIFilterAnalyzer)
                    comobj1 = Activator.CreateInstance(comtype1)
                    Me.InogeniAnalyzer = CType(comobj1, ISourcePropertiesInogeni)
                    Me.baseINOGENIFlt = CType(Me.InogeniAnalyzer, IBaseFilter)
                Catch ex As Exception
                    'MsgBox("Does not find INOGENI Analyzer!")
                    Debug.WriteLine("Does not find INOGENI Analyzer!")
                End Try
            Else
                Me.InogeniAnalyzer = Nothing
                Me.baseINOGENIFlt = Nothing
            End If






            'AVI decompressor
            Dim comtype2 As Type = Nothing
            Dim comobj2 As Object = Nothing
            Try
                comtype2 = Type.GetTypeFromCLSID(CLSID_AVIDec)
                comobj2 = Activator.CreateInstance(comtype2)
                Me.AViDecFlt = CType(comobj2, IBaseFilter)
            Catch ex As Exception
                'MsgBox("Does not find AVI Decompressor!")
                Debug.WriteLine("Does not find AVI Decompressor!")
            End Try


            ' renderer
            Dim comtype3 As Type = Nothing
            Dim comobj3 As Object = Nothing
            Try
                comtype3 = Type.GetTypeFromCLSID(CLSID_Renderer)
                comobj3 = Activator.CreateInstance(comtype3)
                Me.RendererIQualProp = CType(comobj3, IQualProp)
                Me.Renderer = CType(comobj3, IBaseFilter)
            Catch ex As Exception
                'MsgBox("Does not find AVI Decompressor!")
                Debug.WriteLine("Does not find the old video renderer!")
            End Try

            'directdraw renderer
            Dim comtype4 As Type = Nothing
            Dim comobj4 As Object = Nothing
            Try
                comtype4 = Type.GetTypeFromCLSID(CLSID_RendererVMR9)
                comobj4 = Activator.CreateInstance(comtype4)
                Me.RendererIQualProDirectDraw = CType(comobj4, IQualProp)
                Me.RendererDirectDraw = CType(comobj4, IBaseFilter)
            Catch ex As Exception
                'MsgBox("Does not find AVI Decompressor!")
                Debug.WriteLine("Does not find the old video renderer!")
            End Try
            
            If Me._OutputType = WindowType.PictureBox Then
                Dim comtype As Type = Type.GetTypeFromCLSID(DShowNET.Clsid.SampleGrabber)
                Dim comobj As Object = Activator.CreateInstance(comtype)
                Me.sampGrabber = CType(comobj, ISampleGrabber)
                Me.baseGrabFlt = CType(Me.sampGrabber, IBaseFilter)

                'TODO replace that after window creation
                hr = Me.MediaEventEx.SetNotifyWindow(Me._CapturePictureBox.Handle, WM_GRAPHNOTIFY, IntPtr.Zero) 'This method designates a window as the recipient of messages generated by or sent to the current DirectShow object
                DsError.ThrowExceptionForHR(hr) 'ThrowExceptionForHR is a wrapper for Marshal.ThrowExceptionForHR, but additionally provides descriptions for any DirectShow specific error messages.If the hr value is not a fatal error, no exception will be thrown:
                Debug.WriteLine("I started Sub Get interfaces , the result is : " & DsError.GetErrorText(hr))
            ElseIf Me._OutputType = WindowType.SeparatedWindow Then


                'TODO replace that after window creation
                hr = Me.MediaEventEx.SetNotifyWindow(Me.Handle, WM_GRAPHNOTIFY, IntPtr.Zero) 'This method designates a window as the recipient of messages generated by or sent to the current DirectShow object
                DsError.ThrowExceptionForHR(hr) 'ThrowExceptionForHR is a wrapper for Marshal.ThrowExceptionForHR, but additionally provides descriptions for any DirectShow specific error messages.If the hr value is not a fatal error, no exception will be thrown:
                Debug.WriteLine("I started Sub Get interfaces , the result is : " & DsError.GetErrorText(hr))
            End If


        End Sub

        Public Overridable Function FindCaptureDevice(ByVal FilterName As String) As IBaseFilter
            Try
                Debug.WriteLine("Start the Sub FindCaptureDevice")
                Dim hr As Integer = 0
                Dim classEnum As IEnumMoniker = Nothing
                Dim moniker As IMoniker() = New IMoniker(0) {}
                Dim source As Object = Nothing
                Dim devEnum As ICreateDevEnum = CType(New CreateDevEnum, ICreateDevEnum)
                hr = devEnum.CreateClassEnumerator(FilterCategory.VideoInputDevice, classEnum, 0)
                Debug.WriteLine("Create an enumerator for the video capture devices : " & DsError.GetErrorText(hr))
                DsError.ThrowExceptionForHR(hr)
                Marshal.ReleaseComObject(devEnum)
                If classEnum Is Nothing Then
                    Throw New ApplicationException("No video capture device was detected.\r\n\r\n" & _
                                   "This sample requires a video capture device, such as a USB WebCam,\r\n" & _
                                   "to be installed and working properly.  The sample will now close.")
                End If
                Dim DeviceFound As Boolean = False
                While classEnum.Next(moniker.Length, moniker, IntPtr.Zero) = 0 And DeviceFound = False
                    Dim iid As Guid = GetType(IBaseFilter).GUID
                    Dim bagobj As Object = Nothing
                    Dim bag As IPropertyBag = Nothing
                    Dim bagguid As Guid = GetType(IPropertyBag).GUID
                    moniker(0).BindToStorage(Nothing, Nothing, bagguid, bagobj)
                    bag = CType(bagobj, IPropertyBag)
                    If bag IsNot Nothing Then
                        Dim val As Object = ""
                        Dim FilterFriendlyname As String = ""
                        hr = bag.Read("FriendlyName", val, Nothing)
                        If (hr <> 0) Then
                            Debug.WriteLine("Error in getting filter name : " & DsError.GetErrorText(hr))
                        End If
                        If CType(val, String) = FilterName Then
                            'We have a winner
                            ' Check if that's a video input filter
                            Me._FilterName = FilterName
                            val = ""
                            hr = bag.Read("CLSID", val, Nothing)
                            If (hr <> 0) Then
                                Debug.WriteLine("Error in getting filter CLSID : " & DsError.GetErrorText(hr))
                            End If
                            CLSID = New Guid(CType(val, String))

                            'Grab the filter to return to capture function
                            Try
                                moniker(0).BindToObject(Nothing, Nothing, iid, source)
                                DeviceFound = True
                            Catch ex As Exception
                                Debug.WriteLine("Error in BindToObject : " & DsError.GetErrorText(hr))
                                DeviceFound = False
                            End Try


                            If DeviceFound Then
                                'Ok we need to get a pointer on the filter and the property interface
                                SourceFilter = CType(source, IBaseFilter)
                            End If
                        End If
                    End If
                    bag = Nothing
                    Marshal.ReleaseComObject(bagobj)
                End While

                If moniker(0) IsNot Nothing Then
                    Marshal.ReleaseComObject(moniker(0))
                End If
                If classEnum IsNot Nothing Then
                    Marshal.ReleaseComObject(classEnum)
                End If
                Return CType(source, IBaseFilter)
            Catch ex As Exception
                MessageBox.Show("An unrecoverable error has occurred.With error : " & ex.ToString)
                Return Nothing
            End Try
        End Function

        Public Shared Function FindCapDevice(ByVal FilterName As String) As IBaseFilter
            Try
                Debug.WriteLine("Start the Sub FindCaptureDevice")
                Dim hr As Integer = 0
                Dim classEnum As IEnumMoniker = Nothing
                Dim moniker As IMoniker() = New IMoniker(0) {}
                Dim source As Object = Nothing
                Dim devEnum As ICreateDevEnum = CType(New CreateDevEnum, ICreateDevEnum)
                hr = devEnum.CreateClassEnumerator(FilterCategory.VideoInputDevice, classEnum, 0)
                Debug.WriteLine("Create an enumerator for the video capture devices : " & DsError.GetErrorText(hr))
                DsError.ThrowExceptionForHR(hr)
                Marshal.ReleaseComObject(devEnum)
                If classEnum Is Nothing Then
                    Throw New ApplicationException("No video capture device was detected.\r\n\r\n" & _
                                   "This sample requires a video capture device, such as a USB WebCam,\r\n" & _
                                   "to be installed and working properly.  The sample will now close.")
                End If
                Dim DeviceFound As Boolean = False
                While classEnum.Next(moniker.Length, moniker, IntPtr.Zero) = 0 And DeviceFound = False
                    Dim iid As Guid = GetType(IBaseFilter).GUID
                    Dim bagobj As Object = Nothing
                    Dim bag As IPropertyBag = Nothing
                    Dim bagguid As Guid = GetType(IPropertyBag).GUID
                    moniker(0).BindToStorage(Nothing, Nothing, bagguid, bagobj)
                    bag = CType(bagobj, IPropertyBag)
                    If bag IsNot Nothing Then
                        Dim val As Object = ""
                        Dim FilterFriendlyname As String = ""
                        hr = bag.Read("FriendlyName", val, Nothing)
                        If (hr <> 0) Then
                            Debug.WriteLine("Error in getting filter name : " & DsError.GetErrorText(hr))
                        End If
                        If CType(val, String) = FilterName Then
                            'We have a winner
                            'Grab the filter to return to capture function
                            Try
                                moniker(0).BindToObject(Nothing, Nothing, iid, source)
                                DeviceFound = True
                            Catch ex As Exception
                                Debug.WriteLine("Error in BindToObject : " & DsError.GetErrorText(hr))
                                DeviceFound = False
                            End Try

                        End If
                    End If
                    bag = Nothing
                    Marshal.ReleaseComObject(bagobj)
                End While

                If moniker(0) IsNot Nothing Then
                    Marshal.ReleaseComObject(moniker(0))
                End If
                If classEnum IsNot Nothing Then
                    Marshal.ReleaseComObject(classEnum)
                End If
                Return CType(source, IBaseFilter)
            Catch ex As Exception
                MessageBox.Show("An unrecoverable error has occurred.With error : " & ex.ToString)
                Return Nothing
            End Try
        End Function



        Public Sub SetupVideoWindow()
            Dim hr As Integer = 0

            If Me._OutputType = WindowType.SeparatedWindow Then

                'set the video window to be a child of the main window
                'putowner : Sets the owning parent window for the video playback window. 
                hr = Me.VideoWindow.put_Owner(Me.Handle)
                DsError.ThrowExceptionForHR(hr)

                hr = Me.VideoWindow.put_WindowStyle(WindowStyle.Child Or WindowStyle.ClipChildren)
                DsError.ThrowExceptionForHR(hr)

                'Use helper function to position video window in client rect of main application window
                RaiseEvent OnWindowSizeRequested(Me)

            ElseIf Me._OutputType = WindowType.PictureBox Then
                If Me.CapturePictureBox Is Nothing Then
                    MsgBox("The capturebox is not defined yet")
                    Return
                End If

                'set the video window to be a child of the main window
                'putowner : Sets the owning parent window for the video playback window. 
                hr = Me.VideoWindow.put_Owner(Me.CapturePictureBox.Handle)
                DsError.ThrowExceptionForHR(hr)

                hr = Me.VideoWindow.put_WindowStyle(WindowStyle.Child Or WindowStyle.ClipChildren)
                DsError.ThrowExceptionForHR(hr)

                'Use helper function to position video window in client rect of main application window
                RaiseEvent OnWindowSizeRequested(Me)
            End If

            'Make the video window visible, now that it is properly positioned
            'put_visible : This method changes the visibility of the video window. 
            hr = Me.VideoWindow.put_Visible(OABool.True)
            DsError.ThrowExceptionForHR(hr)
        End Sub

        Public Overridable Sub DisconnectTheGraph()
            _FilterName = ""
            RaiseEvent OnGraphBuilded(Me, False)
            '//stop previewing data
            If Not (Me.MediaControl Is Nothing) Then
                Me.MediaControl.StopWhenReady()
            End If

            Me.CurrentState = PlayState.Stopped

            '//stop recieving events
            If Not (Me.MediaEventEx Is Nothing) Then
                Me.MediaEventEx.SetNotifyWindow(IntPtr.Zero, WM_GRAPHNOTIFY, IntPtr.Zero)
            End If

            '// Relinquish ownership (IMPORTANT!) of the video window.
            '// Failing to call put_Owner can lead to assert failures within
            '// the video renderer, as it still assumes that it has a valid
            '// parent window.
            If Not (Me.VideoWindow Is Nothing) Then
                Me.VideoWindow.put_Visible(OABool.False)
                Me.VideoWindow.put_Owner(IntPtr.Zero)
            End If

            ' // Remove filter graph from the running object table
            If Not (rot Is Nothing) Then
                rot.Dispose()
                rot = Nothing
            End If

            'Ok disconnect the source filter


            FilterGraph.Disconnect(PinInput)

        End Sub

        Public Overridable Sub closeinterfaces()
            _FilterName = ""
            RaiseEvent OnGraphBuilded(Me, False)
            '//stop previewing data
            If Not (Me.MediaControl Is Nothing) Then
                Me.MediaControl.StopWhenReady()
            End If

            Me.CurrentState = PlayState.Stopped

            '//stop recieving events
            If Not (Me.MediaEventEx Is Nothing) Then
                Me.MediaEventEx.SetNotifyWindow(IntPtr.Zero, WM_GRAPHNOTIFY, IntPtr.Zero)
            End If

            '// Relinquish ownership (IMPORTANT!) of the video window.
            '// Failing to call put_Owner can lead to assert failures within
            '// the video renderer, as it still assumes that it has a valid
            '// parent window.
            If Not (Me.VideoWindow Is Nothing) Then
                Me.VideoWindow.put_Visible(OABool.False)
                Me.VideoWindow.put_Owner(IntPtr.Zero)
            End If

            ' // Remove filter graph from the running object table
            If Not (rot Is Nothing) Then
                rot.Dispose()
                rot = Nothing
            End If


            If Me.FilterGraph IsNot Nothing Then
                Marshal.ReleaseComObject(Me.FilterGraph) : Me.FilterGraph = Nothing
            End If
            '// Release DirectShow interfaces
            If Me.MediaControl IsNot Nothing Then
                Marshal.ReleaseComObject(Me.MediaControl) : Me.MediaControl = Nothing
            End If
            If Me.MediaEventEx IsNot Nothing Then
                Marshal.ReleaseComObject(Me.MediaEventEx) : Me.MediaEventEx = Nothing
            End If
            If Me.VideoWindow IsNot Nothing Then
                Marshal.ReleaseComObject(Me.VideoWindow) : Me.VideoWindow = Nothing
            End If
            If Me.GraphBuilder IsNot Nothing Then
                Marshal.ReleaseComObject(Me.GraphBuilder) : Me.GraphBuilder = Nothing
            End If
            If Me.CaptureGraphBuilder IsNot Nothing Then
                Marshal.ReleaseComObject(Me.CaptureGraphBuilder) : Me.CaptureGraphBuilder = Nothing
            End If

            If Me.ConfigStreamCaps IsNot Nothing Then
                Marshal.ReleaseComObject(Me.ConfigStreamCaps) : Me.ConfigStreamCaps = Nothing
            End If

            If Me.MediaControlRecorder IsNot Nothing Then
                Marshal.ReleaseComObject(Me.MediaControlRecorder) : Me.MediaControlRecorder = Nothing
            End If






            If baseGrabFlt IsNot Nothing Then
                Marshal.ReleaseComObject(Me.baseGrabFlt) : Me.baseGrabFlt = Nothing
            End If
            If sampGrabber IsNot Nothing Then
                Marshal.ReleaseComObject(Me.sampGrabber) : Me.sampGrabber = Nothing
            End If

            If Me.baseINOGENIFlt IsNot Nothing Then
                Marshal.ReleaseComObject(Me.baseINOGENIFlt) : Me.baseINOGENIFlt = Nothing
            End If

            If Me.AViDecFlt IsNot Nothing Then
                Marshal.ReleaseComObject(Me.AViDecFlt) : Me.AViDecFlt = Nothing
            End If

            If Me.Renderer IsNot Nothing Then
                Marshal.ReleaseComObject(Me.Renderer) : Me.Renderer = Nothing
            End If

            If Me.RendererIQualProp IsNot Nothing Then
                Marshal.ReleaseComObject(Me.RendererIQualProp) : Me.RendererIQualProp = Nothing
            End If

            If Me.RendererDirectDraw IsNot Nothing Then
                Marshal.ReleaseComObject(Me.RendererDirectDraw) : Me.RendererDirectDraw = Nothing
            End If

            If Me.RendererIQualProDirectDraw IsNot Nothing Then
                Marshal.ReleaseComObject(Me.RendererIQualProDirectDraw) : Me.RendererIQualProDirectDraw = Nothing
            End If

            If Me.InogeniAnalyzer IsNot Nothing Then
                Marshal.ReleaseComObject(Me.InogeniAnalyzer) : Me.InogeniAnalyzer = Nothing
            End If

            If videoRendererFilter IsNot Nothing Then

                Marshal.ReleaseComObject(Me.videoRendererFilter) : Me.videoRendererFilter = Nothing
            End If

            If AVIDecompressorFilter IsNot Nothing Then
                Marshal.ReleaseComObject(Me.AVIDecompressorFilter) : Me.AVIDecompressorFilter = Nothing
            End If

            If ColorSpaceConverterFilter IsNot Nothing Then
                Marshal.ReleaseComObject(Me.ColorSpaceConverterFilter) : Me.ColorSpaceConverterFilter = Nothing
            End If

            If SourceFilter IsNot Nothing Then
                Marshal.ReleaseComObject(Me.SourceFilter) : Me.SourceFilter = Nothing
            End If


            If PinInput IsNot Nothing Then
                Marshal.ReleaseComObject(Me.PinInput) : Me.PinInput = Nothing
            End If
        End Sub

        Public Function ShowRendererPropertyPage(ByVal o As Control) As Boolean
            Dim specifyPropertyPages As ISpecifyPropertyPages = Nothing
            Dim propertyPageName As String = ""

            If (videoRendererFilter IsNot Nothing) Then
                specifyPropertyPages = CType(videoRendererFilter, ISpecifyPropertyPages)
                propertyPageName = "Video Renderer"
            Else
                If Renderer IsNot Nothing Then
                    specifyPropertyPages = CType(Renderer, ISpecifyPropertyPages)
                    propertyPageName = "Video Renderer"
                End If
            End If

            Dim pp As PropertyPage = New PropertyPage(propertyPageName, specifyPropertyPages)
            If pp IsNot Nothing Then
                pp.show(o)
                'pp.show(Me)
                pp.Dispose()
            End If
        End Function

        Public Function isRendererHasPropertyPage() As Boolean
            Dim specifyPropertyPages As ISpecifyPropertyPages = Nothing
            Try
                If (videoRendererFilter IsNot Nothing) Then
                    specifyPropertyPages = CType(videoRendererFilter, ISpecifyPropertyPages)
                    Marshal.ReleaseComObject(specifyPropertyPages) : specifyPropertyPages = Nothing
                    Return True
                Else
                    If Renderer IsNot Nothing Then
                        specifyPropertyPages = CType(Renderer, ISpecifyPropertyPages)
                        Marshal.ReleaseComObject(specifyPropertyPages) : specifyPropertyPages = Nothing
                        Return True
                    End If
                End If
                Return False
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function ShowsourcePropertyPage(ByVal o As Control) As Boolean
            Try
                Dim specifyPropertyPages As ISpecifyPropertyPages = Nothing
                Dim propertyPageName As String = ""

                If (SourceFilter IsNot Nothing) Then
                    specifyPropertyPages = CType(SourceFilter, ISpecifyPropertyPages)
                    propertyPageName = "Video Renderer"
                End If

                Dim pp As PropertyPage = New PropertyPage(propertyPageName, specifyPropertyPages)
                If pp IsNot Nothing Then
                    pp.show(o)
                    pp.Dispose()
                End If
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function isSourceHasPropertyPage() As Boolean
            Try
                Dim specifyPropertyPages As ISpecifyPropertyPages = Nothing
                Dim propertyPageName As String = ""

                If (SourceFilter IsNot Nothing) Then
                    specifyPropertyPages = CType(SourceFilter, ISpecifyPropertyPages)
                    propertyPageName = "Video Renderer"
                Else
                    Return False
                End If

                Dim pp As PropertyPage = New PropertyPage(propertyPageName, specifyPropertyPages)
                If pp IsNot Nothing Then

                    pp.Dispose()
                    Marshal.ReleaseComObject(specifyPropertyPages) : specifyPropertyPages = Nothing
                    pp = Nothing
                    Return True
                End If
                Return False
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function ShowpinPropertyPage(ByVal o As Control) As Boolean
            Try
                Dim specifyPropertyPages As ISpecifyPropertyPages = Nothing
                Dim propertyPageName As String = ""

                If (PinInput IsNot Nothing) Then
                    specifyPropertyPages = CType(PinInput, ISpecifyPropertyPages)
                    propertyPageName = "Video Renderer"
                End If

                Dim pp As PropertyPage = New PropertyPage(propertyPageName, specifyPropertyPages)
                If pp IsNot Nothing Then

                    pp.show(o)
                    pp.Dispose()
                End If
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function
        Public Function isPinHasPropertyPage() As Boolean
            Try
                Dim specifyPropertyPages As ISpecifyPropertyPages = Nothing
                Dim propertyPageName As String = ""

                If (PinInput IsNot Nothing) Then
                    specifyPropertyPages = CType(PinInput, ISpecifyPropertyPages)
                    propertyPageName = "Video Renderer"
                Else
                    Return False
                End If

                Dim pp As PropertyPage = New PropertyPage(propertyPageName, specifyPropertyPages)
                If pp IsNot Nothing Then
                    Marshal.ReleaseComObject(specifyPropertyPages) : specifyPropertyPages = Nothing
                    pp.Dispose()
                    pp = Nothing
                    Return True
                End If
                Return False
            Catch ex As Exception
                Return False
            End Try
        End Function

#If 0 Then
    /// <summary>
		/// Show property page of object 
		/// </summary>
		/// <param name="filter"></param>
		/// <param name="o"></param>
		/// <returns></returns>
		public bool ShowPropertyPage(int filter, Control o)
		{
			ISpecifyPropertyPages specifyPropertyPages =  null;
			String propertyPageName = "";
        Switch(Filter)
			{
				case 1:
					if(this.videoRendererFilter != null)
					{
						specifyPropertyPages = this.videoRendererFilter as ISpecifyPropertyPages;
						propertyPageName = "Video Renderer";
					}
					break;
				case 2:
					if(this.deInterlaceFilter != null)
					{
						specifyPropertyPages = this.deInterlaceFilter as ISpecifyPropertyPages;
						propertyPageName = "De-Interlacer";
					}
					break;
				default:
					break;
			}
			DirectShowPropertyPage PropertyPage = new DirectShowPropertyPage(propertyPageName, specifyPropertyPages);
			if(PropertyPage != null)
			{
				PropertyPage.Show(o);
				PropertyPage.Dispose();
				return true;
			}
			return false;
		}
#End If
        Public Sub ChangePreviewState(ByVal showVideo As Boolean)
            Dim hr As Integer = 0
            '// If the media control interface isn't ready, don't call it
            If Me.MediaControl Is Nothing Then
                Debug.WriteLine("MediaControl is nothing")
                Return
            End If

            If showVideo = True Then
                If Not (Me.CurrentState = PlayState.Running) Then
                    Debug.WriteLine("Start previewing video data")
                    hr = Me.MediaControl.Run
                    Me.CurrentState = PlayState.Running
                End If
            Else
                Debug.WriteLine("Stop previewing video data")
                hr = Me.MediaControl.StopWhenReady
                Me.CurrentState = PlayState.Stopped
            End If
        End Sub

#End Region

#Region "Window preview handler"
        Public Sub VideoWindowResized(ByVal left As Integer, ByVal top As Integer, ByVal Width As Integer, ByVal Height As Integer)
            If Me._OutputType = WindowType.PictureBox AndAlso Me.VideoWindow IsNot Nothing Then
                Me.VideoWindow.SetWindowPosition(left, top, Width, Height)
            ElseIf Me._OutputType = WindowType.SeparatedWindow Then
                Me.VideoWindow.SetWindowPosition(left, top, Width, Height)
            End If
        End Sub
        Private Sub HandleGraphEvent()
            Dim hr As Integer = 0
            Dim evCode As EventCode
            Dim evParam1 As Integer
            Dim evParam2 As Integer
            Try
                If Me.MediaEventEx Is Nothing Then
                    Return
                End If
                While Me.MediaEventEx.GetEvent(evCode, evParam1, evParam2, 0) = 0
                    '// Free event parameters to prevent memory leaks associated with
                    '// event parameter data.  While this application is not interested
                    '// in the received events, applications should always process them.
                    hr = Me.MediaEventEx.FreeEventParams(evCode, evParam1, evParam2)
                    DsError.ThrowExceptionForHR(hr)

                    '// Insert event processing code here, if desired
                End While
            Catch ex As Exception

            End Try

        End Sub
        Public Sub Notify(ByRef m As Message)
            Select Case m.Msg
                Case WM_GRAPHNOTIFY
                    HandleGraphEvent()
            End Select
            Try
                If Not (Me.VideoWindow Is Nothing) Then
                    Me.VideoWindow.NotifyOwnerMessage(m.HWnd, m.Msg, m.WParam.ToInt32, m.LParam.ToInt32)
                End If
            Catch ex As Exception

            End Try

        End Sub

#End Region



    End Class
End Namespace