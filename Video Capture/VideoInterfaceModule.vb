﻿#Region "Options"
Option Explicit On
Option Strict Off
#End Region

#Region "Copyright"
'Dextera Labs Inc.
'(c) Copyright 2005-2013 Dextera Labs Inc.
'ALL RIGHTS RESERVED

'The software, source code and information contained herein are
'proprietary to, and comprise valuable intellectual property and
'trade secrets of Dextera Labs Inc.

'This software is furnished pursuant to a Source Code License
'Agreement between Dextera and the Licensee and may be used,
'copied, transmitted, and stored only by the Licensee in accordance
'with the terms of such license and with the inclusion of this
'copyright notice.

'This software, source code and information or any other copies
'thereof may not be provided or otherwise made available to any
'person not bound by the terms of the Source Code License Agreement.
#End Region

#Region "Imports"
Imports System.Runtime.InteropServices
#End Region

Module INOGENIInterfaceModule
    Public Enum HRESULT
        S_OK = 0
        S_FALSE = 1
        E_NOTIMPL = &H80004001
        E_INVALIDARG = &H80070057
        E_NOINTERFACE = &H80004002
        E_FAIL = &H80004005
        E_UNEXPECTED = &H8000FFFF
    End Enum
    Public ReadOnly CLSID_MEdiaType_Video As Guid = New Guid("73646976-0000-0010-8000-00AA00389B71")
    Public ReadOnly CLSID_UVC_VIDEO_DEVICE As Guid = New Guid("17CCA71B-ECD7-11D0-B908-00A0C9223196")

    Public ReadOnly CLSID_AVIDec As Guid = New Guid("CF49D4E0-1115-11ce-B03A-0020AF0BA770")

    Public ReadOnly CLSID_DirectDrawRenderer As Guid = New Guid("70E102B0-5556-11CE-97C0-00AA0055595A")
    Public ReadOnly CLSID_Renderer As Guid = New Guid("6BC1CFFA-8FC1-4261-AC22-CFB4CC38DB50")
    Public ReadOnly CLSID_RendererVMR9 As Guid = New Guid("51B4ABF3-748F-4E3B-A276-C828330E926A")

    Public ReadOnly CLSID_INOGENIFilterAnalyzer As Guid = New Guid("BEFCC67B-A371-4497-B4FC-E967F170CE60")
    Public ReadOnly CLSID_INOGENIFilterAnalyzerSourceProperties As Guid = New Guid("05BCD40C-E0F9-4b65-9431-6D12CB82D963")


    <ComImport(), System.Security.SuppressUnmanagedCodeSecurity(), Guid("05BCD40C-E0F9-4b65-9431-6D12CB82D963"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)> _
    Public Interface ISourcePropertiesInogeni
        ' <PreserveSig()> Function Gettest(ByRef ptest As Long) As HRESULT
        <PreserveSig()> Function GetProperty(ByRef pValue As Long) As HRESULT
        <PreserveSig()> Function GetFrameRate(ByRef pFrameRate As Long) As HRESULT
        <PreserveSig()> Function GetFormat(ByRef pWidth As Long, ByRef pHeight As Long) As HRESULT
        <PreserveSig()> Function GetPresentationTimeStampLength(ByRef pPresentionLenInt As ULong, ByRef pPresentionLenRest As ULong) As HRESULT
        <PreserveSig()> Function GetPresentationTimeStampStart(ByRef pPresentionTimeStart As Int64) As HRESULT
        <PreserveSig()> Function GetPresentationTimeStampStop(ByRef pPresentionTimeStop As Int64) As HRESULT
        <PreserveSig()> Function SetPTSRegenerationMode(ByVal Enable As Byte) As HRESULT
        <PreserveSig()> Function GetPTSRegenerationMode(ByRef pEnable As Byte) As HRESULT
    End Interface

End Module
