﻿

Public Class HIDCommand


#Region "Constant"
    Public Shared MAXIMUM_FX3_REGISTER_INDEX As Byte = 3

    Public Const MAXIMUM_SPI_READ_SIZE As Byte = 59
    Public Const MAXIMUM_SPI_WRITE_SIZE As Byte = 56
    Public Const MAXIMUM_FPGA_READ_SIZE As Byte = 58
    Public Const MAXIMUM_FPGA_WRITE_SIZE As Byte = 59
    Public Const MAXIMUM_FX3_ERROR_CODE As Byte = ErrorCode.HID_NO_RESPONSE_OR_INVALID - 1
    Public Const SIZE_SPI_EEPROM As ULong = &H80000

    Public Const EEPROM_FX3_BYTE_PER_PAGE As ULong = 256


    'OK SPI EEPROM Config

    ' SPIE EEPROM M25P40
    Public Const M25P40_FX3_FIRST_PAGE As ULong = 0
    Public Const M25P40_FX3_LAST_PAGE As ULong = 767
    Public Const M25P40_FX3_FIRST_SECTOR As ULong = 0
    Public Const M25P40_FX3_LAST_SECTOR As ULong = 2
    Public Const M25P40_FX3_SIZE As ULong = 196608 '//0x30000 or 192KByte or 1572864 bits
    Public Const M25P40_FX3_START_ADDRESS As ULong = &H0
    '// FPGA
    Public Const M25P40_FPGA_FIRST_PAGE As ULong = 768
    Public Const M25P40_FPGA_LAST_PAGE As ULong = 1792
    Public Const M25P40_FPGA_SIZE As ULong = 262144 '//0x40000 or 256KByte or 2097152 bits
    Public Const M25P40_FPGA_FIRST_SECTOR As ULong = 3
    Public Const M25P40_FPGA_LAST_SECTOR As ULong = 6
    Public Const M25P40_FPGA_START_ADDRESS As ULong = &H30000

    '// EDID
    Public Const M25P40_EDID_FIRST_PAGE As ULong = 1793
    Public Const M25P40_EDID_LAST_PAGE As ULong = 2047
    Public Const M25P40_EDID_SIZE As ULong = 256 '//0x100  or 2048 bits
    Public Const M25P40_EDID_SECTOR_SIZE As ULong = 65536 '0x10000  or 524288 bits
    Public Const M25P40_EDID_FIRST_SECTOR As ULong = 7
    Public Const M25P40_EDID_LAST_SECTOR As ULong = 7
    Public Const M25P40_EDID_START_ADDRESS As ULong = &H7FF00 '//The EDID is actually the last 256 bytes of the EEPROM

    ' SPIE EEPROM M25P80
    Public Const M25P80_FX3_FIRST_PAGE As ULong = 0
    Public Const M25P80_FX3_LAST_PAGE As ULong = 767
    Public Const M25P80_FX3_FIRST_SECTOR As ULong = 0
    Public Const M25P80_FX3_LAST_SECTOR As ULong = 2
    Public Const M25P80_FX3_SIZE As ULong = 196608 '//0x30000 or 192KByte or 1572864 bits
    Public Const M25P80_FX3_START_ADDRESS As ULong = &H0
    '// FPGA
    Public Const M25P80_FPGA_FIRST_PAGE As ULong = 768
    Public Const M25P80_FPGA_LAST_PAGE As ULong = 3584
    Public Const M25P80_FPGA_SIZE As ULong = 720896 '//0xB0000 or 704KByte or 5767168 bits
    Public Const M25P80_FPGA_FIRST_SECTOR As ULong = 3
    Public Const M25P80_FPGA_LAST_SECTOR As ULong = 13
    Public Const M25P80_FPGA_START_ADDRESS As ULong = &H30000
    '// EDID
    Public Const M25P80_EDID_FIRST_PAGE As ULong = 3585
    Public Const M25P80_EDID_LAST_PAGE As ULong = 4095
    Public Const M25P80_EDID_SIZE As ULong = 256 '//0x100  or 2048 bits
    Public Const M25P80_EDID_SECTOR_SIZE As ULong = 65536 '0x10000  or 524288 bits
    Public Const M25P80_EDID_FIRST_SECTOR As ULong = 15
    Public Const M25P80_EDID_LAST_SECTOR As ULong = 15
    Public Const M25P80_EDID_START_ADDRESS As ULong = &HFEF00 '//The EDID is actually the last 256 bytes of the EEPROM

    'HDMI Rx
    Public Const AVD7612_I2C_ADDRESS_IO As Byte = &H98
    Public Const AVD7612_I2C_ADDRESS_CEC As Byte = &H80
    Public Const AVD7612_I2C_ADDRESS_INFO As Byte = &H7C
    Public Const AVD7612_I2C_ADDRESS_DPLL As Byte = &H4C
    Public Const AVD7612_I2C_ADDRESS_KSV As Byte = &H64
    Public Const AVD7612_I2C_ADDRESS_EDID As Byte = &H6C
    Public Const AVD7612_I2C_ADDRESS_HDMI As Byte = &H68
    Public Const AVD7612_I2C_ADDRESS_CP As Byte = &H44

    'General Register
    Public Const REG_GENERAL_STOP_FPGA_DOWNLOAD As Byte = &H1

    'Line rate
    '/*1920*1080*/
    Public Const LINE_RATE_1080p60 As ULong = 67500
    Public Const LINE_RATE_1080p59 As ULong = 67430
    Public Const LINE_RATE_1080p50 As ULong = 56250
    Public Const LINE_RATE_1080p30 As ULong = 33750
    Public Const LINE_RATE_1080p29 As ULong = 33720
    Public Const LINE_RATE_1080p25 As ULong = 28130
    Public Const LINE_RATE_1080p24 As ULong = 27000
    Public Const LINE_RATE_1080p23 As ULong = 26980

    '/*1080i*/
    Public Const LINE_RATE_1080i60 As ULong = 33750
    Public Const LINE_RATE_1080i59 As ULong = 33720
    Public Const LINE_RATE_1080i50 As ULong = 28130

    '/* 720p */
    Public Const LINE_RATE_720p60 As ULong = 45000
    Public Const LINE_RATE_720p59 As ULong = 44960
    Public Const LINE_RATE_720p50 As ULong = 37500
    Public Const LINE_RATE_720p30 As ULong = 22500
    Public Const LINE_RATE_720p29 As ULong = 22480
    Public Const LINE_RATE_720p25 As ULong = 18750
    Public Const LINE_RATE_720p24 As ULong = 18000
    Public Const LINE_RATE_720p23 As ULong = 17990

#End Region

#Region "Struct"
    Public Class SPIEPROMLocation
#Region "Properties"
        Private DestinationValue As SPILocationDesc
        Public Property Dextination() As SPILocationDesc
            Get
                Return DestinationValue
            End Get
            Set(ByVal value As SPILocationDesc)
                DestinationValue = value
            End Set
        End Property

        Private _FirstPage As ULong = 0
        Public ReadOnly Property FirstPage As ULong
            Get
                Return _FirstPage
            End Get
        End Property
        Private _LastPage As ULong = 0
        Public ReadOnly Property LastPage As ULong
            Get
                Return _LastPage
            End Get
        End Property
        Private _FirstSector As ULong = 0
        Public ReadOnly Property FirstSector As ULong
            Get
                Return _FirstSector
            End Get
        End Property
        Private _LastSector As ULong = 0
        Public ReadOnly Property LastSector As ULong
            Get
                Return _LastSector
            End Get
        End Property
        Private _Size As ULong = 0
        Public ReadOnly Property Size As ULong
            Get
                Return _Size
            End Get
        End Property
        Private _FirstAddressActive As ULong = 0
        Public ReadOnly Property FirstAddressActive As ULong
            Get
                Return _FirstAddressActive
            End Get
        End Property
        Private _LocationStartAddress As ULong = 0
        Public ReadOnly Property LocationStartAddress As ULong
            Get
                Return _LocationStartAddress
            End Get
        End Property
#End Region

#Region "Constructor"
        Public Sub New(ByVal FirstPage As ULong, ByVal LastPage As ULong, ByVal FirstSector As ULong, ByVal LastSector As ULong, ByVal Size As ULong, ByVal FirstActiveAddress As ULong, ByVal Destination As SPILocationDesc)
            Me._FirstPage = FirstPage
            Me._LastPage = LastPage
            Me._FirstSector = FirstSector
            Me._LastSector = LastSector
            Me._Size = Size
            Me._FirstAddressActive = FirstActiveAddress
            Me._LocationStartAddress = Me._FirstPage * EEPROM_FX3_BYTE_PER_PAGE
            Me.DestinationValue = Destination
        End Sub
#End Region

    End Class
#End Region





#Region "Enums"
    Public Enum EEPROM
        M25P40
        M25P80
    End Enum
    Public Enum SPILocationDesc
        FX3
        FPGA
        EDID
    End Enum
    Public Enum HDMIRxMap
        IO
        CEC
        INFO
        DPLL
        KSV
        EDID
        HDMI
        CP
    End Enum
    Public Enum Command
        HID_I2C_WR_CMD = 0
        HID_I2C_RD_CMD
        HID_SPI_WR_CMD
        HID_SPI_RD_CMD
        HID_SPI_ERASE_CMD
        HID_SET_BOOT_CMD
        HID_FPGA_WR_CMD
        HID_FPGA_RD_CMD
        HID_DEBUG_WR
        HID_DEBUG_RD
    End Enum
    Public Enum ErrorCode
        HID_ACK = 0
        HID_BUSY
        HID_BAD_CMD_NUM
        HID_BAD_LENGTH
        HID_BAD_ADDR
        HID_TOO_BAD
        HID_BAD_DATA_LENGTH
        HID_SPI_FAIL
        HID_I2C_NO_ANSWER 'the target I2C device did not responde
        HID_I2C_COMM_FAILED ' Teh I2C communication failed the reason are not given
        HID_NOT_SUPPORTED ' The current command is valid but module is removed from code with define
        HID_REG_ACCES_NOT_SUPPORTED
        HID_FPGA_NOT_CONFIGURED
        'NN FX3 error code must come before this line
        HID_NO_RESPONSE_OR_INVALID ' this a error code for PC only: No response for current primitive
        HID_PC_EXCEPTION ' this a error code for PC only: No response for current primitive

    End Enum

    Public Enum SPIPortSelect
        SPI_EEPROM_PORT = 0
        SPI_FPGA_PORT
    End Enum

    Public Enum FX3Register
        VersionMajor
        VersionMinor
        SPIType
        FPGAVersion
        GeneralRegister
        REG_WIDTH_MSB
        REG_WIDTH_LSB
        REG_HEIGHT_MSB
        REG_HEIGHT_LSB
        REG_FRAME_RATE_MSB
        REG_FRAME_RATE_LSB
        REG_VIDEO_GENERAL      ' //bit 0 1 == video locked
        EDID_VERSION
    End Enum

    Public Enum FPGAStatRegister
        Version
        LineRatesLSB
        LineRatesMSB
        FixedAB
    End Enum

    Public Structure VideoFormat
        Public Locked As Byte
        Public ActiveLinesF0 As UShort
        Public ActivesPixel As UShort
    End Structure
#End Region

    Public Const TOLERANCELINE_RATE As ULong = 100
    Public Shared Function GetFrameRate(ByVal format As VideoFormat, ByVal Linerate As ULong) As ULong
        If (format.ActiveLinesF0 = 720) Then
            If ((Linerate) >= (LINE_RATE_720p60 - TOLERANCELINE_RATE)) AndAlso ((Linerate) <= (LINE_RATE_720p60 + TOLERANCELINE_RATE)) Then
                Return 6000
            ElseIf ((Linerate) >= (LINE_RATE_720p50 - TOLERANCELINE_RATE)) AndAlso ((Linerate) <= (LINE_RATE_720p50 + TOLERANCELINE_RATE)) Then
                Return 5000
            ElseIf ((Linerate) >= (LINE_RATE_720p30 - TOLERANCELINE_RATE)) AndAlso ((Linerate) <= (LINE_RATE_720p30 + TOLERANCELINE_RATE)) Then
                Return 3000
            ElseIf ((Linerate) >= (LINE_RATE_720p25 - TOLERANCELINE_RATE)) AndAlso ((Linerate) <= (LINE_RATE_720p25 + TOLERANCELINE_RATE)) Then
                Return 2500
            ElseIf ((Linerate) >= (LINE_RATE_720p24 - TOLERANCELINE_RATE)) AndAlso ((Linerate) <= (LINE_RATE_720p24 + TOLERANCELINE_RATE)) Then
                Return 2400
            End If
        ElseIf (format.ActiveLinesF0 = 1080) Then
            If ((Linerate) >= (LINE_RATE_1080p60 - TOLERANCELINE_RATE)) AndAlso ((Linerate) <= (LINE_RATE_1080p60 + TOLERANCELINE_RATE)) Then
                Return 6000
            ElseIf ((Linerate) >= (LINE_RATE_1080p50 - TOLERANCELINE_RATE)) AndAlso ((Linerate) <= (LINE_RATE_1080p50 + TOLERANCELINE_RATE)) Then
                Return 5000
            ElseIf ((Linerate) >= (LINE_RATE_1080p30 - TOLERANCELINE_RATE)) AndAlso ((Linerate) <= (LINE_RATE_1080p30 + TOLERANCELINE_RATE)) Then
                Return 3000
            ElseIf ((Linerate) >= (LINE_RATE_1080p25 - TOLERANCELINE_RATE)) AndAlso ((Linerate) <= (LINE_RATE_1080p25 + TOLERANCELINE_RATE)) Then
                Return 2500
            ElseIf ((Linerate) >= (LINE_RATE_1080p24 - TOLERANCELINE_RATE)) AndAlso ((Linerate) <= (LINE_RATE_1080p24 + TOLERANCELINE_RATE)) Then
                Return 2400
            End If
        ElseIf (format.ActiveLinesF0 = 540) Then
            If ((Linerate) >= (LINE_RATE_1080i60 - TOLERANCELINE_RATE)) AndAlso ((Linerate) <= (LINE_RATE_1080i60 + TOLERANCELINE_RATE)) Then
                Return 6000
            ElseIf ((Linerate) >= (LINE_RATE_1080i50 - TOLERANCELINE_RATE)) AndAlso ((Linerate) <= (LINE_RATE_1080i50 + TOLERANCELINE_RATE)) Then
                Return 5000
            End If
        End If
            Return 0
    End Function


#Region "Properties"

    Public ReadOnly FX3FirmwareInM25P40 As New SPIEPROMLocation(M25P40_FX3_FIRST_PAGE, M25P40_FX3_LAST_PAGE, M25P40_FX3_FIRST_SECTOR, M25P40_FX3_LAST_SECTOR, M25P40_FX3_SIZE, M25P40_FX3_START_ADDRESS, SPILocationDesc.FX3)
    Public ReadOnly FPGABitStreamInM25P40 As New SPIEPROMLocation(M25P40_FPGA_FIRST_PAGE, M25P40_FPGA_LAST_PAGE, M25P40_FPGA_FIRST_SECTOR, M25P40_FPGA_LAST_SECTOR, M25P40_FPGA_SIZE, M25P40_FPGA_START_ADDRESS, SPILocationDesc.FPGA)
    Public ReadOnly EDIDInM25P40 As New SPIEPROMLocation(M25P40_EDID_FIRST_PAGE, M25P40_EDID_LAST_PAGE, M25P40_EDID_FIRST_SECTOR, M25P40_EDID_LAST_SECTOR, M25P40_EDID_SIZE, M25P40_EDID_START_ADDRESS, SPILocationDesc.EDID)

    Public ReadOnly FX3FirmwareInM25P80 As New SPIEPROMLocation(M25P80_FX3_FIRST_PAGE, M25P80_FX3_LAST_PAGE, M25P80_FX3_FIRST_SECTOR, M25P80_FX3_LAST_SECTOR, M25P80_FX3_SIZE, M25P80_FX3_START_ADDRESS, SPILocationDesc.FX3)
    Public ReadOnly FPGABitStreamInM25P80 As New SPIEPROMLocation(M25P80_FPGA_FIRST_PAGE, M25P80_FPGA_LAST_PAGE, M25P80_FPGA_FIRST_SECTOR, M25P80_FPGA_LAST_SECTOR, M25P80_FPGA_SIZE, M25P80_FPGA_START_ADDRESS, SPILocationDesc.FPGA)
    Public ReadOnly EDIDInM25P80 As New SPIEPROMLocation(M25P80_EDID_FIRST_PAGE, M25P80_EDID_LAST_PAGE, M25P80_EDID_FIRST_SECTOR, M25P80_EDID_LAST_SECTOR, M25P80_EDID_SIZE, M25P80_EDID_START_ADDRESS, SPILocationDesc.EDID)


    Private _MessageID As Byte = 1
    Public ReadOnly Property MessageId As Byte
        Get
            Dim temp As Byte = _MessageID
            If _MessageID < 255 Then
                _MessageID += 1
            Else
                _MessageID = 1
            End If
            Return temp
        End Get
    End Property

    Private _Primitive() As Byte
#End Region

#Region "Constructor"
    Public Sub New()
        clear()
    End Sub
#End Region

#Region "methods Common"
    Public Sub clear()
        _Primitive = Nothing
    End Sub
#End Region

#Region "Generic command"

    ''' <summary>
    ''' Parse the answer from a get FX3 Register Command
    ''' </summary>
    ''' <param name="Answer">The array of byte receive from USB3 link</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ParseDeviceAnswer(ByVal Answer() As Byte, ByRef ReturnAnswerData() As Byte) As ErrorCode
        If Answer Is Nothing OrElse Answer.Length < 4 OrElse _Primitive Is Nothing OrElse _Primitive.Length < 3 Then
            Return ErrorCode.HID_NO_RESPONSE_OR_INVALID
        End If
        'Check Message ID
        If _Primitive(0) <> Answer(0) Then
            'Oops this not an answer from the current request
            Return ErrorCode.HID_NO_RESPONSE_OR_INVALID
        End If
        'Check the command it must be the same
        If Answer(1) <> _Primitive(1) Then
            'Oops this not an answer from the current request
            Return ErrorCode.HID_NO_RESPONSE_OR_INVALID
        End If


        If Answer(2) < 1 Then
            Return ErrorCode.HID_NO_RESPONSE_OR_INVALID
        End If


        'Error code expected
        If Answer(3) = ErrorCode.HID_ACK Then
            'Success
        ElseIf Answer(3) > MAXIMUM_FX3_ERROR_CODE Then
            'Invalid
            Return ErrorCode.HID_NO_RESPONSE_OR_INVALID
        Else
            'error
            Return CType(Answer(3), ErrorCode)
        End If
        Dim DataLen As Integer = Answer(2) - 1 ' ok remove the acknoledge byte

        'Ok we have an answer
        If DataLen Then
            'we have an answer to return
            ReDim ReturnAnswerData(DataLen - 1) ' Ok set to grab all data from device
            Array.Copy(Answer, 4, ReturnAnswerData, 0, DataLen)
        End If
        Return CType(Answer(3), ErrorCode)
    End Function
    ''' <summary>
    ''' Parse the answer from a get FX3 Register Command
    ''' </summary>
    ''' <param name="Answer">The array of byte receive from USB3 link</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ParseDeviceAnswer(ByVal Answer() As Byte) As ErrorCode
        Dim Data() As Byte = Nothing
        Return ParseDeviceAnswer(Answer, Data)
    End Function
#End Region

#Region "FX3 Registers"
    ''' <summary>
    ''' Build a Get FX3 Regiter command
    ''' </summary>
    ''' <param name="Register">The Register to get</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function BuildGetFX3Register(ByVal Register As Byte) As Byte()
        ReDim _Primitive(3) '4 bytes primitive
        _Primitive(0) = MessageId ' Message ID; manage by the object
        _Primitive(1) = Command.HID_DEBUG_RD ' command
        _Primitive(2) = 1 ' Always length of 1
        _Primitive(3) = Register 'Register
        Return _Primitive
    End Function
    ''' <summary>
    ''' Build a Set FX3 Regiter command
    ''' </summary>
    ''' <param name="Register">The Register to Set</param>
    ''' <param name="Value">The value at wich to set the register</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function BuildSetFX3Register(ByVal Register As Byte, ByVal Value As Byte) As Byte()
        ReDim _Primitive(4) '5 byte primitive
        _Primitive(0) = MessageId ' Message ID; manage by the object
        _Primitive(1) = Command.HID_DEBUG_WR ' command
        _Primitive(2) = 2 ' Always length of 2
        _Primitive(3) = Register 'Register
        _Primitive(4) = Value 'Register value
        Return _Primitive
    End Function
#End Region

#Region "I2C Registers"
    ''' <summary>
    ''' Build a Get I2C Register(s) command
    ''' </summary>
    ''' <param name="I2CAddress">The I2C Address</param>
    ''' <param name="Register">The Register to get</param>
    ''' <param name="Len"> number of register to get</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function BuildI2CRead(ByVal I2CAddress As Byte, ByVal Register As Byte, ByVal Len As Byte) As Byte()
        ReDim _Primitive(5) '6 bytes primitive
        _Primitive(0) = MessageId ' Message ID; manage by the object
        _Primitive(1) = Command.HID_I2C_RD_CMD ' command
        _Primitive(2) = 3 ' Always length of 3
        _Primitive(3) = I2CAddress 'I2CAddress
        _Primitive(4) = Register 'Register
        _Primitive(5) = Len 'number of register to read
        Return _Primitive
    End Function

    ''' <summary>
    ''' Build a Get FX3 get I2C Register command
    ''' </summary>
    ''' <param name="I2CAddress">The I2C Address</param>
    ''' <param name="Register">The Register to get</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function BuildGetI2CRegister(ByVal I2CAddress As Byte, ByVal Register As Byte) As Byte()
        Return BuildI2CRead(I2CAddress, Register, 1)
    End Function

    ''' <summary>
    ''' Build a Write I2C
    ''' </summary>
    ''' <param name="I2CAddress">>The I2C device address</param>
    ''' <param name="Register">The register to start to write in</param>
    ''' <param name="Data">The data to be written in the register</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function BuildI2CWrite(ByVal I2CAddress As Byte, ByVal Register As Byte, ByVal Data() As Byte) As Byte()
        ReDim _Primitive(64) '
        Dim DataLen As Integer = Data.Length

        ReDim _Primitive(DataLen + 5 - 1) '5 non data byte minus one to get upperbound
        _Primitive(0) = MessageId ' Message ID; manage by the object
        _Primitive(1) = Command.HID_I2C_WR_CMD ' command
        _Primitive(2) = DataLen + 2 ' 2 non data byte + data
        _Primitive(3) = I2CAddress
        _Primitive(4) = Register
        'Ok add data
        Array.Copy(Data, 0, _Primitive, 5, DataLen)
        Return _Primitive
    End Function

    ''' <summary>
    ''' Build a Set I2C Register command
    ''' </summary>
    ''' <param name="Register">The Register to Set</param>
    ''' <param name="Value">The value at wich to set the register</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function BuildSetI2CRegister(ByVal I2CAddress As Byte, ByVal Register As Byte, ByVal Value As Byte) As Byte()
        Dim Data() As Byte = {Value}
        Return BuildI2CWrite(I2CAddress, Register, Data)
    End Function
#End Region

#Region "EEPROM SPI Access"
    ''' <summary>
    ''' Build a Get FX3 Read SPI device
    ''' </summary>
    ''' <param name="Address">The SPI EEPROM address or device register</param>
    ''' <param name="Len">The number of data to get from SPI device</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function BuildReadSPIEEPROMFX3(ByVal Address As ULong, ByVal Len As Byte) As Byte()
        ReDim _Primitive(7) '7 bytes primitive
        _Primitive(0) = MessageId ' Message ID; manage by the object
        _Primitive(1) = Command.HID_SPI_RD_CMD ' command
        _Primitive(2) = 5 ' Always length of 5
        _Primitive(3) = 0 ' SPI_DEV is for future used left at 0
        _Primitive(4) = ((Address And &HFF0000) >> 16) 'Address [16:23]
        _Primitive(5) = ((Address And &HFF00) >> 8) 'Address  [8:15]
        _Primitive(6) = (Address And &HFF)  'Address  [0:7]
        _Primitive(7) = Len 'Len to read
        Return _Primitive
    End Function
    ''' <summary>
    ''' Build a Write SPI FX3 device
    ''' </summary>
    ''' <param name="Address">The address where to write</param>
    ''' <param name="Data">The byte to be written at selected address</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function BuildWriteSPIEEPROMFX3(ByVal Address As ULong, ByVal Data() As Byte) As Byte()
        ReDim _Primitive(64) '
        Dim DataLen As Integer = Data.Length

        ReDim _Primitive(DataLen + 7 - 1) '7 byte primitive minus one to get upperbound
        _Primitive(0) = MessageId ' Message ID; manage by the object
        _Primitive(1) = Command.HID_SPI_WR_CMD ' command
        _Primitive(2) = DataLen + 4 ' 
        _Primitive(3) = 0 ' SPI_DEV is for future used left at 0
        _Primitive(4) = ((Address And &HFF0000) >> 16) 'Address [16:23]
        _Primitive(5) = ((Address And &HFF00) >> 8) 'Address  [8:15]
        _Primitive(6) = (Address And &HFF)  'Address  [0:7]
        'Ok add data
        Array.Copy(Data, 0, _Primitive, 7, DataLen)
        Return _Primitive
    End Function

    ''' <summary>
    ''' Build a Write SPI FX3 device
    ''' </summary>
    ''' <param name="Sector">The Sector to be erased</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function BuildEraseEEPROMSectorSPIFX3(ByVal Sector As Byte) As Byte()
        ReDim _Primitive(4) '
        _Primitive(0) = MessageId ' Message ID; manage by the object
        _Primitive(1) = Command.HID_SPI_ERASE_CMD ' command
        _Primitive(2) = 2 ' 
        _Primitive(3) = 0 ' SPI_DEV is for future used left at 0
        _Primitive(4) = Sector ' The sector to be erased
        Return _Primitive
    End Function
#End Region

#Region "FPGA Access"
    ''' <summary>
    ''' Build a Read FPGA registers
    ''' </summary>
    ''' <param name="control">The FPGA control byte</param>
    '''  <param name="Register">The FPGA register also known as address byte</param>
    ''' <param name="Len">The number of data to get from FPGA</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function BuildReadFPGA(ByVal control As Byte, ByVal Register As Byte, ByVal Len As Byte) As Byte()
        ReDim _Primitive(5) '6 bytes primitive
        If Len > MAXIMUM_FPGA_READ_SIZE Then
            Len = MAXIMUM_FPGA_READ_SIZE
        End If
        'OK in a read the control byte bit 0 MUST BE 1
        control = control Or &H1
        _Primitive(0) = MessageId ' Message ID; manage by the object
        _Primitive(1) = Command.HID_FPGA_RD_CMD ' command
        _Primitive(2) = 5 ' Always length of 5
        _Primitive(3) = control
        _Primitive(4) = Register
        _Primitive(5) = Len 'Len to read
        Return _Primitive
    End Function

    ''' <summary>
    ''' Build a Write to FPGA Register
    ''' </summary>
    ''' <param name="control">The FPGA control byte</param>
    ''' <param name="Register">The FPGA register also known as address byte</param>
    ''' <param name="Data">The Data to be written</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function BuildWriteFPGA(ByVal control As Byte, ByVal Register As Byte, ByVal Data() As Byte) As Byte()
        ReDim _Primitive(64) '
        Dim DataLen As Integer = Data.Length

        If DataLen > MAXIMUM_FPGA_WRITE_SIZE Then
            DataLen = MAXIMUM_FPGA_WRITE_SIZE
        End If

        control = control And &HFE

        ReDim _Primitive(DataLen + 5 - 1) '5 byte primitive minus one to get upperbound
        _Primitive(0) = MessageId ' Message ID; manage by the object
        _Primitive(1) = Command.HID_FPGA_WR_CMD ' command
        _Primitive(2) = DataLen + 2 ' 
        _Primitive(3) = control
        _Primitive(4) = Register
        'Ok add data
        Array.Copy(Data, 0, _Primitive, 5, DataLen)
        Return _Primitive
    End Function
#End Region
End Class
