﻿#Region "Options"
Option Explicit On
Option Strict Off

Imports System.Collections.Generic

#End Region

#Region "Copyright"
'Dextera Labs Inc.
'(c) Copyright 2005-2013 Dextera Labs Inc.
'ALL RIGHTS RESERVED

'The software, source code and information contained herein are
'proprietary to, and comprise valuable intellectual property and
'trade secrets of Dextera Labs Inc.

'This software is furnished pursuant to a Source Code License
'Agreement between Dextera and the Licensee and may be used,
'copied, transmitted, and stored only by the Licensee in accordance
'with the terms of such license and with the inclusion of this
'copyright notice.

'This software, source code and information or any other copies
'thereof may not be provided or otherwise made available to any
'person not bound by the terms of the Source Code License Agreement.
#End Region

Public Class Device
    Private mName As String
    Private mManufacturer As String
    Private mDescription As String
    Private mService As String
    Private mDeviceID As String
    Private mPNPDeviceID As String
    Private mClassGUID As String

    Public Property Name() As String
        Get
            Return mName
        End Get
        Set(ByVal value As String)
            mName = value
        End Set
    End Property

    Public Property Manufacturer() As String
        Get
            Return mManufacturer
        End Get
        Set(ByVal value As String)
            mManufacturer = value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return mDescription
        End Get
        Set(ByVal value As String)
            mDescription = value
        End Set
    End Property

    Public Property Service() As String
        Get
            Return mService
        End Get
        Set(ByVal value As String)
            mService = value
        End Set
    End Property

    Public Property DeviceID() As String
        Get
            Return mDeviceID
        End Get
        Set(ByVal value As String)
            mDeviceID = value
        End Set
    End Property

    Public Property PNPDeviceID() As String
        Get
            Return mPNPDeviceID
        End Get
        Set(ByVal value As String)
            mPNPDeviceID = value
        End Set
    End Property

    Public Property ClassGUID() As String
        Get
            Return mClassGUID
        End Get
        Set(ByVal value As String)
            mClassGUID = value
        End Set
    End Property

    Public Shared Function GetAllDevices() As Generic.List(Of Device)
        Dim pc As String = "." 'local
        Dim wmi As Object = GetObject("winmgmts:\\" & pc & "\root\cimv2")
        Dim allDevices As New List(Of Device)
        Dim devices As Object = wmi.ExecQuery("Select * from Win32_PnPEntity")
        Dim device As Device
        For Each d As Object In devices
            device = New Device
            With device
                .mClassGUID = IIf(IsDBNull(d.ClassGuid), 0, d.ClassGuid)
                .mDescription = IIf(IsDBNull(d.Description), 0, d.Description)
                .DeviceID = IIf(IsDBNull(d.DeviceID), 0, d.DeviceID)
                .Manufacturer = IIf(IsDBNull(d.Manufacturer), 0, d.Manufacturer)
                .Name = IIf(IsDBNull(d.Name), 0, d.Name)
                .PNPDeviceID = IIf(IsDBNull(d.PNPDeviceID), 0, d.PNPDeviceID)
                .Service = IIf(IsDBNull(d.Service), 0, d.Service)
            End With
            allDevices.Add(device)
        Next
        Return allDevices
    End Function

    Public Shared Function GetNonWorkingDevices() As List(Of Device)
        Dim pc As String = "." 'local
        Dim wmi As Object = GetObject("winmgmts:\\" & pc & "\root\cimv2")
        Dim notWorking As New List(Of Device)
        Dim devices As Object = wmi.ExecQuery("Select * from Win32_PnPEntity WHERE ConfigManagerErrorCode <> 0")
        Dim device As Device
        For Each d As Object In devices
            device = New Device
            With device

            End With
            Try
                device.mClassGUID = d.ClassGuid
            Catch ex As Exception

            End Try

            device.mClassGUID = IIf(IsDBNull(d.ClassGuid), "N/A", d.ClassGuid)
            device.mDescription = IIf(IsDBNull(d.Description), "N/A", d.Description)
            device.DeviceID = IIf(IsDBNull(d.DeviceID), "N/A", d.DeviceID)
            device.Manufacturer = IIf(IsDBNull(d.Manufacturer), "N/A", d.Manufacturer)
            device.Name = IIf(IsDBNull(d.Name), "N/A", d.Name)
            device.PNPDeviceID = IIf(IsDBNull(d.PNPDeviceID), "N/A", d.PNPDeviceID)
            device.Service = IIf(IsDBNull(d.Service), "N/A", d.Service)
            notWorking.Add(device)
        Next
        Return notWorking
    End Function

End Class
