﻿#Region "Options"
Option Explicit On
Option Strict Off
#End Region

#Region "Copyright"
'Dextera Labs Inc.
'(c) Copyright 2005-2013 Dextera Labs Inc.
'ALL RIGHTS RESERVED

'The software, source code and information contained herein are
'proprietary to, and comprise valuable intellectual property and
'trade secrets of Dextera Labs Inc.

'This software is furnished pursuant to a Source Code License
'Agreement between Dextera and the Licensee and may be used,
'copied, transmitted, and stored only by the Licensee in accordance
'with the terms of such license and with the inclusion of this
'copyright notice.

'This software, source code and information or any other copies
'thereof may not be provided or otherwise made available to any
'person not bound by the terms of the Source Code License Agreement.
#End Region

#Region "Imports"
Imports GENIE1080PVIDEOCAPTURER.Device
Imports DShowNET
Imports System.Runtime.InteropServices
#End Region

''' <summary>
''' DirectShow filter property page definition and tool
''' </summary>
''' <remarks></remarks>
Public Class PropertyPage

#Region "Define com access"
    ''CAUUID
    '<StructLayout(LayoutKind.Sequential), ComVisible(False)>
    'Public Structure DsCAUUID
    '    Public cElems As Integer
    '    Public pElems As IntPtr
    'End Structure


    '<ComVisible(True), ComImport(), System.Security.SuppressUnmanagedCodeSecurity(), Guid("B196B28B-BAB4-101A-B69C-00AA00341D07"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)> _
    'Public Interface ISpecifyPropertyPages
    '    <PreserveSig()> Function GetPages(ByRef pPages As DsCAUUID)
    'End Interface
#End Region

    Protected specifyPropertyPages As ISpecifyPropertyPages
    Public Name As String

    ''' <summary>
    ''' CProperty page constructor
    ''' </summary>
    ''' <param name="name">Property Page name</param>
    ''' <param name="specifyPropertyPages">Pass the ISpecifyProperty page for the selected object</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal name As String, ByRef specifyPropertyPages As ISpecifyPropertyPages)
        Me.Name = name
        Me.specifyPropertyPages = specifyPropertyPages
    End Sub

    ''' <summary>
    ''' Show the property page in the device control
    ''' </summary>
    ''' <param name="owner"> The control to own the property page</param>
    ''' <remarks></remarks>
    Public Sub show(ByVal owner As Control)
        Dim cauuid As DsCAUUID = New DsCAUUID

        Try
            Dim hr As Integer = specifyPropertyPages.GetPages(cauuid)
            If hr <> 0 Then
                Marshal.ThrowExceptionForHR(hr)
            End If

            Dim o As Object = specifyPropertyPages
            OleCreatePropertyFrame(owner.Handle, 30, 30, "", 1, o, cauuid.cElems, cauuid.pElems, 0, 0, IntPtr.Zero)
        Catch ex As Exception

        Finally
            If cauuid.pElems <> IntPtr.Zero Then
                Marshal.FreeCoTaskMem(cauuid.pElems)
            End If
        End Try
    End Sub

    ''' <summary>
    ''' Dispose of the property page cleanly.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Dispose()
        If specifyPropertyPages IsNot Nothing Then
            Marshal.ReleaseComObject(specifyPropertyPages)
            specifyPropertyPages = Nothing
        End If
    End Sub

    Declare Function OleCreatePropertyFrame Lib "oleaut32" _
     (ByVal hwndOwner As IntPtr, _
     ByVal x As Int32, _
     ByVal y As Int32, _
     <MarshalAs(UnmanagedType.LPWStr)> _
     ByVal lpszCaption As String, _
     ByVal cObjects As Int32, _
     <MarshalAs(UnmanagedType.Interface, ArraySubType:=UnmanagedType.IUnknown)> _
     ByRef ppUnk As Object, _
     ByVal cPages As Int32, _
     ByVal pPageClsID As Int32, _
     ByVal lcid As Int32, _
     ByVal dwReserved As Int32, _
     ByVal pvReserved As IntPtr) As Integer


End Class
