#Region "options"
Option Strict Off
Option Explicit On
#End Region

#Region "imports"
Imports INOGENISampleApp.DeviceManagement
Imports INOGENISampleApp.FileIO
Imports INOGENISampleApp.Hid
Imports Microsoft.Win32.SafeHandles
Imports System.Globalization
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Timers
Imports VBLibraries.Utils.BaseUtility.Time
Imports VBLibraries.Communication
Imports VBLibraries.Utils.BaseUtility
Imports INOGENISampleApp.HIDCommand
Imports CyUSB
Imports INOGENISampleApp.GENIE1080P.DSVideoGraph
Imports INOGENISampleApp.DSAudioGraph
Imports INOGENISampleApp.INOGENIInterfaceModule.HRESULT
Imports System.Collections.Generic
Imports DirectShowLib
Imports INOGENISampleApp.GENIE1080P

Imports System.Runtime.InteropServices.ComTypes
Imports System.Threading

'Imports VBLibraries.Utils.BaseUtility.Convert
#End Region

Friend Class FrmMain
    Inherits System.Windows.Forms.Form

    Friend WithEvents prntDoc As New Printing.PrintDocument()
    Friend WithEvents PageSetupDialog1 As System.Windows.Forms.PageSetupDialog
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents QuitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SettingsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VideoFormatToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VideoFrameRateToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrinterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents lblStatus As System.Windows.Forms.ToolStripStatusLabel
    Dim PrintBitmap As Bitmap

#Region "Windows Form Designer generated code "
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()
        CommandBuilder = New HIDCommand
    End Sub
    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
        If Disposing Then
            CommandBuilder = Nothing
            If Not components Is Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(Disposing)
    End Sub
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Public ToolTip1 As System.Windows.Forms.ToolTip
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnStop As System.Windows.Forms.Button
    Friend WithEvents btnPlay As System.Windows.Forms.Button
    Friend WithEvents cbxDevice As System.Windows.Forms.ComboBox
    Friend WithEvents cbxCaps As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox17 As System.Windows.Forms.GroupBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Timer3 As System.Windows.Forms.Timer
    Friend WithEvents cbxUseanalyser As System.Windows.Forms.CheckBox
    Friend WithEvents cbxFrameRate As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox16 As System.Windows.Forms.GroupBox
    Friend WithEvents pbxCapture As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblDevSyncOffset As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblSyncOffset As System.Windows.Forms.Label
    Friend WithEvents lblAverageSyncOffset As System.Windows.Forms.Label
    Friend WithEvents lblJitter As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblFrameDropped As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblFrameDisplayed As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblAverageFrameRate As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btnPlayAudio As System.Windows.Forms.Button
    Friend WithEvents cbxDeviceAudio As System.Windows.Forms.ComboBox
    Friend WithEvents btnStopAudio As System.Windows.Forms.Button
    Friend WithEvents btnRefreshAudioDevice As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMain))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnStop = New System.Windows.Forms.Button()
        Me.btnPlay = New System.Windows.Forms.Button()
        Me.cbxDevice = New System.Windows.Forms.ComboBox()
        Me.cbxCaps = New System.Windows.Forms.ComboBox()
        Me.GroupBox17 = New System.Windows.Forms.GroupBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Timer3 = New System.Windows.Forms.Timer(Me.components)
        Me.cbxUseanalyser = New System.Windows.Forms.CheckBox()
        Me.cbxFrameRate = New System.Windows.Forms.ComboBox()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.GroupBox16 = New System.Windows.Forms.GroupBox()
        Me.pbxCapture = New System.Windows.Forms.PictureBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblDevSyncOffset = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblSyncOffset = New System.Windows.Forms.Label()
        Me.lblAverageSyncOffset = New System.Windows.Forms.Label()
        Me.lblJitter = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblFrameDropped = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblFrameDisplayed = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblAverageFrameRate = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btnPlayAudio = New System.Windows.Forms.Button()
        Me.cbxDeviceAudio = New System.Windows.Forms.ComboBox()
        Me.btnStopAudio = New System.Windows.Forms.Button()
        Me.btnRefreshAudioDevice = New System.Windows.Forms.Button()
        Me.PageSetupDialog1 = New System.Windows.Forms.PageSetupDialog()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VideoFormatToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VideoFrameRateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrinterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.lblStatus = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.GroupBox17.SuspendLayout()
        Me.GroupBox16.SuspendLayout()
        CType(Me.pbxCapture, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Timer1
        '
        Me.Timer1.Interval = 500
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1, Me.ToolStripMenuItem2})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(190, 48)
        Me.ContextMenuStrip1.Text = "Program"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(189, 22)
        Me.ToolStripMenuItem1.Text = "Program RAM"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(189, 22)
        Me.ToolStripMenuItem2.Text = "Program SPI EPPROM"
        '
        'btnStop
        '
        Me.btnStop.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnStop.BackgroundImage = Global.INOGENISampleApp.My.Resources.Resources.stop_video
        Me.btnStop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnStop.Location = New System.Drawing.Point(6, 98)
        Me.btnStop.Name = "btnStop"
        Me.btnStop.Size = New System.Drawing.Size(50, 50)
        Me.btnStop.TabIndex = 43
        Me.btnStop.UseVisualStyleBackColor = True
        Me.btnStop.Visible = False
        '
        'btnPlay
        '
        Me.btnPlay.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPlay.BackgroundImage = Global.INOGENISampleApp.My.Resources.Resources.play_video
        Me.btnPlay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnPlay.Location = New System.Drawing.Point(6, 42)
        Me.btnPlay.Name = "btnPlay"
        Me.btnPlay.Size = New System.Drawing.Size(50, 50)
        Me.btnPlay.TabIndex = 42
        Me.btnPlay.UseVisualStyleBackColor = True
        Me.btnPlay.Visible = False
        '
        'cbxDevice
        '
        Me.cbxDevice.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cbxDevice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxDevice.FormattingEnabled = True
        Me.cbxDevice.Location = New System.Drawing.Point(62, 42)
        Me.cbxDevice.Name = "cbxDevice"
        Me.cbxDevice.Size = New System.Drawing.Size(184, 22)
        Me.cbxDevice.TabIndex = 44
        Me.cbxDevice.Visible = False
        '
        'cbxCaps
        '
        Me.cbxCaps.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cbxCaps.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxCaps.FormattingEnabled = True
        Me.cbxCaps.Location = New System.Drawing.Point(62, 70)
        Me.cbxCaps.Name = "cbxCaps"
        Me.cbxCaps.Size = New System.Drawing.Size(184, 22)
        Me.cbxCaps.TabIndex = 45
        Me.cbxCaps.Visible = False
        '
        'GroupBox17
        '
        Me.GroupBox17.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox17.Controls.Add(Me.Label27)
        Me.GroupBox17.Controls.Add(Me.Label28)
        Me.GroupBox17.Controls.Add(Me.Label25)
        Me.GroupBox17.Controls.Add(Me.Label26)
        Me.GroupBox17.Controls.Add(Me.Label24)
        Me.GroupBox17.Controls.Add(Me.Label23)
        Me.GroupBox17.Location = New System.Drawing.Point(494, 312)
        Me.GroupBox17.Name = "GroupBox17"
        Me.GroupBox17.Size = New System.Drawing.Size(473, 67)
        Me.GroupBox17.TabIndex = 56
        Me.GroupBox17.TabStop = False
        Me.GroupBox17.Text = "Video Intermediary Filter Info"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(179, 50)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(90, 14)
        Me.Label27.TabIndex = 5
        Me.Label27.Text = "No PTS Detected!"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(9, 50)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(170, 14)
        Me.Label28.TabIndex = 4
        Me.Label28.Text = "Presentation Time Stamp Length : "
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(179, 33)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(90, 14)
        Me.Label25.TabIndex = 3
        Me.Label25.Text = "No PTS Detected!"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(9, 33)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(134, 14)
        Me.Label26.TabIndex = 2
        Me.Label26.Text = "Presentation Time Stamp : "
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(179, 17)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(104, 14)
        Me.Label24.TabIndex = 1
        Me.Label24.Text = "No Format Detected!"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(8, 17)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(44, 14)
        Me.Label23.TabIndex = 0
        Me.Label23.Text = "Video : "
        '
        'Timer3
        '
        Me.Timer3.Interval = 5000
        '
        'cbxUseanalyser
        '
        Me.cbxUseanalyser.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxUseanalyser.AutoSize = True
        Me.cbxUseanalyser.Location = New System.Drawing.Point(91, 40)
        Me.cbxUseanalyser.Name = "cbxUseanalyser"
        Me.cbxUseanalyser.Size = New System.Drawing.Size(179, 18)
        Me.cbxUseanalyser.TabIndex = 57
        Me.cbxUseanalyser.Text = "Use Intermediary Filter Analyzer"
        Me.cbxUseanalyser.UseVisualStyleBackColor = True
        '
        'cbxFrameRate
        '
        Me.cbxFrameRate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cbxFrameRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxFrameRate.FormattingEnabled = True
        Me.cbxFrameRate.Location = New System.Drawing.Point(62, 98)
        Me.cbxFrameRate.Name = "cbxFrameRate"
        Me.cbxFrameRate.Size = New System.Drawing.Size(184, 22)
        Me.cbxFrameRate.TabIndex = 58
        Me.cbxFrameRate.Visible = False
        '
        'btnRefresh
        '
        Me.btnRefresh.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRefresh.BackgroundImage = Global.INOGENISampleApp.My.Resources.Resources.PNG_Refresh_png_256x256
        Me.btnRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnRefresh.Location = New System.Drawing.Point(252, 42)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(50, 50)
        Me.btnRefresh.TabIndex = 59
        Me.btnRefresh.UseVisualStyleBackColor = True
        Me.btnRefresh.Visible = False
        '
        'GroupBox16
        '
        Me.GroupBox16.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox16.Controls.Add(Me.pbxCapture)
        Me.GroupBox16.Location = New System.Drawing.Point(2, 27)
        Me.GroupBox16.Name = "GroupBox16"
        Me.GroupBox16.Size = New System.Drawing.Size(968, 244)
        Me.GroupBox16.TabIndex = 60
        Me.GroupBox16.TabStop = False
        Me.GroupBox16.Text = "Video"
        '
        'pbxCapture
        '
        Me.pbxCapture.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pbxCapture.Location = New System.Drawing.Point(3, 16)
        Me.pbxCapture.Name = "pbxCapture"
        Me.pbxCapture.Size = New System.Drawing.Size(962, 225)
        Me.pbxCapture.TabIndex = 30
        Me.pbxCapture.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.btnPrint)
        Me.GroupBox1.Controls.Add(Me.cbxFrameRate)
        Me.GroupBox1.Controls.Add(Me.cbxCaps)
        Me.GroupBox1.Controls.Add(Me.btnPlay)
        Me.GroupBox1.Controls.Add(Me.cbxDevice)
        Me.GroupBox1.Controls.Add(Me.btnStop)
        Me.GroupBox1.Controls.Add(Me.btnRefresh)
        Me.GroupBox1.Location = New System.Drawing.Point(-1, 274)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(971, 159)
        Me.GroupBox1.TabIndex = 61
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Video Control"
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.BackgroundImage = Global.INOGENISampleApp.My.Resources.Resources.print_button
        Me.btnPrint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnPrint.Enabled = False
        Me.btnPrint.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnPrint.Location = New System.Drawing.Point(421, 15)
        Me.btnPrint.MaximumSize = New System.Drawing.Size(140, 140)
        Me.btnPrint.MinimumSize = New System.Drawing.Size(140, 140)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(140, 140)
        Me.btnPrint.TabIndex = 60
        Me.btnPrint.Text = "PRINT"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.lblDevSyncOffset)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.lblSyncOffset)
        Me.GroupBox2.Controls.Add(Me.lblAverageSyncOffset)
        Me.GroupBox2.Controls.Add(Me.cbxUseanalyser)
        Me.GroupBox2.Controls.Add(Me.lblJitter)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.lblFrameDropped)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.lblFrameDisplayed)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.lblAverageFrameRate)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Location = New System.Drawing.Point(5, 312)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(473, 67)
        Me.GroupBox2.TabIndex = 62
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Video Renderer Info"
        '
        'lblDevSyncOffset
        '
        Me.lblDevSyncOffset.AutoSize = True
        Me.lblDevSyncOffset.Location = New System.Drawing.Point(444, 50)
        Me.lblDevSyncOffset.Name = "lblDevSyncOffset"
        Me.lblDevSyncOffset.Size = New System.Drawing.Size(13, 14)
        Me.lblDevSyncOffset.TabIndex = 11
        Me.lblDevSyncOffset.Text = "0"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(223, 50)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(206, 14)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Standard Deviation Sync Offset (msec) : "
        '
        'lblSyncOffset
        '
        Me.lblSyncOffset.AutoSize = True
        Me.lblSyncOffset.Location = New System.Drawing.Point(444, 33)
        Me.lblSyncOffset.Name = "lblSyncOffset"
        Me.lblSyncOffset.Size = New System.Drawing.Size(13, 14)
        Me.lblSyncOffset.TabIndex = 9
        Me.lblSyncOffset.Text = "0"
        '
        'lblAverageSyncOffset
        '
        Me.lblAverageSyncOffset.AutoSize = True
        Me.lblAverageSyncOffset.Location = New System.Drawing.Point(223, 33)
        Me.lblAverageSyncOffset.Name = "lblAverageSyncOffset"
        Me.lblAverageSyncOffset.Size = New System.Drawing.Size(157, 14)
        Me.lblAverageSyncOffset.TabIndex = 8
        Me.lblAverageSyncOffset.Text = "Average Sync Offset (msec) : "
        '
        'lblJitter
        '
        Me.lblJitter.AutoSize = True
        Me.lblJitter.Location = New System.Drawing.Point(444, 17)
        Me.lblJitter.Name = "lblJitter"
        Me.lblJitter.Size = New System.Drawing.Size(13, 14)
        Me.lblJitter.TabIndex = 7
        Me.lblJitter.Text = "0"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(222, 17)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(76, 14)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "Jitter (msec) : "
        '
        'lblFrameDropped
        '
        Me.lblFrameDropped.AutoSize = True
        Me.lblFrameDropped.Location = New System.Drawing.Point(179, 50)
        Me.lblFrameDropped.Name = "lblFrameDropped"
        Me.lblFrameDropped.Size = New System.Drawing.Size(13, 14)
        Me.lblFrameDropped.TabIndex = 5
        Me.lblFrameDropped.Text = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(96, 14)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Frames Dropped : "
        '
        'lblFrameDisplayed
        '
        Me.lblFrameDisplayed.AutoSize = True
        Me.lblFrameDisplayed.Location = New System.Drawing.Point(179, 33)
        Me.lblFrameDisplayed.Name = "lblFrameDisplayed"
        Me.lblFrameDisplayed.Size = New System.Drawing.Size(13, 14)
        Me.lblFrameDisplayed.TabIndex = 3
        Me.lblFrameDisplayed.Text = "0"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 33)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(102, 14)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Frames Displayed : "
        '
        'lblAverageFrameRate
        '
        Me.lblAverageFrameRate.AutoSize = True
        Me.lblAverageFrameRate.Location = New System.Drawing.Point(179, 17)
        Me.lblAverageFrameRate.Name = "lblAverageFrameRate"
        Me.lblAverageFrameRate.Size = New System.Drawing.Size(28, 14)
        Me.lblAverageFrameRate.TabIndex = 1
        Me.lblAverageFrameRate.Text = "0.00"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 17)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(116, 14)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Average Frame Rate : "
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.Controls.Add(Me.btnPlayAudio)
        Me.GroupBox3.Controls.Add(Me.cbxDeviceAudio)
        Me.GroupBox3.Controls.Add(Me.btnStopAudio)
        Me.GroupBox3.Controls.Add(Me.btnRefreshAudioDevice)
        Me.GroupBox3.Location = New System.Drawing.Point(5, 387)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(962, 61)
        Me.GroupBox3.TabIndex = 63
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Audio Control"
        '
        'btnPlayAudio
        '
        Me.btnPlayAudio.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPlayAudio.Location = New System.Drawing.Point(6, 20)
        Me.btnPlayAudio.Name = "btnPlayAudio"
        Me.btnPlayAudio.Size = New System.Drawing.Size(40, 23)
        Me.btnPlayAudio.TabIndex = 42
        Me.btnPlayAudio.Text = "Play"
        Me.btnPlayAudio.UseVisualStyleBackColor = True
        '
        'cbxDeviceAudio
        '
        Me.cbxDeviceAudio.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cbxDeviceAudio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxDeviceAudio.FormattingEnabled = True
        Me.cbxDeviceAudio.Location = New System.Drawing.Point(62, 20)
        Me.cbxDeviceAudio.Name = "cbxDeviceAudio"
        Me.cbxDeviceAudio.Size = New System.Drawing.Size(395, 22)
        Me.cbxDeviceAudio.TabIndex = 44
        '
        'btnStopAudio
        '
        Me.btnStopAudio.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnStopAudio.Location = New System.Drawing.Point(497, 19)
        Me.btnStopAudio.Name = "btnStopAudio"
        Me.btnStopAudio.Size = New System.Drawing.Size(41, 23)
        Me.btnStopAudio.TabIndex = 43
        Me.btnStopAudio.Text = "stop"
        Me.btnStopAudio.UseVisualStyleBackColor = True
        '
        'btnRefreshAudioDevice
        '
        Me.btnRefreshAudioDevice.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRefreshAudioDevice.BackgroundImage = Global.INOGENISampleApp.My.Resources.Resources.PNG_Refresh_png_256x256
        Me.btnRefreshAudioDevice.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnRefreshAudioDevice.Location = New System.Drawing.Point(463, 20)
        Me.btnRefreshAudioDevice.Name = "btnRefreshAudioDevice"
        Me.btnRefreshAudioDevice.Size = New System.Drawing.Size(28, 23)
        Me.btnRefreshAudioDevice.TabIndex = 59
        Me.btnRefreshAudioDevice.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.SettingsToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(972, 24)
        Me.MenuStrip1.TabIndex = 64
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.QuitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'QuitToolStripMenuItem
        '
        Me.QuitToolStripMenuItem.Name = "QuitToolStripMenuItem"
        Me.QuitToolStripMenuItem.Size = New System.Drawing.Size(97, 22)
        Me.QuitToolStripMenuItem.Text = "Quit"
        '
        'SettingsToolStripMenuItem
        '
        Me.SettingsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VideoFormatToolStripMenuItem, Me.VideoFrameRateToolStripMenuItem, Me.PrinterToolStripMenuItem})
        Me.SettingsToolStripMenuItem.Name = "SettingsToolStripMenuItem"
        Me.SettingsToolStripMenuItem.Size = New System.Drawing.Size(61, 20)
        Me.SettingsToolStripMenuItem.Text = "Settings"
        '
        'VideoFormatToolStripMenuItem
        '
        Me.VideoFormatToolStripMenuItem.Name = "VideoFormatToolStripMenuItem"
        Me.VideoFormatToolStripMenuItem.Size = New System.Drawing.Size(173, 22)
        Me.VideoFormatToolStripMenuItem.Text = "Capture format"
        '
        'VideoFrameRateToolStripMenuItem
        '
        Me.VideoFrameRateToolStripMenuItem.Name = "VideoFrameRateToolStripMenuItem"
        Me.VideoFrameRateToolStripMenuItem.Size = New System.Drawing.Size(173, 22)
        Me.VideoFrameRateToolStripMenuItem.Text = "Capture frame rate"
        '
        'PrinterToolStripMenuItem
        '
        Me.PrinterToolStripMenuItem.Name = "PrinterToolStripMenuItem"
        Me.PrinterToolStripMenuItem.Size = New System.Drawing.Size(173, 22)
        Me.PrinterToolStripMenuItem.Text = "Printer"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AboutToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.AboutToolStripMenuItem.Text = "About"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblStatus})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 438)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(972, 22)
        Me.StatusStrip1.TabIndex = 65
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'lblStatus
        '
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(0, 17)
        '
        'FrmMain
        '
        Me.ClientSize = New System.Drawing.Size(972, 460)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.GroupBox16)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox17)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox3)
        Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Generic HID Tester"
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.GroupBox17.ResumeLayout(False)
        Me.GroupBox17.PerformLayout()
        Me.GroupBox16.ResumeLayout(False)
        CType(Me.pbxCapture, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
#End Region

    Private WithEvents graph1 As GENIE1080P.DSVideoGraph
    Private WithEvents graphAudio As DSAudioGraph
    Private SelectedFilterName As String = ""
    Private SelectedAudioFilterName As String = ""

    Private pictureFilePath As String = ""
    Private PlayFlag As Boolean = False
    Private bmp As Bitmap = Nothing

#Region "Constant"
    Private Const MAXIMUM_NUMBER_OF_LINE As ULong = 50000
    Private Const DISPLAY_DEBUG_ON_TEXT_BOX As Boolean = False

    Public ReadOnly ALLOWED_DEVICE As Guid = CLSID_UVC_VIDEO_DEVICE


    ' Public Const INOGENI_PNPID As String = "USB\VEN_04B4&DEV_00FA" 
    Public Const INOGENI_OLD_PNPID As String = "USB\VID_04B4&PID_00FA"
    Public Const INOGENI_DEXTERA_PNPID As String = "USB\VID_2997&PID_0001"


#End Region

#Region "Enums"
    Public Enum RequestType
        Read
        Write
        Unspecified
    End Enum
#End Region

#Region "Delegates"
    Private Delegate Sub AddStringToLog1Delegate(ByVal LineToLog As String, ByVal myTextColor As System.Drawing.Color)
    Private Delegate Sub enableButtonDelegate(ByVal Button As Button, ByVal Enable As Boolean)
    Private Delegate Sub enableComboBOxDelegate(ByVal cbx As ComboBox, ByVal Enable As Boolean)
    ' This delegate has the same parameters as AccessForm.
    ' Used in accessing the application's form from a different thread.

    Private Delegate Sub MarshalToForm _
     (ByVal action As String, _
     ByVal textToAdd As String)
#End Region

#Region "Control handler thread safe"


    Private Sub enableButton(ByVal Button As Button, ByVal Enable As Boolean)
        Try
            If Button IsNot Nothing AndAlso Button.InvokeRequired <> True Then
                Button.Enabled = Enable
            Else
                Button.BeginInvoke(New enableButtonDelegate(AddressOf enableButton), Button, Enable)
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
            Return
        End Try
    End Sub

    Private Sub enableComboBox(ByVal cbx As ComboBox, ByVal Enable As Boolean)
        Try
            If cbx IsNot Nothing AndAlso cbx.InvokeRequired <> True Then
                cbx.Enabled = Enable
            Else
                cbx.BeginInvoke(New enableComboBOxDelegate(AddressOf enableComboBox), cbx, Enable)
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
            Return
        End Try
    End Sub

#End Region

#Region "Private members"
    'USB cypress bootloader

    Private deviceNotificationHandle As IntPtr
    Private exclusiveAccess As Boolean
    Private fileStreamdevicedata As FileStream
    Private hidHandle As SafeFileHandle
    Private hidUsage As String
    Private myDeviceDetected As Boolean
    Private myDevicePathName As String
    Private transferInProgress As Boolean = False
    Private writeHandle As SafeFileHandle

    Private MyDebugging As New Debugging() ' For viewing results of API calls via Debug.Write.
    Private MyDeviceManagement As New DeviceManagement()
    Private MyHid As New Hid()

    Private Shared tmrReadTimeout As System.Timers.Timer
    Private Shared tmrContinuousDataCollect As System.Timers.Timer

    Friend FrmMy As FrmMain

    Private CommandBuilder As New HIDCommand

    Private EnableLog As Boolean = True
    Dim US3VendorIdCypress As String = "04B4"
    Dim US3ProductIdCypress As String = "00FA"
    Dim US3VendorIdInogeni As String = "2997"
    Dim US3ProductIDextera As String = "0001"
    Dim US3ProductIVidyo As String = "0002"
    Dim US3ProductIDextera4K As String = "0004"
    Dim VendorIDCypress As Int32 = Int32.Parse(US3VendorIdCypress, NumberStyles.AllowHexSpecifier)
    Dim ProductIDCypress As Int32 = Int32.Parse(US3ProductIdCypress, NumberStyles.AllowHexSpecifier)
    Dim VendorIDNIGENI As Int32 = Int32.Parse(US3VendorIdInogeni, NumberStyles.AllowHexSpecifier)
    Dim ProductIDDextera As Int32 = Int32.Parse(US3ProductIDextera, NumberStyles.AllowHexSpecifier)
    Dim ProductIDVidyo As Int32 = Int32.Parse(US3ProductIVidyo, NumberStyles.AllowHexSpecifier)
    Dim ProductIDDextera4K As Int32 = Int32.Parse(US3ProductIDextera4K, NumberStyles.AllowHexSpecifier)
    Dim EnableDebugConsoleWrite As Boolean = True
#End Region








    Private Sub EnableControl(ByVal Enable As Boolean)


    End Sub
    Private Sub ListAudioCaptureDeviceInCombobox()
        Dim ArrayOfCaptureFilter As String(,) = DSAudioGraph.ListAudioCaptureDevice

        cbxDeviceAudio.Items.Clear()

        For i As Integer = 0 To ArrayOfCaptureFilter.GetUpperBound(1)
            cbxDeviceAudio.Items.Add(ArrayOfCaptureFilter(0, i))
        Next
        If cbxDeviceAudio.Items.Count > 0 Then
            cbxDeviceAudio.SelectedIndex = 0
        End If
    End Sub
    ''' <summary>
    ''' Called when a WM_DEVICECHANGE message has arrived,
    ''' indicating that a device has been attached or removed.
    ''' </summary>
    ''' 
    ''' <param name="m"> a message with information about the device </param>

    Friend Sub OnDeviceChange(ByVal m As Message)

        '  DebugWriteLine("WM_DEVICECHANGE")

        Try
            If (m.WParam.ToInt32 = DBT_DEVICEARRIVAL) Then

                ' If WParam contains DBT_DEVICEARRIVAL, a device has been attached.

                DebugWriteLine("A device has been attached.")

                ' Find out if it's the device we're communicating with.

                If MyDeviceManagement.DeviceNameMatch(m, myDevicePathName) Then
                    DebugWriteLine("My device attached.")
                Else
                    DebugWriteLine("A device has been attached.")
                End If

                IsGenie1080PPresent()
                ListVideoCaptureDevice()
                updatemenu()


            ElseIf (m.WParam.ToInt32 = DBT_DEVICEREMOVECOMPLETE) Then

                ' If WParam contains DBT_DEVICEREMOVAL, a device has been removed.

                DebugWriteLine("A device has been removed.")

                ' Find out if it's the device we're communicating with.

                If MyDeviceManagement.DeviceNameMatch(m, myDevicePathName) Then



                    DebugWriteLine("My device removed.")




                    FrmMy.myDeviceDetected = False
                    'EnableControl(False)
                    'Shutdown()

                End If
                If IsGenie1080PPresent() = False Then

                End If

                ListVideoCaptureDevice()
                updatemenu()
            ElseIf (m.WParam.ToInt32 = BT_DEVNODES_CHANGED) Then
                Console.WriteLine("A device has node changed.")
                'Ok there was a node change chek for the GENIE1080P
            End If



        Catch ex As Exception
            DisplayException(Me.Name, ex)
            Throw
        End Try
    End Sub




    ''' <summary>
    ''' Uses a series of API calls to locate a HID-class device
    ''' by its Vendor ID and Product ID.
    ''' </summary>
    '''         
    ''' <returns>
    '''  True if the device is detected, False if not detected.
    ''' </returns>

    Private Function isDevicePresent() As Boolean

        Dim deviceFound As Boolean
        Dim devicePathName(127) As String
        Dim hidGuid As System.Guid
        Dim memberIndex As Int32
        Dim success As Boolean
        Dim hidHandle As SafeFileHandle = Nothing
        Try

            ' ***
            ' API function: 'HidD_GetHidGuid

            ' Purpose: Retrieves the interface class GUID for the HID class.

            ' Accepts: 'A System.Guid object for storing the GUID.
            ' ***

            HidD_GetHidGuid(hidGuid)

            DebugWriteLine(MyDebugging.ResultOfAPICall("GetHidGuid"))
            DebugWriteLine("  GUID for system HIDs: " & hidGuid.ToString)

            ' Fill an array with the device path names of all attached HIDs.

            deviceFound = MyDeviceManagement.FindDeviceFromGuid _
             (hidGuid, _
             devicePathName)

            ' If there is at least one HID, attempt to read the Vendor ID and Product ID
            ' of each device until there is a match or all devices have been examined.

            If deviceFound Then

                memberIndex = 0

                Do
                    ' ***
                    ' API function:
                    ' CreateFile

                    ' Purpose:
                    ' Retrieves a handle to a device.

                    ' Accepts:
                    ' A device path name returned by SetupDiGetDeviceInterfaceDetail
                    ' The type of access requested (read/write).
                    ' FILE_SHARE attributes to allow other processes to access the device while this handle is open.
                    ' A Security structure or IntPtr.Zero. 
                    ' A creation disposition value. Use OPEN_EXISTING for devices.
                    ' Flags and attributes for files. Not used for devices.
                    ' Handle to a template file. Not used.

                    ' Returns: a handle without read or write access.
                    ' This enables obtaining information about all HIDs, even system
                    ' keyboards and mice. 
                    ' Separate handles are used for reading and writing.
                    ' ***

                    ' Open the handle without read/write access to enable getting information about any HID, even system keyboards and mice.

                    hidHandle = CreateFile _
                     (devicePathName(memberIndex), _
                     0, _
                     FILE_SHARE_READ Or FILE_SHARE_WRITE, _
                     IntPtr.Zero, _
                     OPEN_EXISTING, _
                     0, _
                     0)



                    If Not (hidHandle.IsInvalid) Then

                        ' The returned handle is valid, 
                        ' so find out if this is the device we're looking for.

                        ' Set the Size property of DeviceAttributes to the number of bytes in the structure.

                        MyHid.DeviceAttributes.Size = Marshal.SizeOf(MyHid.DeviceAttributes)

                        ' ***
                        ' API function:
                        ' HidD_GetAttributes

                        ' Purpose:
                        ' Retrieves a HIDD_ATTRIBUTES structure containing the Vendor ID, 
                        ' Product ID, and Product Version Number for a device.

                        ' Accepts:
                        ' A handle returned by CreateFile.
                        ' A pointer to receive a HIDD_ATTRIBUTES structure.

                        ' Returns:
                        ' True on success, False on failure.
                        ' ***

                        success = HidD_GetAttributes(hidHandle, MyHid.DeviceAttributes)

                        If success Then

                            ' Find out if the device matches the one we're looking for.

                            If ((MyHid.DeviceAttributes.VendorID = VendorIDCypress) And _
                            (MyHid.DeviceAttributes.ProductID = ProductIDCypress)) OrElse _
                           ((MyHid.DeviceAttributes.VendorID = VendorIDNIGENI) And _
                            (MyHid.DeviceAttributes.ProductID = ProductIDDextera)) OrElse _
                           ((MyHid.DeviceAttributes.VendorID = VendorIDNIGENI) And _
                            (MyHid.DeviceAttributes.ProductID = ProductIDVidyo)) OrElse _
                           ((MyHid.DeviceAttributes.VendorID = VendorIDNIGENI) And _
                            (MyHid.DeviceAttributes.ProductID = ProductIDDextera4K)) Then

                                'Yes our device is detected

                                Return True
                            Else

                                ' It's not a match, so close the handle.

                                hidHandle.Close()

                            End If

                        Else
                            ' There was a problem in retrieving the information.
                            hidHandle.Close()
                        End If

                    End If

                    ' Keep looking until we find the device or there are no devices left to examine.

                    memberIndex = memberIndex + 1

                Loop Until ((memberIndex = devicePathName.Length))
            End If
            Return False
        Catch ex As Exception
            DisplayException(Me.Name, ex)
            Throw
        Finally
            If hidHandle IsNot Nothing Then
                hidHandle.Close()
            End If
        End Try
    End Function




    ''' <summary>
    ''' Called if the user changes the Vendor ID or Product ID in the text box.
    ''' </summary>
    ''' 
    Private Sub DeviceHasChanged()

        Try
            ' If a device was previously detected, stop receiving notifications about it.

            If myDeviceDetected Then
                MyDeviceManagement.StopReceivingDeviceNotifications(deviceNotificationHandle)
            End If

            ' Search for the device the next time FindTheHid is called.

            myDeviceDetected = False

        Catch ex As Exception
            DisplayException(Me.Name, ex)
            Throw
        End Try
    End Sub




    ''' <summary>
    ''' Perform shutdown operations.
    ''' </summary>
    Private Sub frmMain_Closed(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Closed

        Try
            Shutdown()

        Catch ex As Exception
            DisplayException(Me.Name, ex)
            Throw
        End Try
    End Sub



    Private Sub FrmMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Timer1.Enabled = False

        Timer3.Enabled = False


        If graph1 IsNot Nothing Then

            graph1.Play(False)
            graph1.closeinterfaces()
        End If

        If graphAudio IsNot Nothing Then
            graphAudio.Play(False)
            graphAudio.closeinterfaces()
        End If

        KillTheGraph()

        My.Settings.WindowSizeX = Me.Width
        My.Settings.WindowSizeY = Me.Height
        My.Settings.Save()

    End Sub

    Private Function isINOGENIDevicePresent() As Boolean
        Dim INOGENIDevicesList() As String = {""}
        'Dim inogeniInfo As INOGENILib.INOGENIInfo

        INOGENIDevicesList = INOGENILib.INOGENIDVI_2_USB3.ListINOGENIDevice
        If INOGENIDevicesList IsNot Nothing Then
            'inogeniInfo = New INOGENILib.INOGENIInfo(INOGENIDevicesList(0))
            'inogeniInfo.RefreshVersionInfo()
            Return True
        Else
            Return False
        End If
    End Function

    ''' <summary>
    ''' Perform startup operations.
    ''' </summary>

    Private Sub frmMain_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Try
            FrmMy = Me
            Me.Text = "INOGENI Snapshot" & String.Format(" (Version {0}.{1}.{2})", My.Application.Info.Version.Major, My.Application.Info.Version.Minor, My.Application.Info.Version.Build)
            Startup()
            ListVideoCaptureDeviceInCombobox()
            updatemenu()
            'If FindTheHid() = False Then
            Timer1.Enabled = True
            Timer3.Enabled = True
            'End If

            ListVideoCaptureDeviceInCombobox()
            ListAudioCaptureDeviceInCombobox()

            If isINOGENIDevicePresent() Then
                Play()
            Else
                While isINOGENIDevicePresent() = False
                    MsgBox("No INOGENI device connected. Connect an INOGENI device to a USB3 port and click OK.")
                    Wait(3000)
                End While
                'wait for the device to boot
                Wait(12000)
                ListVideoCaptureDeviceInCombobox()
                ListAudioCaptureDeviceInCombobox()
                Play()
            End If

            UpdateSettings()

            If My.Settings.WindowSizeX = 0 And My.Settings.WindowSizeY = 0 Then
                Me.Width = 988
                Me.Height = 498
            Else
                Me.Width = My.Settings.WindowSizeX
                Me.Height = My.Settings.WindowSizeY
            End If

        Catch ex As Exception
            DisplayException(Me.Name, ex)
            Throw
        End Try

    End Sub



    ''' <summary>
    ''' Finds and displays the number of Input buffers
    ''' (the number of Input reports the host will store). 
    ''' </summary>

    Private Sub GetInputReportBufferSize()

        Dim numberOfInputBuffers As Int32

        Try
            ' Get the number of input buffers.

            MyHid.GetNumberOfInputBuffers _
             (hidHandle, _
             numberOfInputBuffers)

            ' Display the result in the text box.



        Catch ex As Exception
            DisplayException(Me.Name, ex)
            Throw
        End Try

    End Sub

    ''' <summary>
    ''' Retrieves Input report data and status information.
    ''' This routine is called automatically when myInputReport.Read
    ''' returns. Calls several marshaling routines to access the main form.
    ''' </summary>
    ''' 
    ''' <param name="ar"> an object containing status information about 
    ''' the asynchronous operation. </param>

    Private Sub GetInputReportData(ByVal ar As IAsyncResult)

        Dim byteValue As String
        Dim count As Int32
        Dim inputReportBuffer As Byte() = Nothing

        Try
            inputReportBuffer = CType(ar.AsyncState, Byte())

            fileStreamdevicedata.EndRead(ar)

            tmrReadTimeout.Stop()

            ' Display the received report data in the form's list box.

            If (ar.IsCompleted) Then
                DebugWriteLine("An Input report has been read.")
                DebugWriteLine(" Input Report ID: " & String.Format("{0:X2} ", inputReportBuffer(0)))

                'DebugWriteLine(" Input Report Data:", Color.Black)


                Dim RX As String = "[RX]:"
                For count = 1 To UBound(inputReportBuffer)

                    ' Display bytes as 2-character Hex strings.
                    byteValue = String.Format("{0:X2} ", inputReportBuffer(count))
                    RX &= " " & byteValue

                Next count

            Else
                DebugWriteLine("The attempt to read an Input report has failed.")
                Debug.Write("The attempt to read an Input report has failed")
            End If


            ' Enable requesting another transfer.


            transferInProgress = False

        Catch ex As Exception
            DisplayException(Me.Name, ex)
            Throw
        End Try

    End Sub


    ''' <summary>
    ''' Initialize the elements on the form.
    ''' </summary>
    ''' 
    Private Sub InitializeDisplay()
        Try
            ' Create a dropdown list box for each byte to send in a report.
            ' Display the values as 2-character hex strings.


            ' Don't allow the user to select an input report buffer size until there is
            ' a handle to a HID.

            'cmdInputReportBufferSize.Focus()
            'cmdInputReportBufferSize.Enabled = False

            If MyHid.IsWindowsXpOrLater() = False Then

                ' If the operating system is earlier than Windows XP,
                ' disable the option to force Input and Output reports to use control transfers.

                'AddStringToLog1("OOops We cannot force input and output reports", Color.Black)

            End If

        Catch ex As Exception
            DisplayException(Me.Name, ex)
            Throw
        End Try

    End Sub





    ''' <summary>
    ''' Perform actions that must execute when the program ends.
    ''' </summary>
    Private Sub Shutdown()

        Try
            ' Stop receiving notifications.

            MyDeviceManagement.StopReceivingDeviceNotifications(deviceNotificationHandle)
            EnableControl(False)
        Catch ex As Exception
            DisplayException(Me.Name, ex)
            Throw
        End Try

    End Sub

    ''' <summary>
    ''' Perform actions that must execute when the program starts.
    ''' </summary>
    Private Sub Startup()

        Try
            MyHid = New Hid()
            InitializeDisplay()





            CommandBuilder = New HIDCommand

            ' Default USB Vendor ID and Product ID:



        Catch ex As Exception
            DisplayException(Me.Name, ex)
            Throw
        End Try

    End Sub

    ''' <summary>
    ''' The Product ID has changed in the text box. Call a routine to handle it.
    ''' </summary>
    Private Sub txtProductID_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Try
            DeviceHasChanged()

        Catch ex As Exception
            DisplayException(Me.Name, ex)
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' The Vendor ID has changed in the text box. Call a routine to handle it.
    ''' </summary>
    Private Sub txtVendorID_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Try
            DeviceHasChanged()

        Catch ex As Exception
            DisplayException(Me.Name, ex)
            Throw
        End Try

    End Sub

    ''' <summary>
    ''' Finalize method.
    ''' </summary>
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub


    ''' <summary>
    '''  Overrides WndProc to enable checking for and handling WM_DEVICECHANGE messages.
    ''' </summary>
    ''' 
    ''' <param name="m"> a Windows Message </param>

    Protected Overrides Sub WndProc(ByRef m As Message)

        Try
            ' The OnDeviceChange routine processes WM_DEVICECHANGE messages.

            If m.Msg = WM_DEVICECHANGE Then
                OnDeviceChange(m)
            End If

            If graph1 IsNot Nothing Then
                graph1.Notify(m)
            End If

            ' Let the base form process the message.

            MyBase.WndProc(m)

        Catch ex As Exception
            DisplayException(Me.Name, ex)
            Throw
        End Try

    End Sub

    ''' <summary>
    ''' Provides a central mechanism for exception handling.
    ''' Displays a message box that describes the exception.
    ''' </summary>
    ''' 
    ''' <param name="moduleName"> the module where the exception occurred. </param>
    ''' <param name="e"> the exception </param>

    Shared Sub DisplayException(ByVal moduleName As String, ByVal e As Exception)

        Dim message As String
        Dim caption As String

        ' Create an error message.

        message = "Exception: " & e.Message & ControlChars.CrLf & _
        "Module: " & moduleName & ControlChars.CrLf & _
        "Method: " & e.TargetSite.Name

        caption = "Unexpected Exception"

        MessageBox.Show(message, caption, MessageBoxButtons.OK)
        Debug.Write(message)

    End Sub



#Region "USB3 HID high Level Request"
    Private Function ValidateHexStringisByte(ByVal HexString As String, ByRef Cleanup As String) As Boolean
        Try
            Dim temp As Double
            Cleanup = RemoveSpace(HexString.ToUpper)
            If IsHexadecimalString(Cleanup) = False Then
                Return False
            End If

            temp = CDbl("&H" & Cleanup)
            If temp < 0 OrElse temp > 255 Then
                Return False
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function ValidateHexStringisUnsigned(ByVal HexString As String, ByRef Cleanup As String, ByVal Max As ULong) As Boolean
        Try
            Dim temp As Double
            Cleanup = RemoveSpace(HexString.ToUpper)
            If IsHexadecimalString(Cleanup) = False Then
                Return False
            End If

            temp = CDbl("&H" & Cleanup)
            If temp < 0 OrElse temp > Max Then
                Return False
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

#End Region


    Public Sub EnableVideoButton(ByVal Enable As Boolean)
        enableButton(btnPlay, Enable)
        enableButton(btnRefresh, Enable)
        enableButton(btnStop, False)
        enableComboBox(cbxDevice, Enable)
        enableComboBox(cbxCaps, Enable)
        enableComboBox(cbxFrameRate, Enable)
    End Sub

    Private DeviceIsDetected As Boolean = False
    Private Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        'Dim RestoreDebugConsoleWrite As Boolean = EnableDebugConsoleWrite
        'Dim RestoreHIDDebug As Boolean = False

        'Try

        '    If MyHid IsNot Nothing Then
        '        RestoreHIDDebug = MyHid.EnableLog
        '        MyHid.EnableLog = False
        '    End If
        '    EnableDebugConsoleWrite = False
        '    If isDevicePresent() Then

        '        If DeviceIsDetected = False Then
        '            DeviceIsDetected = True
        '            ListVideoCaptureDeviceInCombobox()
        '            updatemenu()
        '        End If

        '    Else
        '        If DeviceIsDetected Then
        '            DeviceIsDetected = False
        '            ListVideoCaptureDeviceInCombobox()
        '        End If


        '        EnableControl(False)
        '    End If
        'Catch ex As Exception
        'Finally
        '    EnableDebugConsoleWrite = RestoreDebugConsoleWrite
        '    If MyHid IsNot Nothing Then
        '        MyHid.EnableLog = RestoreHIDDebug
        '    End If
        'End Try
    End Sub






    Public Function GetHMDIRxMapAddress(ByVal map As HDMIRxMap) As Byte
        Select Case map
            Case HDMIRxMap.IO
                Return AVD7612_I2C_ADDRESS_IO
            Case HDMIRxMap.CEC
                Return AVD7612_I2C_ADDRESS_CEC
            Case HDMIRxMap.INFO
                Return AVD7612_I2C_ADDRESS_INFO
            Case HDMIRxMap.DPLL
                Return AVD7612_I2C_ADDRESS_DPLL
            Case HDMIRxMap.KSV
                Return AVD7612_I2C_ADDRESS_KSV
            Case HDMIRxMap.EDID
                Return AVD7612_I2C_ADDRESS_EDID
            Case HDMIRxMap.HDMI
                Return AVD7612_I2C_ADDRESS_HDMI
            Case HDMIRxMap.CP
                Return AVD7612_I2C_ADDRESS_CP
            Case Else
                'Should not happen
                Throw New System.Exception("GetGUISelectedHMDIRxMapAddress: cbxHDMIRxMap as an invalid index!:")
        End Select
    End Function















    Public Sub DebugWriteLine(ByVal message As String)
        If EnableDebugConsoleWrite Then
            Debug.WriteLine(message)
        End If
    End Sub




    Private Sub FrmMain_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        If graph1 IsNot Nothing Then
            'pbxCapture.Width = 16 * pbxCapture.Height / 9
            graph1.VideoWindowResized(pbxCapture.Location.X, pbxCapture.Location.Y, pbxCapture.Width, pbxCapture.Height)
            ' graph1.VideoWindowResized(0, pbxCapture.Location.Y, pbxCapture.Width, pbxCapture.Height)
        End If

        btnPrint.Location = New Point(Me.Width / 2 - btnPrint.Width / 2, btnPrint.Location.Y)
        'My.Settings.WindowSizeX = Me.Width
        'My.Settings.WindowSizeY = Me.Height
        'My.Settings.Save()


    End Sub





#Region "Direct show methods"

    Private Sub KillTheGraph()
        If graph1 IsNot Nothing Then
            graph1.Play(False)
            graph1.closeinterfaces()
        End If
        'graph1 = Nothing
    End Sub
    Private Sub ListVideoCaptureDeviceInCombobox()
        Try
            Dim ArrayOfCaptureFilter As String(,) = GENIE1080P.DSVideoGraph.ListVideoCaptureDevice
            Dim VideoInput1PreferredIndex As Integer = 0

            cbxDevice.Items.Clear()

            For i As Integer = 0 To ArrayOfCaptureFilter.GetUpperBound(1)
                cbxDevice.Items.Add(ArrayOfCaptureFilter(0, i))
            Next
            'If cbxDevice.Items.Count > 0 Then
            '    cbxDevice.SelectedIndex = VideoInput1PreferredIndex
            'End If
            For Each item As String In cbxDevice.Items
                If item.Contains("INOGENI") Then
                    cbxDevice.Text = item
                    Exit For
                End If
            Next
        Catch ex As Exception

        End Try

    End Sub
    Private Function VerifyForGENIE1080P() As Boolean
        Dim ArrayOfCaptureFilter As String(,) = ListVideoCaptureDevice()
        SelectedFilterName = ""
        For i As Integer = 0 To ArrayOfCaptureFilter.GetUpperBound(1)

            Dim CLSID As New Guid(ArrayOfCaptureFilter(1, i))
            If CLSID.ToString = ALLOWED_DEVICE.ToString Then
                'We have a winner for input 1
                SelectedFilterName = ArrayOfCaptureFilter(0, i)
                Return True
            Else
                'Ok unknown filter

            End If
        Next
        Return False
    End Function
    Private Sub BreakGraph()
        If graph1 IsNot Nothing Then
            If graph1.isGraphBuilded = True Then
                graph1.BreakTheGraph()
            End If
        End If
    End Sub
    Private Sub BreakAudioGraph()
        If graphAudio IsNot Nothing Then
            If graphAudio.isGraphBuilded = True Then
                graphAudio.BreakTheGraph()
            End If
        End If
    End Sub
    Private Sub BuildGraph()
        If (SelectedFilterName <> "") Then
            If graph1 IsNot Nothing Then
                graph1.BreakTheGraph()
                'graph1 = Nothing
            End If
            If graph1 Is Nothing Then
                graph1 = New GENIE1080P.DSVideoGraph("INOGENI", pbxCapture)
            End If





            graph1.UseAnalyzerFilter = cbxUseanalyser.Checked
            graph1.BuildTheGraph(SelectedFilterName)
            getselectedformat()
        End If
    End Sub
    Private Sub BuildAudioGraph()
        If (SelectedAudioFilterName <> "") Then
            If graphAudio IsNot Nothing Then
                graphAudio.BreakTheGraph()
                'graphAudio = Nothing
            End If
            If graphAudio Is Nothing Then
                graphAudio = New DSAudioGraph("INOGENI Audio", Me.Handle)
            End If

            graphAudio.SelectedAudioDev = SelectedAudioFilterName
            graphAudio.BuildTheGraph()
        End If
    End Sub
    Private Sub updatecap()
        If (SelectedFilterName <> "") Then
            If graph1 IsNot Nothing Then
                graph1.BreakTheGraph()
                'graph1 = Nothing
            End If
            If graph1 Is Nothing Then
                graph1 = New GENIE1080P.DSVideoGraph("INOGENI", pbxCapture)
            End If

            graph1.updatecap(SelectedFilterName)
            getselectedformat()
        End If
    End Sub

    Private Function BuildRecorderGraph(ByVal Filename As String) As Boolean
        If (SelectedFilterName <> "") Then
            If graph1 IsNot Nothing Then
                graph1.BreakTheGraph()
                graph1 = Nothing
            End If

            If graph1 Is Nothing Then
                graph1 = New GENIE1080P.DSVideoGraph("INOGENI", pbxCapture)
            End If
            graph1.UseAnalyzerFilter = cbxUseanalyser.Checked
            Return graph1.BuildRecorderGraph(SelectedFilterName, Filename)
        End If
        Return False
    End Function
    Private Sub Play(ByVal Running As Boolean)
        Try
            If Running Then
                BuildGraph()
                If graph1 IsNot Nothing Then
                    graph1.Play(True)
                End If
            Else
                If graph1 IsNot Nothing Then
                    graph1.Play(False)
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
    Private Sub PlayAudio(ByVal Running As Boolean)
        Try
            If Running Then
                BuildAudioGraph()
                If graphAudio IsNot Nothing Then
                    graphAudio.Play(True)
                End If
            Else
                If graphAudio IsNot Nothing Then
                    graphAudio.Play(False)
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
    Private Sub Record(ByVal Running As Boolean, ByVal Filename As String)
        Try
            If Running Then
                BuildRecorderGraph(Filename)
                If graph1 IsNot Nothing Then
                    graph1.Record(True)
                End If
            Else
                If graph1 IsNot Nothing Then
                    graph1.Record(False)
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
    Private Sub Play()
        PlayFlag = True
        BuildGraph()
        Play(True)
        updatemenu()
        If Form1 IsNot Nothing Then
            Form1.Show()
        End If
        btnPrint.Enabled = True
    End Sub
    Private Sub PlayAudio()
        BuildAudioGraph()
        PlayAudio(True)
        'If Form1 IsNot Nothing Then
        '    Form1.Show()
        'End If
    End Sub
    Private Sub Record()
        Dim FileDiag As New SaveFileDialog
        FileDiag.FileName = "capture.wmv"
        FileDiag.Title = "Save video as..."
        FileDiag.Filter = "Bitmap file (*.wmv)|*.wmv"
        FileDiag.FilterIndex = 1
        If (FileDiag.ShowDialog() <> DialogResult.OK) Then
            Return
        End If

        Record(True, FileDiag.FileName)
        Dim result As DialogResult = DlgWaitCapture.ShowDialog
        Play(False)
        If graph1 IsNot Nothing Then
            graph1.DisconnectTheGraph()
        End If

        'KillTheGraph()
    End Sub
    Private Sub StopCapture()
        btnPrint.Enabled = False
        PlayFlag = False
        Play(False)
        If graph1 IsNot Nothing Then
            graph1.DisconnectTheGraph()
        End If
        ' KillTheGraph()
    End Sub
    Private Sub StopAudioCapture()
        PlayAudio(False)
        If graphAudio IsNot Nothing Then
            graphAudio.DisconnectTheGraph()
        End If
        ' KillTheGraph()
    End Sub
    Private Sub TakeASnapshot()
        If graph1 IsNot Nothing Then
            Dim image2 As System.Drawing.Image = graph1.TakeAPicture3()
            Dim sd As SaveFileDialog = New SaveFileDialog()
            sd.FileName = "Snapshot.bmp"
            sd.Title = "Save Image as..."
            sd.Filter = "Bitmap file (*.bmp)|*.bmp"
            sd.FilterIndex = 1
            If (sd.ShowDialog() <> DialogResult.OK) Then
                Return
            End If


            image2.Save(sd.FileName, System.Drawing.Imaging.ImageFormat.Bmp)
        End If
    End Sub
#End Region

#Region "DirectShow Event Handlers"

    Private Sub graph1_OnGraphBuilded(ByVal Sender As Object, ByVal GraphIsBuilded As Boolean) Handles graph1.OnGraphBuilded

    End Sub
    Private Sub graph1_OnGraphStateChanged(ByVal Sender As Object, ByVal GraphState As GENIE1080P.DSVideoGraph.PlayState) Handles graph1.OnGraphStateChanged
        If GraphState = GENIE1080P.DSVideoGraph.PlayState.Running Then
            btnPlay.Enabled = False
            btnRefresh.Enabled = False
            btnStop.Enabled = True
            cbxDevice.Enabled = False
            cbxCaps.Enabled = False
            enableComboBox(cbxFrameRate, False)
        Else
            btnPlay.Enabled = True
            btnRefresh.Enabled = True
            btnStop.Enabled = False
            cbxDevice.Enabled = True
            cbxCaps.Enabled = True
            enableComboBox(cbxFrameRate, True)
        End If
    End Sub
    Private Sub graph1_OnWindowSizeRequested(ByVal Sender As Object) Handles graph1.OnWindowSizeRequested
        If graph1 IsNot Nothing Then
            graph1.VideoWindowResized(pbxCapture.Location.X, pbxCapture.Location.Y, pbxCapture.Width, pbxCapture.Height)
        End If
    End Sub
#End Region




    Private Sub cbxDevice_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxDevice.SelectedIndexChanged
        SelectedFilterName = cbxDevice.Text
        BreakGraph()
        'If FX3graphpresent Then


        updatecap()

        'End If
        updatemenu()

    End Sub

    Private Sub btnPlay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPlay.Click
        Play()
    End Sub

    Private Sub btnStop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStop.Click

        StopCapture()
    End Sub

    Private Sub updatemenu()



        Dim selectedindex As Integer = 0
        Try



            cbxCaps.Items.Clear()
            cbxFrameRate.Items.Clear()
            cbxFrameRate.Text = ""
            If graph1 IsNot Nothing AndAlso graph1.VideoCapabilities IsNot Nothing Then
                For Each Element As VideoCapabilitiesDef In graph1.VideoCapabilities
                    Dim index As Integer = cbxCaps.Items.Add(String.Format("{0} {1}x{2} ({3}Hz-{4}Hz)", Element.subTypeName, Element.Width, Element.Height, Element.MinFrameRate, Element.MaxFrameRate))

                    If graph1.SelectedCap IsNot Nothing AndAlso graph1.SelectedCap.Compare(Element) Then
                        selectedindex = index
                    End If
                    'cbxCaps.Items(index).Tag = Element
                Next
            End If
        Catch ex As Exception

        End Try

        If cbxCaps.Items.Count Then
            cbxCaps.SelectedIndex = selectedindex
        End If

    End Sub

    Private Sub cbxCaps_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCaps.SelectedIndexChanged
        Dim cap As VideoCapabilitiesDef = getselectedformat()
        UpdateFrameRateSelection(cap)
    End Sub

    Private Function getselectedformat() As VideoCapabilitiesDef
        Dim format As String() = Nothing
        Dim framerate As String = Nothing

        If graph1 IsNot Nothing AndAlso graph1.VideoCapabilities IsNot Nothing Then
            Dim selectedstring As String
            If My.Settings.VideoFormat = "" Then
                selectedstring = String.Format("YUY2 {0} (23.98Hz-60Hz)", "1280x720")
                My.Settings.VideoFormat = "1280x720"
                My.Settings.Save()
                UpdateSettings()
            Else
                selectedstring = String.Format("YUY2 {0} (23.98Hz-60Hz)", My.Settings.VideoFormat)
            End If

            If My.Settings.VideoFramerate = "" Then
                My.Settings.VideoFramerate = "60"
                My.Settings.Save()
                UpdateSettings()
            End If

            For Each Element As VideoCapabilitiesDef In graph1.VideoCapabilities
                If selectedstring = String.Format("{0} {1}x{2} ({3}Hz-{4}Hz)", Element.subTypeName, Element.Width, Element.Height, Element.MinFrameRate, Element.MaxFrameRate) Then

                    format = Split(My.Settings.VideoFormat, "x")
                    Element.Width = format(0)
                    Element.Height = format(1)

                    framerate = My.Settings.VideoFramerate

                    graph1.SelectedCap = Element
                    'Try to update the frame rate
                    If (cbxFrameRate.Text <> "") Then
                        If isNumber(cbxFrameRate.Text, Element.MinFrameRate, Element.MaxFrameRate) Then
                            graph1.SelectedCap.AvgFrameRate = CSng(framerate)
                        End If
                    End If
                    Return Element
                End If
            Next
        End If
        Return Nothing
    End Function

    Private Sub UpdateFrameRateSelection(ByVal Cap As VideoCapabilitiesDef)
        Try
            cbxFrameRate.Items.Clear()
            'Ok fill the combobox with possible value
            If Cap.MinFrameRate = Cap.MaxFrameRate Then
                cbxFrameRate.Items.Add(Cap.MinFrameRate)
            Else


                Dim Value As Single = Cap.MinFrameRate
                Do
                    cbxFrameRate.Items.Add(Value)

                    'Next
                    Value += 1
                    Value = Math.Round(Value)
                Loop While Value <= Cap.MaxFrameRate

                'Let add only unit in between the tolerate
                If Value <> Cap.MaxFrameRate Then
                    cbxFrameRate.Items.Add(Cap.MaxFrameRate)
                End If
            End If
            cbxFrameRate.Text = Cap.AvgFrameRate
        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' Check if the INOGENI device is listed as an unknown device in the system.
    ''' That would mean the device driver is not intakled
    ''' </summary>
    ''' <returns>
    ''' Return true if the GENIE1080P is detected in the system.
    ''' </returns>
    ''' <remarks></remarks>
    Public Shared Function IsGenie1080PPresent() As Boolean
        Try
            Dim Devices As List(Of Device) = Device.GetNonWorkingDevices
            If Devices IsNot Nothing Then
                For Each dev As Device In Devices
                    If (dev.PNPDeviceID.Contains(INOGENI_OLD_PNPID)) Then
                        Return True
                    End If
                    If (dev.PNPDeviceID.Contains(INOGENI_DEXTERA_PNPID)) Then
                        Return True
                    End If
                Next
            End If
            Return False
        Catch ex As Exception
            'MsgBox(ex.ToString)
        End Try
    End Function

    Private Sub Timer3_Tick1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer3.Tick
        Try
            If PlayFlag = True And isINOGENIDevicePresent() = False Then
                StopCapture()
                While isINOGENIDevicePresent() = False
                    Timer3.Enabled = False
                    MsgBox("The INOGENI device has been disconnected. Please reconnect the INOGENI device and click OK.")
                    Wait(3000)
                End While
                Wait(12000)
                ListVideoCaptureDeviceInCombobox()
                ListAudioCaptureDeviceInCombobox()
                Play()
                Timer3.Enabled = True
            End If

            If graph1 IsNot Nothing AndAlso graph1.isGraphRunning AndAlso graph1.isInogeniAnalyserWorking Then

                '  GroupBox17.Visible = True
                'Get video format and software frame rate
                Dim PixelFormat As DSVideoGraph.VideoFormat = Nothing
                PixelFormat.FrameRate = -1
                PixelFormat.Height = -1
                PixelFormat.Width = -1
                If graph1 IsNot Nothing AndAlso graph1.isGraphRunning Then
                    graph1.GetInogeniAnalyzerResolution(PixelFormat)
                End If

                If PixelFormat.Width > 0 AndAlso PixelFormat.Height > 0 AndAlso PixelFormat.FrameRate > 0 Then
                    Label24.Text = String.Format("{0}x{1}@{2} fps", PixelFormat.Width, PixelFormat.Height, PixelFormat.FrameRate)
                ElseIf PixelFormat.Width > 0 AndAlso PixelFormat.Height > 0 Then
                    Label24.Text = String.Format("{0}x{1}", PixelFormat.Width, PixelFormat.Height)
                Else
                    Label24.Text = "No Format Detected!"
                End If

                'Get Presentation Time
                Dim PTSStart As Int64 = -1
                Dim PTSEnd As Int64 = -1
                If graph1 IsNot Nothing AndAlso graph1.isGraphRunning Then
                    graph1.GetInogeniAnalyzerPTS(PTSStart, PTSEnd)
                End If
                If PTSStart >= 0 AndAlso PTSEnd >= 0 Then
                    Label25.Text = String.Format("{0}ms --> {1}ms", Int(PTSStart / 10000), Int(PTSEnd / 10000))
                    Label27.Text = String.Format("{0}.{1}ms", Int((PTSEnd - PTSStart) / 10000), Int((PTSEnd - PTSStart) Mod 10000))


                Else
                    Label25.Text = "No PTS Detected!"
                    Label27.Text = "No PTS Detected!"
                End If
            Else
                '   GroupBox17.Visible = False
            End If
        Catch ex As Exception
        End Try

        'Renderer Info
        Dim stat As RendereringProp
        Try
            If graph1 IsNot Nothing AndAlso graph1.isGraphRunning Then
                If graph1.GetRenderStatistics(stat) Then
                    lblAverageFrameRate.Text = String.Format("{0:0.00}", stat.AverageFrameRate / 100)
                    lblFrameDisplayed.Text = stat.FramesDrawn.ToString
                    lblFrameDropped.Text = stat.FramesDropped.ToString
                    lblJitter.Text = stat.Jitter.ToString
                    lblSyncOffset.Text = stat.SyncOffset.ToString
                    lblDevSyncOffset.Text = stat.DevSyncOffset.ToString
                Else
                    Dim errorstr As String = "Error"
                    lblAverageFrameRate.Text = errorstr
                    lblFrameDisplayed.Text = errorstr
                    lblFrameDropped.Text = errorstr
                    lblJitter.Text = errorstr
                    lblSyncOffset.Text = errorstr
                    lblDevSyncOffset.Text = errorstr
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub cbxUseanalyser_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxUseanalyser.CheckedChanged
        If graph1 IsNot Nothing Then
            If graph1.isGraphRunning Then
                StopCapture()
            End If
            graph1.UseAnalyzerFilter = cbxUseanalyser.Checked
        End If
    End Sub

    Private Sub cbxFrameRate_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxFrameRate.SelectedIndexChanged
        If graph1 IsNot Nothing AndAlso graph1.VideoCapabilities IsNot Nothing Then

            Dim selectedstring As String = cbxCaps.Text
            For Each Element As VideoCapabilitiesDef In graph1.VideoCapabilities
                If selectedstring = String.Format("{0} {1}x{2} ({3}Hz-{4}Hz)", Element.subTypeName, Element.Width, Element.Height, Element.MinFrameRate, Element.MaxFrameRate) Then
                    graph1.SelectedCap = Element
                    If (cbxFrameRate.Text <> "") Then
                        If isNumber(cbxFrameRate.Text, Element.MinFrameRate, Element.MaxFrameRate) Then
                            graph1.SelectedCap.AvgFrameRate = CSng(cbxFrameRate.Text)
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    Private Sub cbxFrameRate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxFrameRate.TextChanged

    End Sub

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Try
            ListVideoCaptureDeviceInCombobox()
            updatemenu()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub graphAudio_OnGraphStateChanged(ByVal Sender As Object, ByVal GraphState As DSAudioGraph.PlayState) Handles graphAudio.OnGraphStateChanged
        If GraphState = DSAudioGraph.PlayState.Running Then
            btnPlayAudio.Enabled = False
            btnStopAudio.Enabled = True
            cbxDeviceAudio.Enabled = False
            btnRefreshAudioDevice.Enabled = False
        Else
            btnPlayAudio.Enabled = True
            btnStopAudio.Enabled = False
            cbxDeviceAudio.Enabled = True
            btnRefreshAudioDevice.Enabled = True
        End If
    End Sub

    Private Sub cbxDeviceAudio_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxDeviceAudio.SelectedIndexChanged
        SelectedAudioFilterName = cbxDeviceAudio.Text
        BreakAudioGraph()
    End Sub

    Private Sub btnPlayAudio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPlayAudio.Click
        PlayAudio()
    End Sub

    Private Sub btnStopAudio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStopAudio.Click
        StopAudioCapture()
    End Sub

    Private Sub btnRefreshAudioDevice_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefreshAudioDevice.Click
        ListAudioCaptureDeviceInCombobox()
    End Sub

    Private Function GrabAFrame() As Bitmap
        Try
            Dim frame As Byte() = Nothing
            Dim cap As VideoCapabilitiesDef

            frame = graph1.GrabAFrame()
            cap = getselectedformat()

            Dim bmp As Bitmap = New Bitmap(cap.Width, cap.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb)
            Dim bitmapData As System.Drawing.Imaging.BitmapData = bmp.LockBits(New System.Drawing.Rectangle(0, 0, bmp.Width, bmp.Height), Imaging.ImageLockMode.ReadWrite, bmp.PixelFormat)
            Dim pNative As IntPtr = bitmapData.Scan0
            Marshal.Copy(frame, 0, pNative, cap.Width * cap.Height * 3)
            bmp.UnlockBits(bitmapData)
            ' Since we capture in RGB, we need to flip image since we receive the image upside down
            bmp.RotateFlip(RotateFlipType.RotateNoneFlipY)

            Return (bmp)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return Nothing
    End Function

    Private Sub btnPrint_Click(sender As System.Object, e As System.EventArgs) Handles btnPrint.Click
        Try
            Dim status As String
            Dim start As Integer = 0
            Dim errorOccured As Boolean = True

            'Let's pause the stream to show user which image will be printed to the printer.
            graph1.Pause(True)

            bmp = GrabImage()

            'Let's print the image
            Printing()

            start = GetTickCount()
            While (GetTickCount() < start + 20000)
                status = GetPrinterStatus(My.Settings.PrinterName)
                lblStatus.Text = status
                If status = "Idle" Then
                    errorOccured = False
                    Exit While
                End If
            End While

            If errorOccured Then
                lblStatus.Text = "An error occured"
            End If

            graph1.Pause(False)

            Return
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Sub SaveBitmapToFile(ByRef bitmap As Bitmap)
        Try
            Dim dateToday As System.DateTime = System.DateTime.Now
            Dim AppDirectory As String = String.Format("{0}\img", Application.StartupPath)
            Dim picturePathString As String = String.Format("{0}\img\{1}_{2}_{3}_{4}_{5}_{6}.jpg", Application.StartupPath, dateToday.Year, dateToday.Month, dateToday.Day, dateToday.Hour, dateToday.Minute, dateToday.Second)

            If Not Directory.Exists(AppDirectory) Then
                System.IO.Directory.CreateDirectory(AppDirectory)
            End If

            pictureFilePath = picturePathString
            bitmap.Save(picturePathString)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Function GrabImage() As Bitmap
        Try
            Dim bmp As Bitmap = Nothing
            bmp = GrabAFrame()
            'SaveBitmapToFile(bmp)
            Return bmp
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function

    Private Sub prntDoc_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles prntDoc.PrintPage
        Try
            Dim pd As New Printing.PrintDocument()
            'Dim bmp As New Bitmap(pbxCapture.Width, pbxCapture.Height)
            Dim pageSettings As System.Drawing.Printing.PageSettings = New System.Drawing.Printing.PageSettings

            Dim newWidth As Double = bmp.Width * 100
            Dim newHeight As Double = bmp.Height * 100
            Dim margins As New System.Drawing.Printing.Margins(100, 100, 100, 100)

            Dim widthFactor As Double = newWidth / (e.PageBounds.Width - margins.Left * 2)
            Dim heightFactor As Double = newHeight / (e.PageBounds.Height - margins.Left * 2)

            Dim newWidthInteger As Integer
            Dim newHeigthInteger As Integer

            Dim y As Integer



            prntDoc.DefaultPageSettings.Landscape = True
            prntDoc.DefaultPageSettings.Margins = margins


            If widthFactor > 1 Or heightFactor > 1 Then
                If widthFactor > heightFactor Then
                    newWidth = newWidth / widthFactor
                    newHeight = newHeight / widthFactor
                Else
                    newWidth = newWidth / heightFactor
                    newHeight = newHeight / heightFactor
                End If
            End If

            newWidthInteger = newWidth
            newHeigthInteger = newHeight

            y = (e.PageBounds.Height * 100 - newHeigthInteger * 100) / 2 / 100
            e.Graphics.DrawImage(bmp, margins.Left, y, newWidthInteger, newHeigthInteger)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Enum PrinterStatus
        PrinterIdle = 3
        PrinterPrinting = 4
        PrinterWarmingUp = 5
    End Enum

    Private Function PrinterStatusToString(ByVal ps As PrinterStatus) As String
        Dim s As String
        Select Case ps
            Case 1
                Return "Waiting..."
            Case 2
                Return "Unknown"
            Case PrinterStatus.PrinterIdle
                Return "Idle"
            Case PrinterStatus.PrinterPrinting
                Return "Printing..."
            Case PrinterStatus.PrinterWarmingUp
                Return "Warmup"
            Case 6
                Return "Stopped Printing"
            Case 7
                Return "Offline"
            Case 8
                Return "Paused"
            Case 9
                Return "Error"
            Case 10
                Return "Busy"
            Case 11
                Return "Not Available"
            Case 12
                Return "Waiting"
            Case 13
                Return "Processing"
            Case 14
                Return "Initialization"
            Case 15
                Return "Power Save"
            Case 16
                Return "Pending Deletion"
            Case 17
                Return "I/O Active"
            Case 18
                Return "Manual Feed"
            Case Else
                Return "unknown"
        End Select
        Return s
    End Function

    Public Sub Printing()
        Try
            Try
                Dim status As String
                Dim pd As New Printing.PrintDocument()
                AddHandler pd.PrintPage, AddressOf prntDoc_PrintPage
                'pd.PrinterSettings.PrinterName = printer
                ' Set the page orientation to landscape.
                pd.PrinterSettings.PrinterName = My.Settings.PrinterName
                pd.DefaultPageSettings.Landscape = True

                pd.Print()
            Finally
                'streamToPrint.Close()
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Function ResizeBitmap(ByVal bitmapToResize As Bitmap, ByVal width As Integer, ByVal height As Integer) As Bitmap
        'make a blank bitmap the correct size
        Dim NewBitmap As New Bitmap(width, height)
        'make an instance of graphics that will draw on "NewBitmap"
        Dim BitmpGraphics As Graphics = Graphics.FromImage(NewBitmap)
        'work out the scale factor
        Dim scaleFactorX As Double = width / bitmapToResize.Width
        Dim scaleFactorY As Double = height / bitmapToResize.Height
        'resize the graphics
        BitmpGraphics.ScaleTransform(scaleFactorX, scaleFactorY)
        'draw the bitmap to NewBitmap
        BitmpGraphics.DrawImage(bitmapToResize, 0, 0)
        Return NewBitmap
    End Function

    Private Sub QuitToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles QuitToolStripMenuItem.Click
        Application.Exit()
    End Sub

    Private Sub AboutToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AboutToolStripMenuItem.Click
        Dim INOGENIDevicesList() As String = {""}
        Dim inogeniInfo As INOGENILib.INOGENIInfo
        Dim str As String
        Dim model As String = "Unknown"
        Dim fx3Version As String = "N/A"
        Dim fpgaVersion As String = "N/A"
        Dim edidVersion As String = "N/A"
        Dim videoInput As INOGENILib.INOGENIInfo.VideoFormat
        Dim videoFormat As String = Nothing

        INOGENIDevicesList = INOGENILib.INOGENIDVI_2_USB3.ListINOGENIDevice
        If INOGENIDevicesList IsNot Nothing Then
            inogeniInfo = New INOGENILib.INOGENIInfo(INOGENIDevicesList(0))

            If INOGENIDevicesList(0).Contains("2997") Then
                If INOGENIDevicesList(0).Contains("0001") Then
                    model = "DVIUSB"
                ElseIf INOGENIDevicesList(0).Contains("0004") Then
                    model = "4K2USB3"
                Else
                    model = "Unknown"
                End If
            End If
            inogeniInfo.RefreshVersionInfo()
            inogeniInfo.RefreshVideoInfo()
            fx3Version = inogeniInfo.USB3ControllerVersionString
            fpgaVersion = inogeniInfo.VideoProcessorVersionString
            edidVersion = inogeniInfo.EDIDVersionString
            videoInput = inogeniInfo.VideoFormatDetected

            If videoInput.Width = 0 Then
                videoFormat = "No input"
            Else
                videoFormat = String.Format("{0}x{1}@{2}", videoInput.Width, videoInput.Height, videoInput.FrameRate)
            End If
            inogeniInfo.Terminate()
        End If

        str = String.Format("INOGENI Snapshot v{0}.{1}.{2} {3}Model : {4}{5}USB3 Controller Version : {6}{7}Video Processor Version : {8}{9}EDID Version : {10}{11}Video Input : {12}", _
                            My.Application.Info.Version.Major, _
                            My.Application.Info.Version.Minor, _
                            My.Application.Info.Version.Build, _
                            vbCrLf, _
                            model, _
                            vbCrLf, _
                            fx3Version, _
                            vbCrLf, _
                            fpgaVersion, _
                            vbCrLf, _
                            edidVersion, _
                            vbCrLf, _
                            videoFormat)
        MsgBox(str)
    End Sub

    Private Sub UpdateSettings()
        Dim i As Integer = 0

        VideoFormatToolStripMenuItem.DropDownItems.Clear()
        VideoFrameRateToolStripMenuItem.DropDownItems.Clear()
        PrinterToolStripMenuItem.DropDownItems.Clear()

        VideoFormatToolStripMenuItem.DropDownItems.Add("1280x720")
        VideoFormatToolStripMenuItem.DropDownItems.Add("1920x1080")
        VideoFormatToolStripMenuItem.DropDownItems.Add("1920x1200")
        VideoFormatToolStripMenuItem.DropDownItems.Add("720x480")
        VideoFormatToolStripMenuItem.DropDownItems.Add("720x576")
        VideoFormatToolStripMenuItem.DropDownItems.Add("640x480")
        VideoFormatToolStripMenuItem.DropDownItems.Add("800x600")
        VideoFormatToolStripMenuItem.DropDownItems.Add("1024x768")
        VideoFormatToolStripMenuItem.DropDownItems.Add("1280x1024")
        VideoFormatToolStripMenuItem.DropDownItems.Add("1280x960")


        VideoFrameRateToolStripMenuItem.DropDownItems.Add("60")
        VideoFrameRateToolStripMenuItem.DropDownItems.Add("59.94")
        VideoFrameRateToolStripMenuItem.DropDownItems.Add("50")
        VideoFrameRateToolStripMenuItem.DropDownItems.Add("30")
        VideoFrameRateToolStripMenuItem.DropDownItems.Add("29.97")
        VideoFrameRateToolStripMenuItem.DropDownItems.Add("25")
        VideoFrameRateToolStripMenuItem.DropDownItems.Add("24")
        VideoFrameRateToolStripMenuItem.DropDownItems.Add("23.98")

        GetPrinters()

        i = 0
        For Each item As ToolStripMenuItem In VideoFormatToolStripMenuItem.DropDownItems
            If My.Settings.VideoFormat = VideoFormatToolStripMenuItem.DropDownItems(i).Text Then
                CType(VideoFormatToolStripMenuItem.DropDownItems(i), ToolStripMenuItem).Checked = True
            End If
            i += 1
        Next

        i = 0
        For Each item As ToolStripMenuItem In VideoFrameRateToolStripMenuItem.DropDownItems
            If My.Settings.VideoFramerate = VideoFrameRateToolStripMenuItem.DropDownItems(i).Text Then
                CType(VideoFrameRateToolStripMenuItem.DropDownItems(i), ToolStripMenuItem).Checked = True
            End If
            i += 1
        Next

        i = 0
        For Each item As ToolStripMenuItem In PrinterToolStripMenuItem.DropDownItems
            If My.Settings.PrinterName = PrinterToolStripMenuItem.DropDownItems(i).Text Then
                CType(PrinterToolStripMenuItem.DropDownItems(i), ToolStripMenuItem).Checked = True
            End If
            i += 1
        Next

        If My.Settings.VideoFormat = "" Then
            My.Settings.VideoFormat = "1280x720"
        End If

        If My.Settings.VideoFramerate = "" Then
            My.Settings.VideoFramerate = "60"
        End If

        If My.Settings.PrinterName = "" Then
            If PrinterToolStripMenuItem.DropDownItems(0) IsNot Nothing Then
                My.Settings.PrinterName = PrinterToolStripMenuItem.DropDownItems(0).Text
            End If
        End If
    End Sub

    Private Function GetPrinterStatus(ByVal printerName As String) As String
        Dim status As String = "Unknown"
        Dim strPrintServer As String
        strPrintServer = "localhost"
        Dim WMIObject As String, PrinterSet As Object, Printer As Object
        WMIObject = "winmgmts://" & strPrintServer
        PrinterSet = GetObject(WMIObject).InstancesOf("win32_Printer")
        For Each Printer In PrinterSet
            If Printer.Name = printerName Then
                status = PrinterStatusToString(Printer.PrinterStatus)
                Return status
            End If
        Next Printer
        Return status
    End Function

    Private Sub GetPrinters()
        Dim strPrintServer As String
        strPrintServer = "localhost"
        Dim WMIObject As String, PrinterSet As Object, Printer As Object
        WMIObject = "winmgmts://" & strPrintServer
        PrinterSet = GetObject(WMIObject).InstancesOf("win32_Printer")
        For Each Printer In PrinterSet
            'MsgBox( _
            'Printer.Name & ": " & _
            'PrinterStatusToString(Printer.PrinterStatus) _
            ')
            PrinterToolStripMenuItem.DropDownItems.Add(Printer.Name)
        Next Printer
    End Sub

    Private Sub VideoFormatToolStripMenuItem_Click(sender As System.Object, ByVal e As ToolStripItemClickedEventArgs) Handles VideoFormatToolStripMenuItem.DropDownItemClicked
        My.Settings.VideoFormat = e.ClickedItem.Text
        My.Settings.Save()
        UpdateSettings()

        StopCapture()
        Play()
    End Sub

    Private Sub VideoFrameRateToolStripMenuItem_Click(sender As System.Object, ByVal e As ToolStripItemClickedEventArgs) Handles VideoFrameRateToolStripMenuItem.DropDownItemClicked
        My.Settings.VideoFramerate = e.ClickedItem.Text
        My.Settings.Save()
        UpdateSettings()

        StopCapture()
        Play()
    End Sub

    Private Sub PrinterToolStripMenuItem_Click(sender As System.Object, ByVal e As ToolStripItemClickedEventArgs) Handles PrinterToolStripMenuItem.DropDownItemClicked
        My.Settings.PrinterName = e.ClickedItem.Text
        My.Settings.Save()
        UpdateSettings()

        'StopCapture()
        'Play()
    End Sub
End Class
