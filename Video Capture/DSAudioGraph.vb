﻿#Region "Options"
Option Explicit On
Option Strict Off
#End Region

#Region "Copyright"
'Dextera Labs Inc.
'(c) Copyright 2005-2014 Dextera Labs Inc.
'ALL RIGHTS RESERVED

'The software, source code and information contained herein are
'proprietary to, and comprise valuable intellectual property and
'trade secrets of Dextera Labs Inc.

'This software is furnished pursuant to a Source Code License
'Agreement between Dextera and the Licensee and may be used,
'copied, transmitted, and stored only by the Licensee in accordance
'with the terms of such license and with the inclusion of this
'copyright notice.

'This software, source code and information or any other copies
'thereof may not be provided or otherwise made available to any
'person not bound by the terms of the Source Code License Agreement.
#End Region


#Region "imports"
Imports System.Runtime.InteropServices
Imports DirectShowLib
Imports System.Runtime.InteropServices.ComTypes
Imports System.Threading
Imports VBLibraries.Utils.BaseUtility.Time
Imports INOGENISampleApp.INOGENIInterfaceModule.HRESULT
#End Region

Public Class DSAudioGraph
#Region "Events"
    Public Event OnWindowSizeRequested(ByVal Sender As Object)
    Public Event OnGraphStateChanged(ByVal Sender As Object, ByVal GraphState As PlayState)
    Public Event OnGraphBuilded(ByVal Sender As Object, ByVal GraphIsBuilded As Boolean)
#End Region

#Region "Constant"
    Public Const VIDEO_STREAM_CONFIG_CAPS As Integer = 128
    Dim D As Integer = Convert.ToInt32("0X8000", 16)
    Public WM_GRAPHNOTIFY As Integer = D + 1
#End Region

#Region "Enums"
    Enum PlayState
        Stopped
        Paused
        Running
        Init
    End Enum
    Enum WindowType
        SeparatedWindow
        PictureBox
    End Enum

#End Region

#Region "inner Classes"
    
#End Region


#Region "Properties"
    Private _CurrentState As PlayState = PlayState.Stopped
    Private Property CurrentState As PlayState
        Get
            Return _CurrentState
        End Get
        Set(ByVal value As PlayState)
            If value <> _CurrentState Then
                RaiseEvent OnGraphStateChanged(Me, value)
            End If
            _CurrentState = value
        End Set
    End Property

    Private _SelectedAudioDev As String
    Public Property SelectedAudioDev As String
        Get
            Return _SelectedAudioDev
        End Get
        Set(ByVal value As String)
            _SelectedAudioDev = value
        End Set
    End Property



    Private VideoWindow As IVideoWindow = Nothing
    Public Shared MediaControl As IMediaControl = Nothing
    Private MediaEventEx As IMediaEventEx = Nothing
    Protected GraphBuilder As IGraphBuilder = Nothing
    Protected CaptureGraphBuilder As ICaptureGraphBuilder2 = Nothing
    Protected ConfigStreamCaps As IAMStreamConfig = Nothing
    'Recorder
    Private MediaControlRecorder As IMediaControl = Nothing
    Private FilterGraph As IFilterGraph2 = Nothing
    'SampleGrabber

    


    Protected AudioRendererFilter As IBaseFilter = Nothing

    Protected AudioSourceFilter As IBaseFilter

    Private Handle As System.IntPtr


    'Filter ID
    Protected _VideoSourceCLSID As Guid
    Protected _VideoSourceFilterName As String
    Public ReadOnly Property VideoSourceFilterName() As String
        Get
            Return _VideoSourceFilterName
        End Get
    End Property
    Public ReadOnly Property SourceFilterCLSID As Guid
        Get
            Return _VideoSourceCLSID
        End Get
    End Property

    Protected _AudioInputCLSID As Guid
    Public ReadOnly Property AudioInputCLSID As Guid
        Get
            Return _AudioInputCLSID
        End Get
    End Property

    Protected _AudioInputName As String
    Public ReadOnly Property AudioInputName As String
        Get
            Return _AudioInputName
        End Get
    End Property


    Public ReadOnly Property isGraphBuilded() As Boolean
        Get
            If GraphBuilder Is Nothing OrElse CaptureGraphBuilder Is Nothing OrElse VideoWindow Is Nothing OrElse MediaControl Is Nothing OrElse MediaEventEx Is Nothing OrElse VideoSourceFilterName = "" Then
                Return False
            Else
                Return True
            End If
        End Get
    End Property
    Public ReadOnly Property isGraphRunning() As Boolean
        Get
            If _CurrentState = PlayState.Running Then
                Return True
            End If
            Return False
        End Get
    End Property

    Private rot As DsROTEntry = Nothing
    Protected VideoInfoHeader As VideoInfoHeader

    Private _CapturePictureBox As PictureBox
    Public Property CapturePictureBox() As PictureBox
        Get
            Return _CapturePictureBox
        End Get
        Set(ByVal value As PictureBox)
            _CapturePictureBox = value
        End Set
    End Property

    Private _OutputType As WindowType
    Public Property OutputType() As WindowType
        Get
            Return _OutputType
        End Get
        Set(ByVal value As WindowType)
            _OutputType = value
        End Set
    End Property
#End Region

#Region "Constructor"
    Public Sub New(ByVal name As String, ByVal VideoOutput As WindowType)
        _OutputType = VideoOutput
        _SelectedAudioDev = ""
    End Sub

    Public Sub New(ByVal name As String, ByRef outputForm As System.IntPtr)
        Me._OutputType = WindowType.SeparatedWindow
        Me.Handle = outputForm
        Me.CapturePictureBox = Nothing
        _SelectedAudioDev = ""
    End Sub
    Public Sub New(ByVal name As String, ByRef CaptureBox As PictureBox)
        Me._OutputType = WindowType.PictureBox
        Me.Handle = Nothing
        Me.CapturePictureBox = CaptureBox
        _SelectedAudioDev = ""
    End Sub
#End Region


#Region "Interfaces"

#End Region

#Region "Direct show methods"
    Public Function BuildTheGraph() As Boolean
        If Me._SelectedAudioDev = "" Then
            Return False
        End If
        Return CaptureAudio(Me._SelectedAudioDev)
    End Function
    Public Sub BreakTheGraph()
        closeinterfaces()
    End Sub


    'Private Function GetPin(ByRef Filter As IBaseFilter, ByRef name As String) As IPin

    'End Function
    Private Function CaptureAudio(ByVal FilterName As String) As Boolean
        Dim hr As Integer = 0
        Dim sourceFilter As IBaseFilter = Nothing
        Try
            Me._SelectedAudioDev = FilterName
            closeinterfaces()
            GetInterfaces()


            hr = Me.CaptureGraphBuilder.SetFiltergraph(Me.GraphBuilder) 'Specifies filter graph "graphbuilder" for the capture graph builder "captureGraphBuilder" to use.
            Debug.WriteLine("Attach the filter graph to the capture graph : " & DsError.GetErrorText(hr))
            DsError.ThrowExceptionForHR(hr)

            'Ok add the audio input device
            If _SelectedAudioDev <> "" Then
                AudioSourceFilter = FindAudioInput(Me._SelectedAudioDev)


                If AudioSourceFilter IsNot Nothing Then
                    hr = Me.GraphBuilder.AddFilter(AudioSourceFilter, "Audio input")
                    DsError.ThrowExceptionForHR(hr)

                    'Add Audio renderer
                    AudioRendererFilter = CType(New AudioRender(), IBaseFilter)
                    hr = Me.GraphBuilder.AddFilter(AudioRendererFilter, "Audio Renderer")

                    ''Connect the 2 filters
                    'Dim PinInogeniOutput As IPin = Nothing
                    'hr = Me.CaptureGraphBuilder.FindPin(Me.AudioSourceFilter, PinDirection.Output, Nothing, Nothing, False, 0, PinInogeniOutput)
                    'Debug.WriteLine("FindPin of Inogeni audio to our graph : " & DsError.GetErrorText(hr))
                    'DsError.ThrowExceptionForHR(hr)


                    'Dim PinAudioRendererInput As IPin = Nothing
                    'hr = Me.CaptureGraphBuilder.FindPin(Me.AudioRendererFilter, PinDirection.Input, Nothing, Nothing, False, 0, PinAudioRendererInput)
                    'Debug.WriteLine("FindPin of Audio renderer to our graph : " & DsError.GetErrorText(hr))
                    'DsError.ThrowExceptionForHR(hr)

                    'hr = Me.GraphBuilder.Connect(PinInogeniOutput, PinAudioRendererInput)
                    'Debug.WriteLine("Connect to Inogeni audio to renderer : " & DsError.GetErrorText(hr))
                    'DsError.ThrowExceptionForHR(hr)

                    hr = Me.CaptureGraphBuilder.RenderStream(PinCategory.Capture, MediaType.Audio, AudioSourceFilter, Nothing, AudioRendererFilter)
                    'hr = Me.GraphBuilder.Render(PinInput)
                    ' hr = Me.CaptureGraphBuilder.RenderStream(PinCategory.Preview, MediaType.Video, sourceFilter, baseGrabFlt, videoRendererFilter)
                    ' hr = Me.CaptureGraphBuilder.RenderStream(PinCategory.Capture, MediaType.Video, sourceFilter, Nothing, Nothing)
                    Debug.WriteLine("Render the preview pin on the video capture filter : " & DsError.GetErrorText(hr))
                    DsError.ThrowExceptionForHR(hr)

                    RaiseEvent OnGraphBuilded(Me, True)
                    Return True
                End If
            End If


            Return False
        Catch ex As Exception
            MessageBox.Show("An unrecoverable error has occurred.With error : " & ex.ToString)
            Return False
        End Try
    End Function
    
  
    

    
   

    Public Sub Play(ByVal play As Boolean)
        Dim hr As Integer = 0
        Try
            If play Then
                If Me.CurrentState <> PlayState.Running Then
                    'SetupVideoWindow()

                    If Not (rot Is Nothing) Then
                        rot.Dispose()
                        rot = Nothing
                    End If
                    rot = New DsROTEntry(Me.GraphBuilder)

                    Dim success As Boolean = False ' Alert UGLY patch, the run will sometimes generate an exception
                    Dim RetryCount As Integer = 10
                    Do
                        Try
                            hr = Me.MediaControl.Run()
                            Debug.WriteLine("Start capturing video data : " & DsError.GetErrorText(hr))
                            DsError.ThrowExceptionForHR(hr)
                            success = True
                        Catch ex As Exception
                            Debug.WriteLine("Start capturing video data exception: " & ex.Message)
                            success = False
                        End Try
                        RetryCount -= 1
                    Loop While RetryCount > 0 AndAlso success = False





                    Me.CurrentState = PlayState.Running
                    Debug.WriteLine("The currentstate : " & Me.CurrentState.ToString)
                End If

            Else
                If Me.CurrentState <> PlayState.Stopped Then
                    hr = Me.MediaControl.Stop
                    Debug.WriteLine("Start capturing video data : " & DsError.GetErrorText(hr))
                    DsError.ThrowExceptionForHR(hr)

                    Me.CurrentState = PlayState.Stopped
                    Debug.WriteLine("The currentstate : " & Me.CurrentState.ToString)
                End If

            End If
        Catch ex As Exception
            MessageBox.Show("An unrecoverable error has occurred.With error : " & ex.ToString)
        End Try
    End Sub
    Public Sub Record(ByVal play As Boolean)
        Dim hr As Integer = 0
        Try
            If play Then
                If Me.CurrentState <> PlayState.Running Then

                    '  rot = New DsROTEntry(Me.GraphBuilder)

                    hr = Me.MediaControlRecorder.Run()
                    Debug.WriteLine("Start capturing video data : " & DsError.GetErrorText(hr))
                    DsError.ThrowExceptionForHR(hr)

                    Me.CurrentState = PlayState.Running
                    Debug.WriteLine("The currentstate : " & Me.CurrentState.ToString)
                End If

            Else
                If Me.CurrentState <> PlayState.Stopped Then
                    hr = Me.MediaControlRecorder.Stop
                    Debug.WriteLine("Start capturing video data : " & DsError.GetErrorText(hr))
                    DsError.ThrowExceptionForHR(hr)

                    Me.CurrentState = PlayState.Stopped
                    Debug.WriteLine("The currentstate : " & Me.CurrentState.ToString)
                End If

            End If
        Catch ex As Exception
            MessageBox.Show("An unrecoverable error has occurred.With error : " & ex.ToString)
        End Try
    End Sub
    
    Public Shared Function ListAudioCaptureDevice() As String(,)
        Try
            Dim Array(,) As String = Nothing
            Dim NumberOfElement As Integer = 0
            Dim AudioInputDevices As DsDevice() = DsDevice.GetDevicesOfCat(DirectShowLib.FilterCategory.AudioInputDevice)
            If AudioInputDevices IsNot Nothing Then
                For Each device As DsDevice In AudioInputDevices
                    If device IsNot Nothing Then
                        If NumberOfElement = 0 Then
                            ReDim Array(1, 0)
                        Else
                            ReDim Preserve Array(1, NumberOfElement)
                        End If

                        Array(0, NumberOfElement) = device.Name
                        Array(1, NumberOfElement) = device.GetPropBagValue("CLSID")
                        NumberOfElement += 1
                    End If
                Next
            End If

            Return Array
        Catch ex As Exception
            MessageBox.Show("An unrecoverable error has occurred.With error : " & ex.ToString)
            Return Nothing
        End Try
    End Function

    Protected Sub GetInterfaces()
        Dim hr As Integer = 0
        Me.GraphBuilder = CType(New FilterGraph, IGraphBuilder)
        Me.CaptureGraphBuilder = CType(New CaptureGraphBuilder2, ICaptureGraphBuilder2)
        Me.MediaControl = CType(Me.GraphBuilder, IMediaControl)
        Me.VideoWindow = CType(Me.GraphBuilder, IVideoWindow)
        Me.MediaEventEx = CType(Me.GraphBuilder, IMediaEventEx)

        'Recorder
        Me.FilterGraph = CType(New FilterGraph, IFilterGraph2)



        If Me._OutputType = WindowType.PictureBox Then
            Dim comtype As Type = Type.GetTypeFromCLSID(DShowNET.Clsid.SampleGrabber)
            Dim comobj As Object = Activator.CreateInstance(comtype)


            'TODO replace that after window creation
            hr = Me.MediaEventEx.SetNotifyWindow(Me._CapturePictureBox.Handle, WM_GRAPHNOTIFY, IntPtr.Zero) 'This method designates a window as the recipient of messages generated by or sent to the current DirectShow object
            DsError.ThrowExceptionForHR(hr) 'ThrowExceptionForHR is a wrapper for Marshal.ThrowExceptionForHR, but additionally provides descriptions for any DirectShow specific error messages.If the hr value is not a fatal error, no exception will be thrown:
            Debug.WriteLine("I started Sub Get interfaces , the result is : " & DsError.GetErrorText(hr))
        ElseIf Me._OutputType = WindowType.SeparatedWindow Then


            'TODO replace that after window creation
            hr = Me.MediaEventEx.SetNotifyWindow(Me.Handle, WM_GRAPHNOTIFY, IntPtr.Zero) 'This method designates a window as the recipient of messages generated by or sent to the current DirectShow object
            DsError.ThrowExceptionForHR(hr) 'ThrowExceptionForHR is a wrapper for Marshal.ThrowExceptionForHR, but additionally provides descriptions for any DirectShow specific error messages.If the hr value is not a fatal error, no exception will be thrown:
            Debug.WriteLine("I started Sub Get interfaces , the result is : " & DsError.GetErrorText(hr))
        End If


    End Sub

    Public Overridable Function FindAudioInput(ByVal FilterName As String) As IBaseFilter
        Try
            Debug.WriteLine("Start the Sub FindAudioInput")
            Dim hr As Integer = 0
            Dim classEnum As IEnumMoniker = Nothing
            Dim moniker As IMoniker() = New IMoniker(0) {}
            Dim source As Object = Nothing
            Dim devEnum As ICreateDevEnum = CType(New CreateDevEnum, ICreateDevEnum)
            hr = devEnum.CreateClassEnumerator(FilterCategory.AudioInputDevice, classEnum, 0)
            Debug.WriteLine("Create an enumerator for the audio capture devices : " & DsError.GetErrorText(hr))
            DsError.ThrowExceptionForHR(hr)
            Marshal.ReleaseComObject(devEnum)
            If classEnum Is Nothing Then
                Throw New ApplicationException("No Audio Input device was detected.\r\n\r\n" & _
                               "This sample requires a Audio Input device,\r\n" & _
                               "to be installed and working properly.  The sample will now close.")
            End If
            Dim DeviceFound As Boolean = False
            While classEnum.Next(moniker.Length, moniker, IntPtr.Zero) = 0 And DeviceFound = False
                Dim iid As Guid = GetType(IBaseFilter).GUID
                Dim bagobj As Object = Nothing
                Dim bag As IPropertyBag = Nothing
                Dim bagguid As Guid = GetType(IPropertyBag).GUID
                moniker(0).BindToStorage(Nothing, Nothing, bagguid, bagobj)
                bag = CType(bagobj, IPropertyBag)
                If bag IsNot Nothing Then
                    Dim val As Object = ""
                    Dim FilterFriendlyname As String = ""
                    hr = bag.Read("FriendlyName", val, Nothing)
                    If (hr <> 0) Then
                        Debug.WriteLine("Error in getting filter name : " & DsError.GetErrorText(hr))
                    End If
                    If CType(val, String) = FilterName Then
                        'We have a winner
                        ' Check if that's a video input filter
                        Me._AudioInputName = FilterName
                        val = ""
                        hr = bag.Read("CLSID", val, Nothing)
                        If (hr <> 0) Then
                            Debug.WriteLine("Error in getting filter CLSID : " & DsError.GetErrorText(hr))
                        End If
                        _AudioInputCLSID = New Guid(CType(val, String))

                        'Grab the filter to return to capture function
                        Try
                            moniker(0).BindToObject(Nothing, Nothing, iid, source)
                            DeviceFound = True
                        Catch ex As Exception
                            Debug.WriteLine("Error in BindToObject : " & DsError.GetErrorText(hr))
                            DeviceFound = False
                        End Try


                        If DeviceFound Then
                            'Ok we need to get a pointer on the filter and the property interface
                            AudioSourceFilter = CType(source, IBaseFilter)
                        End If
                    End If
                End If
                bag = Nothing
                Marshal.ReleaseComObject(bagobj)
            End While

            If moniker(0) IsNot Nothing Then
                Marshal.ReleaseComObject(moniker(0))
            End If
            If classEnum IsNot Nothing Then
                Marshal.ReleaseComObject(classEnum)
            End If
            Return CType(source, IBaseFilter)
        Catch ex As Exception
            MessageBox.Show("An unrecoverable error has occurred.With error : " & ex.ToString)
            Return Nothing
        End Try
    End Function

    Public Shared Function FindCapDevice(ByVal FilterName As String) As IBaseFilter
        Try
            Debug.WriteLine("Start the Sub FindCaptureDevice")
            Dim hr As Integer = 0
            Dim classEnum As IEnumMoniker = Nothing
            Dim moniker As IMoniker() = New IMoniker(0) {}
            Dim source As Object = Nothing
            Dim devEnum As ICreateDevEnum = CType(New CreateDevEnum, ICreateDevEnum)
            hr = devEnum.CreateClassEnumerator(FilterCategory.VideoInputDevice, classEnum, 0)
            Debug.WriteLine("Create an enumerator for the video capture devices : " & DsError.GetErrorText(hr))
            DsError.ThrowExceptionForHR(hr)
            Marshal.ReleaseComObject(devEnum)
            If classEnum Is Nothing Then
                Throw New ApplicationException("No video capture device was detected.\r\n\r\n" & _
                               "This sample requires a video capture device, such as a USB WebCam,\r\n" & _
                               "to be installed and working properly.  The sample will now close.")
            End If
            Dim DeviceFound As Boolean = False
            While classEnum.Next(moniker.Length, moniker, IntPtr.Zero) = 0 And DeviceFound = False
                Dim iid As Guid = GetType(IBaseFilter).GUID
                Dim bagobj As Object = Nothing
                Dim bag As IPropertyBag = Nothing
                Dim bagguid As Guid = GetType(IPropertyBag).GUID
                moniker(0).BindToStorage(Nothing, Nothing, bagguid, bagobj)
                bag = CType(bagobj, IPropertyBag)
                If bag IsNot Nothing Then
                    Dim val As Object = ""
                    Dim FilterFriendlyname As String = ""
                    hr = bag.Read("FriendlyName", val, Nothing)
                    If (hr <> 0) Then
                        Debug.WriteLine("Error in getting filter name : " & DsError.GetErrorText(hr))
                    End If
                    If CType(val, String) = FilterName Then
                        'We have a winner
                        'Grab the filter to return to capture function
                        Try
                            moniker(0).BindToObject(Nothing, Nothing, iid, source)
                            DeviceFound = True
                        Catch ex As Exception
                            Debug.WriteLine("Error in BindToObject : " & DsError.GetErrorText(hr))
                            DeviceFound = False
                        End Try

                    End If
                End If
                bag = Nothing
                Marshal.ReleaseComObject(bagobj)
            End While

            If moniker(0) IsNot Nothing Then
                Marshal.ReleaseComObject(moniker(0))
            End If
            If classEnum IsNot Nothing Then
                Marshal.ReleaseComObject(classEnum)
            End If
            Return CType(source, IBaseFilter)
        Catch ex As Exception
            MessageBox.Show("An unrecoverable error has occurred.With error : " & ex.ToString)
            Return Nothing
        End Try
    End Function


    Public Overridable Sub DisconnectTheGraph()
        Dim hr As Integer = 0
        _VideoSourceFilterName = ""
        RaiseEvent OnGraphBuilded(Me, False)
        '//stop previewing data
        If Not (Me.MediaControl Is Nothing) Then
            Me.MediaControl.StopWhenReady()
        End If

        Me.CurrentState = PlayState.Stopped

        '//stop recieving events
        If Not (Me.MediaEventEx Is Nothing) Then
            Me.MediaEventEx.SetNotifyWindow(IntPtr.Zero, WM_GRAPHNOTIFY, IntPtr.Zero)
        End If

        '// Relinquish ownership (IMPORTANT!) of the video window.
        '// Failing to call put_Owner can lead to assert failures within
        '// the video renderer, as it still assumes that it has a valid
        '// parent window.
        If Not (Me.VideoWindow Is Nothing) Then
            Me.VideoWindow.put_Visible(OABool.False)
            Me.VideoWindow.put_Owner(IntPtr.Zero)
        End If

        ' // Remove filter graph from the running object table
        If Not (rot Is Nothing) Then
            rot.Dispose()
            rot = Nothing
        End If

        'Ok disconnect the source filter

        'Connect the 2 filters
        If Me.AudioSourceFilter IsNot Nothing AndAlso FilterGraph IsNot Nothing Then
            Dim PinInogeniOutput As IPin = Nothing
            hr = Me.CaptureGraphBuilder.FindPin(Me.AudioSourceFilter, PinDirection.Output, Nothing, Nothing, False, 0, PinInogeniOutput)
            Debug.WriteLine("FindPin of Inogeni audio to our graph : " & DsError.GetErrorText(hr))
            DsError.ThrowExceptionForHR(hr)

            FilterGraph.Disconnect(PinInogeniOutput)
        Else
            'problem 
            Dim problemhere As Boolean = True
        End If


    End Sub

    Public Overridable Sub closeinterfaces()
        _VideoSourceFilterName = ""
        RaiseEvent OnGraphBuilded(Me, False)
        '//stop previewing data
        If Not (Me.MediaControl Is Nothing) Then
            Me.MediaControl.StopWhenReady()
        End If

        Me.CurrentState = PlayState.Stopped

        '//stop recieving events
        If Not (Me.MediaEventEx Is Nothing) Then
            Me.MediaEventEx.SetNotifyWindow(IntPtr.Zero, WM_GRAPHNOTIFY, IntPtr.Zero)
        End If

        '// Relinquish ownership (IMPORTANT!) of the video window.
        '// Failing to call put_Owner can lead to assert failures within
        '// the video renderer, as it still assumes that it has a valid
        '// parent window.
        If Not (Me.VideoWindow Is Nothing) Then
            Me.VideoWindow.put_Visible(OABool.False)
            Me.VideoWindow.put_Owner(IntPtr.Zero)
        End If

        ' // Remove filter graph from the running object table
        If Not (rot Is Nothing) Then
            rot.Dispose()
            rot = Nothing
        End If


        If Me.FilterGraph IsNot Nothing Then
            Marshal.ReleaseComObject(Me.FilterGraph) : Me.FilterGraph = Nothing
        End If
        '// Release DirectShow interfaces
        If Me.MediaControl IsNot Nothing Then
            Marshal.ReleaseComObject(Me.MediaControl) : Me.MediaControl = Nothing
        End If
        If Me.MediaEventEx IsNot Nothing Then
            Marshal.ReleaseComObject(Me.MediaEventEx) : Me.MediaEventEx = Nothing
        End If
        If Me.VideoWindow IsNot Nothing Then
            Marshal.ReleaseComObject(Me.VideoWindow) : Me.VideoWindow = Nothing
        End If
        If Me.GraphBuilder IsNot Nothing Then
            Marshal.ReleaseComObject(Me.GraphBuilder) : Me.GraphBuilder = Nothing
        End If
        If Me.CaptureGraphBuilder IsNot Nothing Then
            Marshal.ReleaseComObject(Me.CaptureGraphBuilder) : Me.CaptureGraphBuilder = Nothing
        End If

        If Me.ConfigStreamCaps IsNot Nothing Then
            Marshal.ReleaseComObject(Me.ConfigStreamCaps) : Me.ConfigStreamCaps = Nothing
        End If

        If Me.MediaControlRecorder IsNot Nothing Then
            Marshal.ReleaseComObject(Me.MediaControlRecorder) : Me.MediaControlRecorder = Nothing
        End If

        If AudioRendererFilter IsNot Nothing Then
            Marshal.ReleaseComObject(Me.AudioRendererFilter) : Me.AudioRendererFilter = Nothing
        End If

        If AudioSourceFilter IsNot Nothing Then
            Marshal.ReleaseComObject(Me.AudioSourceFilter) : Me.AudioSourceFilter = Nothing
        End If
    End Sub


    Public Sub ChangePreviewState(ByVal showVideo As Boolean)
        Dim hr As Integer = 0
        '// If the media control interface isn't ready, don't call it
        If Me.MediaControl Is Nothing Then
            Debug.WriteLine("MediaControl is nothing")
            Return
        End If

        If showVideo = True Then
            If Not (Me.CurrentState = PlayState.Running) Then
                Debug.WriteLine("Start previewing video data")
                hr = Me.MediaControl.Run
                Me.CurrentState = PlayState.Running
            End If
        Else
            Debug.WriteLine("Stop previewing video data")
            hr = Me.MediaControl.StopWhenReady
            Me.CurrentState = PlayState.Stopped
        End If
    End Sub

#End Region

#Region "Window preview handler"
    Private Sub HandleGraphEvent()
        Dim hr As Integer = 0
        Dim evCode As EventCode
        Dim evParam1 As Integer
        Dim evParam2 As Integer
        If Me.MediaEventEx Is Nothing Then
            Return
        End If
        While Me.MediaEventEx.GetEvent(evCode, evParam1, evParam2, 0) = 0
            '// Free event parameters to prevent memory leaks associated with
            '// event parameter data.  While this application is not interested
            '// in the received events, applications should always process them.
            hr = Me.MediaEventEx.FreeEventParams(evCode, evParam1, evParam2)
            DsError.ThrowExceptionForHR(hr)

            '// Insert event processing code here, if desired
        End While
    End Sub

    Public Sub Notify(ByRef m As Message)
        Select Case m.Msg
            Case WM_GRAPHNOTIFY
                HandleGraphEvent()
        End Select
        If Not (Me.VideoWindow Is Nothing) Then
            Me.VideoWindow.NotifyOwnerMessage(m.HWnd, m.Msg, m.WParam.ToInt32, m.LParam.ToInt32)
        End If
    End Sub
#End Region


End Class
